import React from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";

const business = helpers.getBusiness();
async function getCityList() {
  const config = {
    method: "get",
    url: AppEnvironment.urlBaseApi + "general/city-list",
    headers: AppEnvironment.headers,
  };

  return axios(config)
    .then((res) => res.data.data)
    .catch((err) => console.log(err));
}

const cityList = async () => {
  const array = await getCityList();
  const cityList = array.map((data) => ({ value: data.id, label: data.name }));
  return cityList;
};

async function getDistrictList(id) {
  const config = {
    method: "get",
    url: AppEnvironment.urlBaseApi + "general/district-list?city_id=" + id,
    headers: AppEnvironment.headers,
  };

  return axios(config)
    .then((res) => res.data.data)
    .catch((err) => console.log(err));
}

const districtList = async (id) => {
  const array = await getDistrictList(id);
  const districtList = array.map((data) => ({ value: data.id, label: data.name }));
  return districtList;
};

async function getWardList(id) {
  const config = {
    method: "get",
    url: AppEnvironment.urlBaseApi + "general/ward-list?district_id=" + id,
    headers: AppEnvironment.headers,
  };

  return axios(config)
    .then((res) => res.data.data)
    .catch((err) => console.log(err));
}

const wardList = async (id) => {
  const array = await getWardList(id);
  const wardList = array.map((data) => ({ value: data.id, label: data.name }));
  return wardList;
};

async function getNationList() {
  const config = {
    method: "get",
    url: AppEnvironment.urlBaseApi + "general/nation-list",
    headers: AppEnvironment.headers,
  };

  return axios(config)
    .then((res) => res.data.data)
    .catch((err) => console.log(err));
}

const nationList = async () => {
  const array = await getNationList();
  const nationList = array.map((data) => ({ value: data.id, label: data.name }));
  return nationList;
};

async function getReligionList() {
  const config = {
    method: "get",
    url: AppEnvironment.urlBaseApi + "general/religion-list",
    headers: AppEnvironment.headers,
  };

  return axios(config)
    .then((res) => res.data.data)
    .catch((err) => console.log(err));
}

const religionList = async () => {
  const array = await getReligionList();
  const religionList = array.map((data) => ({ value: data.id, label: data.name }));
  return religionList;
};

async function getEthnicList() {
  const config = {
    method: "get",
    url: AppEnvironment.urlBaseApi + "general/ethnic-list",
    headers: AppEnvironment.headers,
  };

  return axios(config)
    .then((res) => res.data.data)
    .catch((err) => console.log(err));
}

const ethnicList = async () => {
  const array = await getEthnicList();
  const ethnicList = array.map((data) => ({ value: data.id, label: data.name }));
  return ethnicList;
};

async function getStaffTypeList() {
  const config = {
    method: "get",
    url: AppEnvironment.urlBaseApi + "staff-type?business_id=" + business.id,
    headers: AppEnvironment.headers,
  };

  return axios(config)
    .then((res) => res.data.data)
    .catch((err) => console.log(err));
}

const staffTypeList = async () => {
  const array = await getStaffTypeList();
  const staffTypeList = array.map((data) => ({ value: data.id, label: data.staff_type_name }));
  return staffTypeList;
};

async function getDepartmentList() {
  const config = {
    method: "get",
    url: AppEnvironment.urlBaseApi + "department?business_id=" + business.id,
    headers: AppEnvironment.headers,
  };

  return axios(config)
    .then((res) => res.data.data)
    .catch((err) => console.log(err));
}

const departmentList = async () => {
  const array = await getDepartmentList();
  const departmentList = array.map((data) => ({ value: data.id, label: data.department_name }));
  return departmentList;
};

async function getPositionList() {
  const config = {
    method: "get",
    url: AppEnvironment.urlBaseApi + "position?business_id=" + business.id,
    headers: AppEnvironment.headers,
  };

  return axios(config)
    .then((res) => res.data.data)
    .catch((err) => console.log(err));
}

const positionList = async () => {
  const array = await getPositionList();
  const positionList = array.map((data) => ({ value: data.id, label: data.position_name }));
  return positionList;
};

async function getLevelList() {
  const config = {
    method: "get",
    url: AppEnvironment.urlBaseApi + "level?business_id=" + business.id,
    headers: AppEnvironment.headers,
  };

  return axios(config)
    .then((res) => res.data.data)
    .catch((err) => console.log(err));
}

const levelList = async () => {
  const array = await getLevelList();
  const levelList = array.map((data) => ({ value: data.id, label: data.level_name }));
  return levelList;
};

async function getTitleList() {
  const config = {
    method: "get",
    url: AppEnvironment.urlBaseApi + "title?business_id=" + business.id,
    headers: AppEnvironment.headers,
  };

  return axios(config)
    .then((res) => res.data.data)
    .catch((err) => console.log(err));
}

const titleList = async () => {
  const array = await getTitleList();
  const titleList = array.map((data) => ({ value: data.id, label: data.title_name }));
  return titleList;
};

async function getTechniqueList() {
  const config = {
    method: "get",
    url: AppEnvironment.urlBaseApi + "technique?business_id=" + business.id,
    headers: AppEnvironment.headers,
  };

  return axios(config)
    .then((res) => res.data.data)
    .catch((err) => console.log(err));
}

const techniqueList = async () => {
  const array = await getTechniqueList();
  const techniqueList = array.map((data) => ({ value: data.id, label: data.technique_name }));
  return techniqueList;
};

async function getCertificationList() {
  const config = {
    method: "get",
    url: AppEnvironment.urlBaseApi + "certification?business_id=" + business.id,
    headers: AppEnvironment.headers,
  };

  return axios(config)
    .then((res) => res.data.data)
    .catch((err) => console.log(err));
}

const certificationList = async () => {
  const array = await getCertificationList();
  const certificationList = array.map((data) => ({ value: data.id, label: data.certification_name }));
  return certificationList;
};

async function getJobTitleList() {
  const config = {
    method: "get",
    url: AppEnvironment.urlBaseApi + "job-title?business_id=" + business.id,
    headers: AppEnvironment.headers,
  };

  return axios(config)
    .then((res) => res.data.data)
    .catch((err) => console.log(err));
}

const jobTitleList = async () => {
  const array = await getJobTitleList();
  const jobTitleList = array.map((data) => ({ value: data.id, label: data.job_title_name + " - " + data.job_title_code }));
  return jobTitleList;
};

export default {
  cityList,
  districtList,
  wardList,
  nationList,
  religionList,
  ethnicList,
  staffTypeList,
  departmentList,
  positionList,
  levelList,
  titleList,
  techniqueList,
  certificationList,
  jobTitleList,
};
