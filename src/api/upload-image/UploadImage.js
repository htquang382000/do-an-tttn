import React from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";

async function uploadImage(image) {
  console.log(new File.readFile(image));
  const config = {
    method: "post",
    url: AppEnvironment.urlBaseApi + "upload-image",
    headers: AppEnvironment.headers,
    data: {
      image: image,
    },
  };

  return axios(config)
    .then((res) => res.data)
    .catch((err) => console.log(err));
}

export default uploadImage;
