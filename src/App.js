import "./App.css";
import React from "react";
import helpers from "./helpers";

import LoginPage from "./pages/login/LoginPage";
import CreateBusinessPage from "./pages/login/CreateBusinessPage";
import BusinessLocationPage from "./pages/login/BusinessLocationPage";

import RegisterPage from "./pages/register/RegisterPage";
import RegisterVerifyOTPPage from "./pages/register/VerifyOTPPage";
import RegisterSucessVerifyOTPPage from "./pages/register/SuccessVerifyPage";

import ResendOTPPage from "./pages/forgot-password/ResendOTPPage";
import ForgotVerifyOTPPage from "./pages/forgot-password/VerifyOTPPage";
import ForgotChangePasswordPage from "./pages/forgot-password/ChangePasswordPage";
import SuccessChangePassword from "./pages/forgot-password/SuccessChangePassword";

import Loading from "./components/Loading";

import HomePage from "./pages/home/HomePage";
import ProfilePage from "./pages/profile/ProfilePage";
import ChangePasswordPage from "./pages/profile/ChangePasswordPage";

import DepartmentPage from "./pages/department/DepartmentPage";
import DepartmentDetailPage from "./pages/department/DepartmentDetailPage";

import PosiotionPage from "./pages/position/PosiotionPage";
import PositionDetailPage from "./pages/position/PositionDetailPage";

import LevelPage from "./pages/level/LevelPage";
import LevelDetailPage from "./pages/level/LevelDetailPage";

import TechniquePage from "./pages/technique/TechniquePage";
import TechniqueDetailPage from "./pages/technique/TechniqueDetailPage";

import CertificationPage from "./pages/certification/CertificationPage";
import CertificationDetailPage from "./pages/certification/CertificationDetailPage";

import StaffTypePage from "./pages/staff-type/StaffTypePage";
import StaffTypeDetailPage from "./pages/staff-type/StaffTypeDetailPage";

import TitlePage from "./pages/title/TitlePage";
import TitleDetailPage from "./pages/title/TitleDetailPage";

import StaffPage from "./pages/staff/StaffPage";
import StaffLeavePage from "./pages/staff/StaffLeavePage";
import CreateStaffPage from "./pages/staff/CreateStaffPage";
import StaffDetailPage from "./pages/staff/StaffDetailPage";
import UploadImage from "./pages/staff/UploadImage";

import JobTitlePage from "./pages/job-title/JobTitlePage";
import JobTitleDetailPage from "./pages/job-title/JobTitleDetailPage";

import BusinessTripPage from "./pages/business-trip/BusinessTripPage";
import BusinessTripDetailPage from "./pages/business-trip/BusinessTripDetailPage";

import CommentPage from "./pages/comment/CommentPage";
import StaffCommentPage from "./pages/comment/StaffCommentPage";

import UserPage from "./pages/user/UserPage";
import UserDetailPage from "./pages/user/UserDetailPage";

import Header from "./components/Header";
import Footer from "./components/Footer";
import SideNav from "./components/SideNav";

import Page404 from "./components/Page404";

import { BrowserRouter, Routes, Route } from "react-router-dom";

function App() {
  const user = helpers.getUser();
  const businessLocationId = helpers.getBusinessLocationId();
  return (
    <div className="App">
      {user ? (
        <>
          {businessLocationId ? (
            <>
              <SideNav />
            </>
          ) : (
            <></>
          )}
          <BrowserRouter>
            <Header />
            <Routes>
              <Route path="/create-business" element={<CreateBusinessPage />} />
              <Route path="/business-location" element={<BusinessLocationPage />} />
              <Route path="/loading" element={<Loading />} />
            </Routes>
          </BrowserRouter>
          <BrowserRouter>
            <Routes>
              <Route path="/" element={<HomePage />} />
              <Route path="/profile" element={<ProfilePage />} />
              <Route path="/profile/change-password" element={<ChangePasswordPage />} />

              <Route path="/department" element={<DepartmentPage />} />
              <Route path="/department/detail" element={<DepartmentDetailPage />} />

              <Route path="/position" element={<PosiotionPage />} />
              <Route path="/position/detail" element={<PositionDetailPage />} />

              <Route path="/level" element={<LevelPage />} />
              <Route path="/level/detail" element={<LevelDetailPage />} />

              <Route path="/technique" element={<TechniquePage />} />
              <Route path="/technique/detail" element={<TechniqueDetailPage />} />

              <Route path="/certification" element={<CertificationPage />} />
              <Route path="/certification/detail" element={<CertificationDetailPage />} />

              <Route path="/staff-type" element={<StaffTypePage />} />
              <Route path="/staff-type/detail" element={<StaffTypeDetailPage />} />

              <Route path="/title" element={<TitlePage />} />
              <Route path="/title/detail" element={<TitleDetailPage />} />

              <Route path="/staff" element={<StaffPage />} />
              <Route path="/staff/leave" element={<StaffLeavePage />} />
              <Route path="/staff/detail" element={<StaffDetailPage />} />
              <Route path="/staff/create" element={<CreateStaffPage />} />
              <Route path="/upload-image" element={<UploadImage />} />

              <Route path="/job-title" element={<JobTitlePage />} />
              <Route path="/job-title/detail" element={<JobTitleDetailPage />} />

              <Route path="/business-trip" element={<BusinessTripPage />} />
              <Route path="/business-trip/detail" element={<BusinessTripDetailPage />} />

              <Route path="/comment" element={<CommentPage />} />
              <Route path="/staff/comment" element={<StaffCommentPage />} />

              {user.user_type === 1 ? (
                <>
                  <Route path="/user" element={<UserPage />} />
                  <Route path="/user/detail" element={<UserDetailPage />} />
                </>
              ) : (
                <></>
              )}

              {/* <Route path="*" element={<Page404 />} /> */}
            </Routes>
            <Footer />
          </BrowserRouter>
        </>
      ) : (
        <>
          <BrowserRouter>
            <Routes>
              <Route path="register" element={<RegisterPage />} />
              <Route path="register/verify-otp" element={<RegisterVerifyOTPPage />} />
              <Route path="register/success-verify-otp" element={<RegisterSucessVerifyOTPPage />} />

              <Route path="forgot-password" element={<ResendOTPPage />} />
              <Route path="/forgot-password/verify-otp" element={<ForgotVerifyOTPPage />} />
              <Route path="/forgot-password/change-password" element={<ForgotChangePasswordPage />} />
              <Route path="/forgot-password/success-change-password" element={<SuccessChangePassword />} />

              <Route path="" element={<LoginPage />} />
              {/* <Route path="*" element={<Page404 />} /> */}
            </Routes>
          </BrowserRouter>
        </>
      )}
    </div>
  );
}

export default App;
