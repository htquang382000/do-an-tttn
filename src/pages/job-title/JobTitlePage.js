import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
import Select from "react-select";
import GeneralApi from "../../api/general/GeneralApi";
import Loading from "../../components/Loading";

import JobTitleListTable from "../../components/job-title/JobTitleListTable";

const business = helpers.getBusiness();
const user = helpers.getUser();

async function createJobTitle(details) {
  var config = {
    method: "post",
    url: AppEnvironment.urlBaseApi + "job-title/store",
    headers: AppEnvironment.headers,
    data: {
      business_id: business.id,
      job_title_name: details.jobTitleName ?? "",
      description: details.description ?? "",
      title_id: details.title.value ?? "",
      level_id: details.level.value ?? "",
      certification_id: details.certification.value ?? "",
      position_id: details.position.value ?? "",
      start_age: details.startAge ?? "",
      end_age: details.endAge ?? "",
      gender: details.gender.value ?? "",
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const JobTitlePage = () => {
  const [details, setDetails] = useState({ jobTitleName: "", description: "", title: "", level: "", certification: "", position: "", startAge: "", endAge: "", gender: "" });
  const [loading, setLoading] = useState(true);
  const [positionList, setPositionList] = useState([]);
  const [levelList, setLevelList] = useState([]);
  const [certifitcationList, setCertifitcationList] = useState([]);
  const [titleList, setTitleList] = useState([]);

  const [errorJobTitleName, setErrorJobTitleName] = useState("");
  const [errorDescription, setErrorDescription] = useState("");
  const [errorTitle, setErrorTitle] = useState("");
  const [errorLevel, setErrorLevel] = useState("");
  const [errorCertification, setErrorCertification] = useState("");
  const [errorPosition, setErrorPosition] = useState("");
  const [errorStartAge, setErrorStartAge] = useState("");
  const [errorEndAge, setErrorEndAge] = useState("");
  const [errorGender, setErrorGender] = useState("");

  useEffect(() => {
    async function fetchData() {
      const positionList = await GeneralApi.positionList();
      setPositionList(positionList);

      const levelList = await GeneralApi.levelList();
      setLevelList(levelList);

      const titleList = await GeneralApi.titleList();
      setTitleList(titleList);

      const certificationList = await GeneralApi.certificationList();
      setCertifitcationList(certificationList);

      setLoading(false);
    }

    fetchData();
  }, []);

  const submitHanble = async (e) => {
    e.preventDefault();
    console.log(details);
    let response = await createJobTitle(details);
    console.log(response);
    let code = response.code;
    if (code === 0) {
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.job_title_name) {
        setErrorJobTitleName(response.messageObject.job_title_name[0]);
      } else {
        setErrorJobTitleName("");
      }

      if (response.messageObject.description) {
        setErrorDescription(response.messageObject.description[0]);
      } else {
        setErrorDescription("");
      }

      if (response.messageObject.title_id) {
        setErrorTitle(response.messageObject.title_id[0]);
      } else {
        setErrorTitle("");
      }

      if (response.messageObject.level_id) {
        setErrorLevel(response.messageObject.level_id[0]);
      } else {
        setErrorLevel("");
      }

      if (response.messageObject.certification_id) {
        setErrorCertification(response.messageObject.certification_id[0]);
      } else {
        setErrorCertification("");
      }

      if (response.messageObject.position_id) {
        setErrorPosition(response.messageObject.position_id[0]);
      } else {
        setErrorPosition("");
      }

      if (response.messageObject.start_age) {
        setErrorStartAge(response.messageObject.start_age[0]);
      } else {
        setErrorStartAge("");
      }

      if (response.messageObject.end_age) {
        setErrorEndAge(response.messageObject.end_age[0]);
      } else {
        setErrorEndAge("");
      }

      if (response.messageObject.gender) {
        setErrorGender(response.messageObject.gender[0]);
      } else {
        setErrorGender("");
      }
    }
  };

  const genderOption = [
    { value: 1, label: "Nam" },
    { value: 2, label: "Nữ" },
  ];

  return loading ? (
    <div className="App">
      <div className="content-wrapper">
        <Loading></Loading>
      </div>
    </div>
  ) : (
    <div className="App">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Chức danh nghề</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <i className="nav-icon fas fa-id-card" /> Chức danh nghề
                  </li>
                  <li className="breadcrumb-item active">Chức danh nghề</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Thêm chức danh nghề</h3>
            </div>
            <form onSubmit={submitHanble}>
              <div className="card-body">
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">
                    Tên chức danh nghề <span className="require-field">*</span>
                  </label>
                  <input type="text" className="form-control" placeholder="Nhập tên chức danh nghề" onChange={(e) => setDetails({ ...details, jobTitleName: e.target.value })} />
                  {errorJobTitleName !== "" ? <div className="error">{errorJobTitleName}</div> : ""}
                </div>

                <div class="form-group">
                  <label>Mô tả</label>
                  <textarea class="form-control" rows="3" placeholder="Nhập mô tả" onChange={(e) => setDetails({ ...details, description: e.target.value })}></textarea>
                  {errorDescription !== "" ? <div className="error">{errorDescription}</div> : ""}
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>
                        Chức danh <span className="require-field">*</span>
                      </label>
                      <Select options={titleList} isSearchable={true} isClearable={true} placeholder="-- Chọn chức danh --" onChange={(e) => setDetails({ ...details, title: e ?? "" })} />
                      {errorTitle !== "" ? <div className="error">{errorTitle}</div> : ""}
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>
                        Trình độ (duyệt theo cấp độ) <span className="require-field">*</span>
                      </label>
                      <Select options={levelList} isSearchable={true} isClearable={true} placeholder="-- Chọn trình độ --" onChange={(e) => setDetails({ ...details, level: e ?? "" })} />
                      {errorLevel !== "" ? <div className="error">{errorLevel}</div> : ""}
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>
                        Bằng cấp <span className="require-field">*</span>
                      </label>
                      <Select
                        options={certifitcationList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn bằng cấp --"
                        onChange={(e) => setDetails({ ...details, certification: e ?? "" })}
                      />
                      {errorCertification !== "" ? <div className="error">{errorCertification}</div> : ""}
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>
                        Chức vụ (duyệt theo cấp độ) <span className="require-field">*</span>
                      </label>
                      <Select options={positionList} isSearchable={true} isClearable={true} placeholder="-- Chọn chức vụ --" onChange={(e) => setDetails({ ...details, position: e ?? "" })} />
                      {errorPosition !== "" ? <div className="error">{errorPosition}</div> : ""}
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Giới tính</label>
                      <Select options={genderOption} isSearchable={true} isClearable={true} placeholder="-- Chọn giới tính --" onChange={(e) => setDetails({ ...details, gender: e ?? "" })} />
                      {errorGender !== "" ? <div className="error">{errorGender}</div> : ""}
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Tuổi bắt đầu</label>
                      <input type="text" className="form-control" placeholder="Nhập tuổi bắt đầu" onChange={(e) => setDetails({ ...details, startAge: e.target.value })} />
                      {errorStartAge !== "" ? <div className="error">{errorStartAge}</div> : ""}
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Tuổi kết thúc</label>
                      <input type="text" className="form-control" placeholder="Nhập tuổi kết thúc" onChange={(e) => setDetails({ ...details, endAge: e.target.value })} />
                      {errorEndAge !== "" ? <div className="error">{errorEndAge}</div> : ""}
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Người tạo</label>
                      <input type="text" className="form-control" placeholder={user.first_name + " " + user.last_name} disabled />
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Ngày tạo</label>
                      <input type="text" className="form-control" placeholder={helpers.getCurrentDay()} disabled />
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
                <button type="submit" className="btn btn-primary">
                  <i className="nav-icon fas fa-plus" /> Tạo chức danh nghề
                </button>
              </div>
            </form>
          </div>
        </section>
        <section className="content">
          <JobTitleListTable />
        </section>
      </div>
    </div>
  );
};

export default JobTitlePage;
