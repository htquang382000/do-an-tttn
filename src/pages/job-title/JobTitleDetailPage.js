import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import { useNavigate } from "react-router-dom";
import helpers from "../../helpers";
import Select from "react-select";
import Loading from "../../components/Loading";
import GeneralApi from "../../api/general/GeneralApi";
import StaffJobTitleList from "../../components/job-title/StaffJobTitleList";
const business = helpers.getBusiness();
const jobTitleId = localStorage.getItem("jobTitleId");

async function updateJobTitle(details) {
  var config = {
    method: "put",
    url: AppEnvironment.urlBaseApi + "job-title/update",
    headers: AppEnvironment.headers,
    data: {
      business_id: business.id,
      job_title_id: jobTitleId,
      job_title_name: details.jobTitleName ?? "",
      description: details.description ?? "",
      title_id: details.title ? details.title.id ?? details.title.value : "",
      level_id: details.level ? details.level.id ?? details.level.value : "",
      certification_id: details.certification ? details.certification.id ?? details.certification.value : "",
      position_id: details.position ? details.position.id ?? details.position.value : "",
      gender: details.gender ? details.gender.value ?? details.gender : "",
      start_age: details.startAge ?? "",
      end_age: details.endAge ?? "",
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const JobTitleDetailPage = () => {
  const [details, setDetails] = useState({ jobTitleName: "", description: "", title: "", level: "", certification: "", position: "", startAge: "", endAge: "", gender: "" });
  const [loading, setLoading] = useState(true);
  const [positionList, setPositionList] = useState([]);
  const [levelList, setLevelList] = useState([]);
  const [certifitcationList, setCertifitcationList] = useState([]);
  const [titleList, setTitleList] = useState([]);

  const [errorJobTitleCode, setErrorJobTitleCode] = useState("");
  const [errorJobTitleName, setErrorJobTitleName] = useState("");
  const [errorDescription, setErrorDescription] = useState("");
  const [errorTitle, setErrorTitle] = useState("");
  const [errorLevel, setErrorLevel] = useState("");
  const [errorCertification, setErrorCertification] = useState("");
  const [errorPosition, setErrorPosition] = useState("");
  const [errorStartAge, setErrorStartAge] = useState("");
  const [errorEndAge, setErrorEndAge] = useState("");
  const [errorGender, setErrorGender] = useState("");

  const navigate = useNavigate();

  async function getJobTitleDetail(jobTitleId) {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "job-title/detail?job_title_id=" + jobTitleId,
      headers: AppEnvironment.headers,
    };

    return axios(config)
      .then((res) => res.data.data)
      .catch((err) => console.log(err));
  }

  useEffect(() => {
    async function fetchData() {
      const jobTitleDetail = await getJobTitleDetail(jobTitleId);
      setDetails({
        jobTitleCode: jobTitleDetail.job_title_code,
        jobTitleName: jobTitleDetail.job_title_name,
        description: jobTitleDetail.description,
        title: jobTitleDetail.title,
        level: jobTitleDetail.level,
        certification: jobTitleDetail.certification,
        position: jobTitleDetail.position,
        startAge: jobTitleDetail.start_age,
        endAge: jobTitleDetail.end_age,
        gender: jobTitleDetail.gender,
        updated_at: jobTitleDetail.updated_at,
        updated_user: jobTitleDetail.updated_user,
      });

      const positionList = await GeneralApi.positionList();
      setPositionList(positionList);

      const levelList = await GeneralApi.levelList();
      setLevelList(levelList);

      const titleList = await GeneralApi.titleList();
      setTitleList(titleList);

      const certificationList = await GeneralApi.certificationList();
      setCertifitcationList(certificationList);

      setLoading(false);
    }

    fetchData();
  }, []);

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await updateJobTitle(details);
    let code = response.code;
    if (code === 0) {
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.job_title_code) {
        setErrorJobTitleCode(response.messageObject.job_title_code[0]);
      } else {
        setErrorJobTitleCode("");
      }

      if (response.messageObject.job_title_name) {
        setErrorJobTitleName(response.messageObject.job_title_name[0]);
      } else {
        setErrorJobTitleName("");
      }

      if (response.messageObject.description) {
        setErrorDescription(response.messageObject.description[0]);
      } else {
        setErrorDescription("");
      }

      if (response.messageObject.title_id) {
        setErrorTitle(response.messageObject.title_id[0]);
      } else {
        setErrorTitle("");
      }

      if (response.messageObject.level_id) {
        setErrorLevel(response.messageObject.level_id[0]);
      } else {
        setErrorLevel("");
      }

      if (response.messageObject.certification_id) {
        setErrorCertification(response.messageObject.certification_id[0]);
      } else {
        setErrorCertification("");
      }

      if (response.messageObject.position_id) {
        setErrorPosition(response.messageObject.position_id[0]);
      } else {
        setErrorPosition("");
      }

      if (response.messageObject.start_age) {
        setErrorStartAge(response.messageObject.start_age[0]);
      } else {
        setErrorStartAge("");
      }

      if (response.messageObject.end_age) {
        setErrorEndAge(response.messageObject.end_age[0]);
      } else {
        setErrorEndAge("");
      }

      if (response.messageObject.gender) {
        setErrorGender(response.messageObject.gender[0]);
      } else {
        setErrorGender("");
      }
    }
  };

  const deleteHandle = (e, jobTitleid) => {
    e.preventDefault();
    const config = {
      method: "delete",
      url: AppEnvironment.urlBaseApi + "job-title/destroy",
      headers: AppEnvironment.headers,
      data: {
        job_title_id: jobTitleId,
      },
    };

    axios(config)
      .then((res) => {
        localStorage.removeItem("jobTitleId");
        navigate("/job-title");
        window.location.reload();
      })
      .catch((err) => console.log(err));
  };

  const genderOption = [
    { value: 1, label: "Nam" },
    { value: 2, label: "Nữ" },
  ];

  return loading ? (
    <div className="App">
      <div className="content-wrapper">
        <Loading></Loading>
      </div>
    </div>
  ) : (
    <div className="App">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Chức danh nghề</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <i className="nav-icon fas fa-id-card" /> Chức danh nghề
                  </li>
                  <li className="breadcrumb-item active">
                    <a href="/job-title" className="text-secondary">
                      Chức danh nghề
                    </a>
                  </li>
                  <li className="breadcrumb-item active">Chi tiết</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Thông tin chức danh nghề</h3>
            </div>
            <form onSubmit={submitHanble}>
              <div className="card-body">
                <div className="form-group">
                  <label>Mã chức danh nghề</label>
                  <input type="text" className="form-control" defaultValue={details.jobTitleCode} disabled />
                  {errorJobTitleCode !== "" ? <div className="error">{errorJobTitleCode}</div> : ""}
                </div>
                <div className="form-group">
                  <label>
                    Tên chức danh nghề <span className="require-field">*</span>
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Nhập tên chức danh nghề"
                    defaultValue={details.jobTitleName}
                    onChange={(e) => setDetails({ ...details, jobTitleName: e.target.value })}
                  />
                  {errorJobTitleName !== "" ? <div className="error">{errorJobTitleName}</div> : ""}
                </div>

                <div className="form-group">
                  <label>Mô tả</label>
                  <textarea
                    className="form-control"
                    rows="3"
                    placeholder="Nhập mô tả"
                    defaultValue={details.description}
                    onChange={(e) => setDetails({ ...details, description: e.target.value })}
                  ></textarea>
                  {errorDescription !== "" ? <div className="error">{errorDescription}</div> : ""}
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Chức danh <span className="require-field">*</span>
                      </label>
                      <Select
                        options={titleList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn chức danh --"
                        defaultValue={{ value: details.title.id, label: details.title.title_name }}
                        onChange={(e) => setDetails({ ...details, title: e ?? "" })}
                      />
                      {errorTitle !== "" ? <div className="error">{errorTitle}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Trình độ (duyệt theo cấp độ) <span className="require-field">*</span>
                      </label>
                      <Select
                        options={levelList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn trình độ --"
                        defaultValue={{ value: details.level.id, label: details.level.level_name }}
                        onChange={(e) => setDetails({ ...details, level: e ?? "" })}
                      />
                      {errorLevel !== "" ? <div className="error">{errorLevel}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Bằng cấp <span className="require-field">*</span>
                      </label>
                      <Select
                        options={certifitcationList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn bằng cấp --"
                        defaultValue={{ value: details.certification.id, label: details.certification.certification_name }}
                        onChange={(e) => setDetails({ ...details, certification: e ?? "" })}
                      />
                      {errorCertification !== "" ? <div className="error">{errorCertification}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Chức vụ (duyệt theo cấp độ) <span className="require-field">*</span>
                      </label>
                      <Select
                        options={positionList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn chức vụ --"
                        defaultValue={{ value: details.position.id, label: details.position.position_name }}
                        onChange={(e) => setDetails({ ...details, position: e ?? "" })}
                      />
                      {errorPosition !== "" ? <div className="error">{errorPosition}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Giới tính</label>
                      <Select
                        options={genderOption}
                        isSearchable={true}
                        isClearable={true}
                        defaultValue={details.gender ? (details.gender === 1 ? { value: 1, label: "Nam" } : { value: 2, label: "Nữ" }) : ""}
                        placeholder="-- Chọn giới tính --"
                        onChange={(e) => setDetails({ ...details, gender: e ?? "" })}
                      />
                      {errorGender !== "" ? <div className="error">{errorGender}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Tuổi bắt đầu</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nhập tuổi bắt đầu"
                        defaultValue={details.startAge}
                        onChange={(e) => setDetails({ ...details, startAge: e.target.value })}
                      />
                      {errorStartAge !== "" ? <div className="error">{errorStartAge}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Tuổi kết thúc</label>
                      <input type="text" className="form-control" placeholder="Nhập tuổi kết thúc" defaultValue={details.endAge} onChange={(e) => setDetails({ ...details, endAge: e.target.value })} />
                      {errorEndAge !== "" ? <div className="error">{errorEndAge}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Người sửa gần nhất</label>
                      <input type="text" className="form-control" defaultValue={details.updated_user.first_name + " " + details.updated_user.last_name} disabled />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Ngày sửa gần nhất</label>
                      <input type="text" className="form-control" defaultValue={helpers.convertDateTimeForApp(details.updated_at)} disabled />
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
                <button type="submit" className="btn btn-primary">
                  <i className="nav-icon fas fa-pen" /> Cập nhật chức danh nghề
                </button>
                <button type="button" className="btn btn-danger float-right" onClick={(e) => deleteHandle(e, jobTitleId)}>
                  Xóa chức danh nghề
                </button>
              </div>
            </form>
          </div>
        </section>
        <section className="content">
          <StaffJobTitleList jobTitleId={jobTitleId} />
        </section>
      </div>
    </div>
  );
};

export default JobTitleDetailPage;
