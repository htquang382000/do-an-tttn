import React, { useState } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";

import ProfileDashboard from "../../components/profile/ProfileDashboard";

const user = helpers.getUser();

async function updatePassword(details) {
  var config = {
    method: "put",
    url: AppEnvironment.urlBaseApi + "auth/change-password",
    headers: AppEnvironment.headers,
    data: {
      current_password: details.currentPassword,
      new_password: details.newPassword,
      password_confirm: details.passwordConfirm,
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const DepartmentPage = () => {
  const [details, setDetails] = useState({ currentPassword: "", newPassword: "", passwordConfirm: "" });

  const [errorCurrentPassword, setErrorCurrentPassword] = useState("");
  const [errorNewPassword, setErrorNewPassword] = useState("");
  const [errorPasswordConfirm, setErrorPasswordConfirm] = useState("");

  const submitHanble = async (e) => {
    console.log(details);
    e.preventDefault();
    let response = await updatePassword(details);
    console.log(response);
    let code = response.code;
    if (code === 0) {
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.current_password) {
        setErrorCurrentPassword(response.messageObject.current_password[0]);
      } else {
        setErrorCurrentPassword("");
      }

      if (response.messageObject.new_password) {
        setErrorNewPassword(response.messageObject.new_password[0]);
      } else {
        setErrorNewPassword("");
      }

      if (response.messageObject.password_confirm) {
        setErrorPasswordConfirm(response.messageObject.password_confirm[0]);
      } else {
        setErrorPasswordConfirm("");
      }
    }
  };

  return (
    <div className="App">
      <div className="content-wrapper">
        <section className="content">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-3">
                <ProfileDashboard user={user}></ProfileDashboard>
              </div>
              <div className="col-md-9">
                <div className="card card-primary">
                  <div className="card-header">
                    <h3 className="card-title">Đổi mật khẩu</h3>
                  </div>
                  <form onSubmit={submitHanble}>
                    <div className="card-body">
                      <div className="form-group">
                        <label htmlFor="exampleInputEmail1">
                          Mật khẩu cũ <span className="require-field">*</span>
                        </label>
                        <input type="password" className="form-control" placeholder="Nhập mật khẩu cũ" onChange={(e) => setDetails({ ...details, currentPassword: e.target.value })} />
                        {errorCurrentPassword !== "" ? <div className="error">{errorCurrentPassword}</div> : ""}
                      </div>

                      <div className="form-group">
                        <label htmlFor="exampleInputEmail1">
                          Mật khẩu mới <span className="require-field">*</span>
                        </label>
                        <input type="password" className="form-control" placeholder="Nhập mật khẩu mới" onChange={(e) => setDetails({ ...details, newPassword: e.target.value })} />
                        {errorNewPassword !== "" ? <div className="error">{errorNewPassword}</div> : ""}
                      </div>
                      <div className="form-group">
                        <label htmlFor="exampleInputEmail1">
                          Nhập lại mật khẩu mới <span className="require-field">*</span>
                        </label>
                        <input
                          type="password"
                          className="form-control"
                          placeholder="Nhập lại mật khẩu mới"
                          onChange={(e) => setDetails({ ...details, passwordConfirm: e.target.value })}
                        />
                        {errorPasswordConfirm !== "" ? <div className="error">{errorPasswordConfirm}</div> : ""}
                      </div>
                    </div>
                    <div className="card-footer">
                      <button type="submit" className="btn btn-primary">
                        <i className="nav-icon fas fa-pen" /> Cập nhật
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};

export default DepartmentPage;
