import React, { useState } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";

import ProfileDashboard from "../../components/profile/ProfileDashboard";

const user = helpers.getUser();

async function updateProfile(details) {
  var config = {
    method: "put",
    url: AppEnvironment.urlBaseApi + "profile/update",
    headers: AppEnvironment.headers,
    data: {
      image: details.image,
      first_name: details.firstName,
      last_name: details.lastName,
      mobile_phone: details.phone,
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

async function getProfile() {
  var config = {
    method: "get",
    url: AppEnvironment.urlBaseApi + "profile",
    headers: AppEnvironment.headers,
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const DepartmentPage = () => {
  const [details, setDetails] = useState({ image: user.image, firstName: user.first_name, lastName: user.last_name, phone: user.mobile_phone });

  const [errorImage, setErrorImage] = useState("");
  const [errorFirstName, setErrorFirstName] = useState("");
  const [errorLastName, setErrorLastName] = useState("");
  const [errorPhone, setErrorPhone] = useState("");

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await updateProfile(details);
    let code = response.code;
    if (code === 0) {
      let profileRes = await getProfile();

      user.image = profileRes.data.image;
      user.first_name = profileRes.data.first_name;
      user.last_name = profileRes.data.last_name;
      user.mobile_phone = profileRes.data.mobile_phone;
      user.updated_at = profileRes.data.updated_at;

      localStorage.setItem("user", JSON.stringify(user));
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.image) {
        setErrorImage(response.messageObject.image[0]);
      } else {
        setErrorImage("");
      }

      if (response.messageObject.first_name) {
        setErrorFirstName(response.messageObject.first_name[0]);
      } else {
        setErrorFirstName("");
      }

      if (response.messageObject.last_name) {
        setErrorLastName(response.messageObject.last_name[0]);
      } else {
        setErrorLastName("");
      }

      if (response.messageObject.mobile_phone) {
        setErrorPhone(response.messageObject.mobile_phone[0]);
      } else {
        setErrorPhone("");
      }
    }
  };

  return (
    <div className="App">
      <div className="content-wrapper">
        <section className="content">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-3">
                <ProfileDashboard user={user}></ProfileDashboard>
              </div>
              <div className="col-md-9">
                <div className="card card-primary">
                  <div className="card-header">
                    <h3 className="card-title">Thay đổi thông tin</h3>
                  </div>
                  <form onSubmit={submitHanble}>
                    <div className="card-body">
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Chọn ảnh:</label>
                            <input type="file" className="custom-file-input" onChange={(e) => setDetails({ ...details, image: e.target.value })} />
                            <label className="custom-file-label custom-add-file">Tìm kiếm</label>
                            {errorImage !== "" ? <div className="error">{errorImage}</div> : ""}
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <img
                          className="profile-user-img img-fluid img-circle"
                          src={user.image ?? "https://png.pngtree.com/element_our/20200610/ourlarge/pngtree-default-avatar-image_2237213.jpg"}
                          alt="User profile picture"
                        />
                      </div>

                      <div className="row">
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Họ và tên đệm:</label>
                            <input
                              type="text"
                              className="form-control"
                              placeholder="Nhập họ và tên đệm"
                              defaultValue={details.firstName}
                              onChange={(e) => setDetails({ ...details, firstName: e.target.value })}
                            />
                            {errorFirstName !== "" ? <div className="error">{errorFirstName}</div> : ""}
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Tên:</label>
                            <input
                              type="text"
                              className="form-control"
                              placeholder="Nhập tên"
                              defaultValue={details.lastName}
                              onChange={(e) => setDetails({ ...details, lastName: e.target.value })}
                            />
                            {errorLastName !== "" ? <div className="error">{errorLastName}</div> : ""}
                          </div>
                        </div>
                      </div>

                      <div className="form-group">
                        <label>Email:</label>
                        <input type="text" className="form-control" defaultValue={user.email} disabled />
                      </div>

                      <div className="form-group">
                        <label>Số điện thoại (Không bắt buộc):</label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Nhập số điện thoại"
                          defaultValue={details.phone}
                          onChange={(e) => setDetails({ ...details, phone: e.target.value })}
                        />
                        {errorPhone !== "" ? <div className="error">{errorPhone}</div> : ""}
                      </div>
                    </div>
                    <div className="card-footer">
                      <button type="submit" className="btn btn-primary">
                        <i className="nav-icon fas fa-pen" /> Cập nhật
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};

export default DepartmentPage;
