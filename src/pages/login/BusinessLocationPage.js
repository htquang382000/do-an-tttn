import React, { useState, useEffect } from "react";
import BusinessLocationList from "../../components/business-location/BusinessLocationList";
import helpers from "../../helpers";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import Select from "react-select";
import GeneralApi from "../../api/general/GeneralApi";

const business = helpers.getBusiness();

const BusinessLocationPage = () => {
  const [details, setDetails] = useState({ businessLocationName: "", address: "", city: "", district: "", ward: "", isDefault: 0 });

  const [cityList, setCityList] = useState([]);
  const [districtList, setDistrictList] = useState([]);
  const [wardList, setWardList] = useState([]);

  const [errorBusinessLocationName, setErrorBusinessLocationName] = useState("");
  const [errorAdress, setErrorAdress] = useState("");
  const [errorCity, setErrorCity] = useState("");
  const [errorDistrict, setErrorDistrict] = useState("");
  const [errorWard, setErrorWard] = useState("");
  const [errorIsDefault, setErrorIsDefault] = useState("");

  useEffect(() => {
    async function fetchData() {
      const cityList = await GeneralApi.cityList();
      setCityList(cityList);
    }

    fetchData();
  }, []);

  async function createBusinessLocation(details) {
    var config = {
      method: "post",
      url: AppEnvironment.urlBaseApi + "business-location/store",
      headers: AppEnvironment.headers,
      data: {
        business_id: business.id,
        business_location_name: details.businessLocationName ?? "",
        address: details.address ?? "",
        city_id: details.city.value ?? "",
        district_id: details.district.value ?? "",
        ward_id: details.ward.value ?? "",
        is_default: details.isDefault ?? 0,
      },
    };

    return axios(config)
      .then((response) => response.data)
      .catch(function (error) {
        console.log(error);
      });
  }

  const loadDistrict = async (e) => {
    setDetails({ ...details, city: e ?? "" });
    console.log(details);

    if (e !== null) {
      const districtList = await GeneralApi.districtList(e.value);
      setDistrictList(districtList);
    }
  };

  const loadWard = async (e) => {
    setDetails({ ...details, district: e ?? "" });
    console.log(details);

    if (e !== null) {
      const wardList = await GeneralApi.wardList(e.value);
      setWardList(wardList);
    }
  };

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await createBusinessLocation(details);
    console.log(response);
    let code = response.code;
    if (code === 0) {
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.business_location_name) {
        setErrorBusinessLocationName(response.messageObject.business_location_name[0]);
      } else {
        setErrorBusinessLocationName("");
      }

      if (response.messageObject.address) {
        setErrorAdress(response.messageObject.address[0]);
      } else {
        setErrorAdress("");
      }

      if (response.messageObject.city_id) {
        setErrorCity(response.messageObject.city_id[0]);
      } else {
        setErrorCity("");
      }

      if (response.messageObject.district_id) {
        setErrorDistrict(response.messageObject.district_id[0]);
      } else {
        setErrorDistrict("");
      }

      if (response.messageObject.ward_id) {
        setErrorWard(response.messageObject.ward_id[0]);
      } else {
        setErrorWard("");
      }

      if (response.messageObject.is_default) {
        setErrorIsDefault(response.messageObject.is_default[0]);
      } else {
        setErrorIsDefault("");
      }
    }
  };

  const finishSetup = (e, business) => {
    e.preventDefault();
    const config = {
      method: "put",
      url: AppEnvironment.urlBaseApi + "business/finish-setup",
      headers: AppEnvironment.headers,
      data: {
        business_id: business.id,
      },
    };

    axios(config)
      .then((res) => {
        console.log(res.data);
        business.is_finished_setup = 1;
        localStorage.setItem("business", JSON.stringify(business));
        window.location.reload();
      })
      .catch((err) => console.log(err));
  };

  return (
    <div className="register-page">
      <div className="business-location-box">
        <div className="card card-outline card-primary">
          <div className="card-header text-center">
            <a href="/" className="h1">
              <b>Quản lý nhân sự</b>
            </a>
          </div>
          <div className="card-body">
            <p className="login-box-msg">Danh sách chi nhánh doanh nghiệp</p>
            <BusinessLocationList />
            {business.is_finished_setup !== 1 ? (
              <div className="row mt-4">
                <div className="col-4">
                  <button type="submit" className="btn btn-secondary btn-block" data-toggle="modal" data-target="#createModal">
                    Thêm chi nhánh
                  </button>
                </div>
                <div className="col-4"></div>
                <div className="col-4">
                  <button type="submit" className="btn btn-primary btn-block" onClick={(e) => finishSetup(e, business)}>
                    Hoàn tất
                  </button>
                </div>
              </div>
            ) : (
              <></>
            )}
          </div>
        </div>
        <div className="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLongTitle">
                  Thông tin chi nhánh doanh nghiệp
                </h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form onSubmit={submitHanble}>
                <div className="modal-body">
                  <div className="form-group">
                    <label htmlFor="exampleInputEmail1">
                      Tên chi nhánh doanh nghiệp <span className="require-field">*</span>
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Nhập tên chi nhánh doanh nghiệp"
                      onChange={(e) => setDetails({ ...details, businessLocationName: e.target.value })}
                    />
                    {errorBusinessLocationName !== "" ? <div className="error">{errorBusinessLocationName}</div> : ""}
                  </div>
                  <div className="form-group">
                    <label htmlFor="exampleInputEmail1">
                      Địa chỉ <span className="require-field">*</span>
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Nhập địa chỉ chi nhánh doanh nghiệp"
                      onChange={(e) => setDetails({ ...details, address: e.target.value })}
                    />
                    {errorAdress !== "" ? <div className="error">{errorAdress}</div> : ""}
                  </div>
                  <div className="row">
                    <div className="col-sm-4">
                      <div className="form-group">
                        <label>
                          Tỉnh/Thành phố <span className="require-field">*</span>
                        </label>
                        <Select options={cityList} isSearchable={true} isClearable={true} placeholder="-- Chọn tỉnh/thành phố --" onChange={loadDistrict} />
                        {errorCity !== "" ? <div className="error">{errorCity}</div> : ""}
                      </div>
                    </div>
                    <div className="col-sm-4">
                      <div className="form-group">
                        <label>
                          Quận/Huyện <span className="require-field">*</span>
                        </label>
                        <Select options={districtList} isSearchable={true} isClearable={true} placeholder="-- Chọn quận/huyện --" onChange={loadWard} />
                        {errorDistrict !== "" ? <div className="error">{errorDistrict}</div> : ""}
                      </div>
                    </div>
                    <div className="col-sm-4">
                      <div className="form-group">
                        <label>
                          Phường/Xã <span className="require-field">*</span>
                        </label>
                        <Select
                          options={wardList}
                          isSearchable={true}
                          isClearable={true}
                          placeholder="-- Chọn phường/xã --"
                          onChange={(e) => setDetails({ ...details, ward: e ?? "" })}
                        />
                        {errorWard !== "" ? <div className="error">{errorWard}</div> : ""}
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="custom-control custom-switch">
                      <input
                        type="checkbox"
                        className="custom-control-input"
                        id="customSwitch1"
                        onChange={(e) => setDetails({ ...details, isDefault: e.target.checked === true ? 1 : 0 })}
                      />
                      <label className="custom-control-label" htmlFor="customSwitch1">
                        Chi nhánh doanh nghiệp mặc định
                      </label>
                    </div>
                  </div>
                </div>

                <div className="modal-footer">
                  <button type="submit" className="btn btn-primary">
                    Thêm
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default BusinessLocationPage;
