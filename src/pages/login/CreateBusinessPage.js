import React, { useState } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import { useNavigate } from "react-router-dom";

async function createBusiness(details) {
  var config = {
    method: "post",
    url: AppEnvironment.urlBaseApi + "business/store",
    headers: AppEnvironment.headers,
    data: {
      business_name: details.businessName,
      business_email: details.businessEmail,
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const CreateBusinessPage = () => {
  const [details, setDetails] = useState({ businessName: "", businessEmail: "" });

  const [errorBusinessName, setErrorBusinessName] = useState("");
  const [errorBusinessEmail, setErrorBusinessEmail] = useState("");

  const navigate = useNavigate();

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await createBusiness(details);
    console.log(response);
    let code = response.code;
    if (code === 0) {
      let business = response.data;
      localStorage.setItem("business", JSON.stringify(business));
      navigate("/create-business-location");
    } else if (code === 2) {
      if (response.messageObject.business_name) {
        setErrorBusinessName(response.messageObject.business_name[0]);
      } else {
        setErrorBusinessName("");
      }

      if (response.messageObject.business_email) {
        setErrorBusinessEmail(response.messageObject.business_email[0]);
      } else {
        setErrorBusinessEmail("");
      }
    }
  };

  return (
    <div className="register-page">
      <div className="register-box">
        <div className="card card-outline card-primary">
          <div className="card-header text-center">
            <a href="/" className="h1">
              <b>Quản lý nhân sự</b>
            </a>
          </div>
          <div className="card-body">
            <p className="login-box-msg">Thêm thông tin doanh nghiệp</p>
            <form onSubmit={submitHanble}>
              <div className="input-group mb-1 mt-3">
                <input type="text" className="form-control" placeholder="Tên doanh nghiệp" onChange={(e) => setDetails({ ...details, businessName: e.target.value })} />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-user" />
                  </div>
                </div>
              </div>
              {errorBusinessName !== "" ? <div className="error">{errorBusinessName}</div> : ""}

              <div className="input-group mb-1 mt-3">
                <input type="text" className="form-control" placeholder="Email doanh nghiệp" onChange={(e) => setDetails({ ...details, businessEmail: e.target.value })} />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-user" />
                  </div>
                </div>
              </div>
              {errorBusinessEmail !== "" ? <div className="error">{errorBusinessEmail}</div> : ""}

              <div className="row mt-4">
                <div className="col-8"></div>
                <div className="col-4">
                  <button type="submit" className="btn btn-primary btn-block">
                    Tiếp tục
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
export default CreateBusinessPage;
