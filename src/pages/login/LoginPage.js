import React, { useState } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import { useNavigate } from "react-router-dom";

async function loginUser(details) {
  var config = {
    method: "post",
    url: AppEnvironment.urlBaseApi + "auth/login",
    headers: AppEnvironment.headers,
    data: {
      email: details.email,
      password: details.password,
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const LoginPage = () => {
  const [details, setDetails] = useState({ email: "", password: "" });

  const [errorEmail, setErrorEmail] = useState("");
  const [errorPassword, setErrorPassword] = useState("");
  const navigate = useNavigate();

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await loginUser(details);
    let code = response.code;
    if (code === 0) {
      let user = response.data.user;
      localStorage.setItem("user", JSON.stringify(user));
      if (response.data.business) {
        let business = response.data.business;
        localStorage.setItem("business", JSON.stringify(business));
        navigate("/business-location");
        window.location.reload();
      } else {
        navigate("/create-business");
        window.location.reload();
      }

      // window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.email) {
        setErrorEmail(response.messageObject.email[0]);
      } else {
        setErrorEmail("");
      }

      if (response.messageObject.password) {
        setErrorPassword(response.messageObject.password[0]);
      } else {
        setErrorPassword("");
      }
    }
  };

  return (
    <div className="login-page">
      <div className="login-box">
        {/* /.login-logo */}
        <div className="card card-outline card-primary">
          <div className="card-header text-center">
            <a href="" className="h1">
              <b>Quản lý nhân sự</b>
            </a>
          </div>
          <div className="card-body">
            <p className="login-box-msg">Đăng nhập và quản lý!</p>
            <form onSubmit={submitHanble}>
              <div className="input-group mb-1">
                <input type="text" className="form-control" placeholder="Email" name="email" id="email" onChange={(e) => setDetails({ ...details, email: e.target.value })} />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-envelope" />
                  </div>
                </div>
              </div>
              {errorEmail !== "" ? <div className="error">{errorEmail}</div> : ""}

              <div className="input-group mt-3 mb-1">
                <input type="password" className="form-control" name="password" id="password" placeholder="Mật khẩu" onChange={(e) => setDetails({ ...details, password: e.target.value })} />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock" />
                  </div>
                </div>
              </div>
              {errorPassword !== "" ? <div className="error">{errorPassword}</div> : ""}

              <div className="row mt-3">
                <div className="col-7">
                  <div className="icheck-primary">
                    <input type="checkbox" id="remember" />
                    <label htmlFor="remember">Nhớ mật khẩu</label>
                  </div>
                </div>
                {/* /.col */}
                <div className="col-5">
                  <button type="submit" className="btn btn-primary btn-block">
                    Đăng nhập
                  </button>
                </div>
                {/* /.col */}
              </div>
            </form>

            <p className="mb-1 mt-3">
              <a href="/forgot-password">Quên mật khẩu?</a>
            </p>
            <p className="mb-0">
              <a href="/register" className="text-center">
                Đăng ký tài khoản
              </a>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
