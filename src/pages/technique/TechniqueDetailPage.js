import React, { useState } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import { useNavigate } from "react-router-dom";

async function updateTechnique(details) {
  var config = {
    method: "put",
    url: AppEnvironment.urlBaseApi + "technique/update",
    headers: AppEnvironment.headers,
    data: {
      technique_id: details.id,
      technique_name: details.techniqueName,
      description: details.description,
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const TechniqueDetailPage = () => {
  let technique = JSON.parse(localStorage.getItem("technique"));

  const [details, setDetails] = useState({
    id: technique.id,
    techniqueCode: technique.technique_code,
    techniqueName: technique.technique_name,
    description: technique.description ?? "",
  });

  const [errorTechniqueName, setErrorTechniqueName] = useState("");
  const [errorDescription, setErrorDescription] = useState("");
  const navigate = useNavigate();

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await updateTechnique(details);
    let code = response.code;
    if (code === 0) {
      console.log(1);
      technique.technique_name = details.techniqueName;
      technique.description = details.description;

      localStorage.setItem("technique", JSON.stringify(technique));
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.technique_name) {
        setErrorTechniqueName(response.messageObject.technique_name[0]);
      } else {
        setErrorTechniqueName("");
      }

      if (response.messageObject.description) {
        setErrorDescription(response.messageObject.description[0]);
      } else {
        setErrorDescription("");
      }
    }
  };

  const deleteHandle = (e, id) => {
    e.preventDefault();
    const config = {
      method: "delete",
      url: AppEnvironment.urlBaseApi + "technique/destroy",
      headers: AppEnvironment.headers,
      data: {
        technique_id: id,
      },
    };

    axios(config)
      .then((res) => {
        localStorage.removeItem("technique");
        navigate("/technique");
        window.location.reload();
      })
      .catch((err) => console.log(err));
  };

  return (
    <div className="App">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Chuyên môn</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <i className="nav-icon fas fa-users" /> Nhân viên
                  </li>
                  <li className="breadcrumb-item active">Chuyên môn</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Sửa chuyên môn</h3>
            </div>
            <form onSubmit={submitHanble}>
              <div className="card-body">
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">Mã chuyên môn</label>
                  <input type="text" className="form-control" defaultValue={details.techniqueCode} disabled />
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">Tên chuyên môn <span className="require-field">*</span></label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Nhập tên chuyên môn"
                    defaultValue={details.techniqueName}
                    onChange={(e) => setDetails({ ...details, techniqueName: e.target.value })}
                  />
                  {errorTechniqueName !== "" ? <div className="error">{errorTechniqueName}</div> : ""}
                </div>

                <div class="form-group">
                  <label>Mô tả</label>
                  <textarea
                    class="form-control"
                    rows="3"
                    placeholder="Nhập mô tả"
                    defaultValue={details.description}
                    onChange={(e) => setDetails({ ...details, description: e.target.value })}
                  ></textarea>
                  {errorDescription !== "" ? <div className="error">{errorDescription}</div> : ""}
                </div>

                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Người sửa</label>
                      <input type="text" className="form-control" value="Hoàng Thanh Quang" disabled />
                    </div>
                  </div>
                  <div class="col-sm-1"></div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Ngày sửa</label>
                      <input type="text" className="form-control" value="08/14/2022" disabled />
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
                <button type="submit" className="btn btn-primary">
                  Cập nhật chuyên môn
                </button>
                <button type="button" className="btn btn-danger float-right" onClick={(e) => deleteHandle(e, details.id)}>
                  Xóa chuyên môn
                </button>
              </div>
            </form>
          </div>
        </section>
      </div>
    </div>
  );
};

export default TechniqueDetailPage;
