import React, { useState } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import { useNavigate } from "react-router-dom";

const ChangePasswordPage = () => {
  const navigate = useNavigate();

  const email = localStorage.getItem("email");
  const otp = localStorage.getItem("otp");
  const [details, setDetails] = useState({ email: email, otp: otp, password: "", passwordConfirm: "" });
  const [errorPassword, setErrorPassword] = useState("");
  const [errorPasswordConfirm, setErrorPasswordConfirm] = useState("");

  async function changePassword(details) {
    var config = {
      method: "put",
      url: AppEnvironment.urlBaseApi + "auth/forgot-password/change-password",
      headers: AppEnvironment.headers,
      data: {
        email: details.email,
        otp_code: details.otp,
        password: details.password ?? "",
        password_confirm: details.passwordConfirm ?? "",
      },
    };

    return axios(config)
      .then((response) => response.data)
      .catch(function (error) {
        console.log(error);
      });
  }

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await changePassword(details);
    console.log(response);
    let code = response.code;
    if (code === 0) {
      localStorage.removeItem("otp");
      navigate("/forgot-password/success-change-password");
    } else if (code === 2) {
      if (response.messageObject.password) {
        setErrorPassword(response.messageObject.password[0]);
      } else {
        setErrorPassword("");
      }

      if (response.messageObject.password_confirm) {
        setErrorPasswordConfirm(response.messageObject.password_confirm[0]);
      } else {
        setErrorPasswordConfirm("");
      }
    }
  };

  return (
    <div className="login-page">
      <div className="login-box">
        <div className="card card-outline card-primary">
          <div className="card-header text-center">
            <a href="/" className="h1">
              <b>Quản lý nhân sự</b>
            </a>
          </div>
          <div className="card-body">
            <p className="login-box-msg">
              <b>{email}</b>
            </p>
            <form onSubmit={submitHanble}>
              <div className="input-group">
                <input type="password" className="form-control" placeholder="Mật khẩu mới" onChange={(e) => setDetails({ ...details, password: e.target.value })} />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock" />
                  </div>
                </div>
              </div>
              {errorPassword !== "" ? <div className="error">{errorPassword}</div> : ""}
              <div className="input-group mt-3">
                <input type="password" className="form-control" placeholder="Nhập lại mật khẩu" onChange={(e) => setDetails({ ...details, passwordConfirm: e.target.value })} />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock" />
                  </div>
                </div>
              </div>
              {errorPasswordConfirm !== "" ? <div className="error">{errorPasswordConfirm}</div> : ""}
              <div className="row mt-3">
                <div className="col-12">
                  <button type="submit" className="btn btn-primary btn-block">
                    Đổi mật khẩu
                  </button>
                </div>
              </div>
            </form>
            <p className="mt-3 mb-1">
              <a href="/">Đăng nhập</a>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
export default ChangePasswordPage;
