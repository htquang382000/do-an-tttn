import React, { useState } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import { useNavigate } from "react-router-dom";

async function forgotPassword(details) {
  var config = {
    method: "put",
    url: AppEnvironment.urlBaseApi + "auth/forgot-password/resend-otp",
    headers: AppEnvironment.headers,
    data: {
      email: details.email,
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const ResendOTPPage = () => {
  const [details, setDetails] = useState({ email: "" });
  const [errorEmail, setErrorEmail] = useState("");
  const navigate = useNavigate();

  const submitHanble = async (e) => {
    e.preventDefault();

    let response = await forgotPassword(details);
    let code = response.code;
    if (code === 0) {
      localStorage.setItem("email", details.email);
      navigate("/forgot-password/verify-otp");
    } else if (code === 2) {
      if (response.messageObject.email) {
        setErrorEmail(response.messageObject.email[0]);
      } else {
        setErrorEmail("");
      }
    }
  };

  return (
    <div className="login-page">
      <div className="login-box">
        <div className="card card-outline card-primary">
          <div className="card-header text-center">
            <a href="/" className="h1">
              <b>Quản lý nhân sự</b>
            </a>
          </div>
          <div className="card-body">
            <p className="login-box-msg">Bạn quên mật khẩu của mình? Tại đây bạn có thể dễ dàng đổi mật khẩu mới.</p>
            <form onSubmit={submitHanble}>
              <div className="input-group mb-1">
                <input type="text" className="form-control" placeholder="Email" onChange={(e) => setDetails({ ...details, email: e.target.value })} />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-envelope" />
                  </div>
                </div>
              </div>
              {errorEmail !== "" ? <div className="error">{errorEmail}</div> : ""}

              <div className="row mt-3">
                <div className="col-12">
                  <button type="submit" className="btn btn-primary btn-block">
                    Lấy mã OTP
                  </button>
                </div>
              </div>
            </form>
            <p className="mt-3 mb-1">
              <a href="/">Đăng nhập</a>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
export default ResendOTPPage;
