import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
import Select from "react-select";
import GeneralApi from "../../api/general/GeneralApi";
import Loading from "../../components/Loading";

import BusinessTripListTable from "../../components/business-trip/BusinessTripListTable";

const business = helpers.getBusiness();
const user = helpers.getUser();
const businessLocationId = helpers.getBusinessLocationId();

async function createBusinessTrip(details) {
  var config = {
    method: "post",
    url: AppEnvironment.urlBaseApi + "business-trip/store",
    headers: AppEnvironment.headers,
    data: {
      business_id: business.id,
      business_location_id: businessLocationId,
      business_trip_name: details.businessTripName ?? "",
      job_title_id: details.jobTitle.value ?? "",
      business_trip_place: details.businessTripPlace ?? "",
      business_trip_purpose: details.businessTripPurpose ?? "",
      description: details.description ?? "",
      start_date: details.startDate ?? "",
      end_date: details.endDate ?? "",
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const BusinessTripPage = () => {
  const [details, setDetails] = useState({
    businessTripName: "",
    jobTitle: "",
    businessTripPlace: "",
    businessTripPurpose: "",
    description: "",
    startDate: "",
    endDate: "",
  });
  const [loading, setLoading] = useState(true);
  const [jobTitleList, setJobTitleList] = useState([]);

  const [errorBusinessTripName, setErrorBusinessTripName] = useState("");
  const [errorJobTitle, setErrorJobTitle] = useState("");
  const [errorBusinessTripPlace, setErrorBusinessTripPlace] = useState("");
  const [errorBusinessTripPurpose, setErrorBusinessTripPurpose] = useState("");
  const [errorDescription, setErrorDescription] = useState("");
  const [errorStartDate, setErrorStartDate] = useState("");
  const [errorEndDate, setErrorEndDate] = useState("");

  useEffect(() => {
    async function fetchData() {
      const jobTitleList = await GeneralApi.jobTitleList();
      setJobTitleList(jobTitleList);

      setLoading(false);
    }

    fetchData();
  }, []);

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await createBusinessTrip(details);
    let code = response.code;
    if (code === 0) {
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.business_trip_name) {
        setErrorBusinessTripName(response.messageObject.business_trip_name[0]);
      } else {
        setErrorBusinessTripName("");
      }

      if (response.messageObject.job_title_id) {
        setErrorJobTitle(response.messageObject.job_title_id[0]);
      } else {
        setErrorJobTitle("");
      }

      if (response.messageObject.business_trip_place) {
        setErrorBusinessTripPlace(response.messageObject.business_trip_place[0]);
      } else {
        setErrorBusinessTripPlace("");
      }

      if (response.messageObject.business_trip_purpose) {
        setErrorBusinessTripPurpose(response.messageObject.business_trip_purpose[0]);
      } else {
        setErrorBusinessTripPurpose("");
      }

      if (response.messageObject.description) {
        setErrorDescription(response.messageObject.description[0]);
      } else {
        setErrorDescription("");
      }

      if (response.messageObject.start_date) {
        setErrorStartDate(response.messageObject.start_date[0]);
      } else {
        setErrorStartDate("");
      }

      if (response.messageObject.end_date) {
        setErrorEndDate(response.messageObject.end_date[0]);
      } else {
        setErrorEndDate("");
      }
    }
  };

  return loading ? (
    <div className="App">
      <div className="content-wrapper">
        <Loading></Loading>
      </div>
    </div>
  ) : (
    <div className="App">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Chuyến công tác</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <i className="nav-icon fas fa-id-card" /> Chuyến công tác
                  </li>
                  <li className="breadcrumb-item active">Chuyến công tác</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Thêm chuyến công tác</h3>
            </div>
            <form onSubmit={submitHanble}>
              <div className="card-body">
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">
                    Tên chuyến công tác <span className="require-field">*</span>
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Nhập tên chuyến công tác"
                    onChange={(e) => setDetails({ ...details, businessTripName: e.target.value })}
                  />
                  {errorBusinessTripName !== "" ? <div className="error">{errorBusinessTripName}</div> : ""}
                </div>
                <div className="row">
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>
                        Chức danh nghề <span className="require-field">*</span>
                      </label>
                      <Select
                        options={jobTitleList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn chức danh nghề --"
                        onChange={(e) => setDetails({ ...details, jobTitle: e ?? "" })}
                      />
                      {errorJobTitle !== "" ? <div className="error">{errorJobTitle}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-8">
                    <div className="form-group">
                      <label>
                        Địa điểm chuyến công tác <span className="require-field">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nhập địa điểm chuyến công tác"
                        onChange={(e) => setDetails({ ...details, businessTripPlace: e.target.value })}
                      />
                      {errorBusinessTripPlace !== "" ? <div className="error">{errorBusinessTripPlace}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Mục đích chuyến công tác <span className="require-field">*</span>
                      </label>
                      <textarea
                        className="form-control"
                        rows="3"
                        placeholder="Nhập mục đích chuyến công tác"
                        onChange={(e) => setDetails({ ...details, businessTripPurpose: e.target.value })}
                      ></textarea>
                      {errorBusinessTripPurpose !== "" ? <div className="error">{errorBusinessTripPurpose}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Mô tả</label>
                      <textarea className="form-control" rows="3" placeholder="Nhập mô tả" onChange={(e) => setDetails({ ...details, description: e.target.value })}></textarea>
                      {errorDescription !== "" ? <div className="error">{errorDescription}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Ngày bắt đầu <span className="require-field">*</span>
                      </label>
                      <input type="date" className="form-control" onChange={(e) => setDetails({ ...details, startDate: e.target.value })} />
                      {errorStartDate !== "" ? <div className="error">{errorStartDate}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Ngày kết thúc <span className="require-field">*</span>
                      </label>
                      <input type="date" className="form-control" onChange={(e) => setDetails({ ...details, endDate: e.target.value })} />
                      {errorEndDate !== "" ? <div className="error">{errorEndDate}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Người tạo</label>
                      <input type="text" className="form-control" placeholder={user.first_name + " " + user.last_name} disabled />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Ngày tạo</label>
                      <input type="text" className="form-control" placeholder={helpers.getCurrentDay()} disabled />
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
                <button type="submit" className="btn btn-primary">
                  <i className="nav-icon fas fa-plus" /> Tạo chuyến công tác
                </button>
              </div>
            </form>
          </div>
        </section>
        <section className="content">
          <BusinessTripListTable />
        </section>
      </div>
    </div>
  );
};

export default BusinessTripPage;
