import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
import Select from "react-select";
import Loading from "../../components/Loading";
import GeneralApi from "../../api/general/GeneralApi";
import StaffBusinessTripList from "../../components/business-trip/StaffBusinessTripList";

const JobTitleDetailPage = () => {
  const businessTripId = localStorage.getItem("businessTripId");

  const [details, setDetails] = useState({
    businessTripCode: "",
    businessTripName: "",
    jobTitle: "",
    businessTripPlace: "",
    businessTripPurpose: "",
    description: "",
    startDate: "",
    endDate: "",
  });
  const [loading, setLoading] = useState(true);
  const [jobTitleList, setJobTitleList] = useState([]);

  const [errorBusinessTripId, setErrorBusinessTripId] = useState("");
  const [errorBusinessTripName, setErrorBusinessTripName] = useState("");
  const [errorJobTitle, setErrorJobTitle] = useState("");
  const [errorBusinessTripPlace, setErrorBusinessTripPlace] = useState("");
  const [errorBusinessTripPurpose, setErrorBusinessTripPurpose] = useState("");
  const [errorDescription, setErrorDescription] = useState("");
  const [errorStartDate, setErrorStartDate] = useState("");
  const [errorEndDate, setErrorEndDate] = useState("");

  async function getBusinessTripDetail(businessTripId) {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "business-trip/detail?business_trip_id=" + businessTripId,
      headers: AppEnvironment.headers,
    };

    return axios(config)
      .then((res) => res.data.data)
      .catch((err) => console.log(err));
  }

  useEffect(() => {
    async function fetchData() {
      const businessTripDetail = await getBusinessTripDetail(businessTripId);
      setDetails({
        ...details,
        businessTripCode: businessTripDetail.business_trip_code,
        businessTripName: businessTripDetail.business_trip_name,
        jobTitle: businessTripDetail.job_title,
        businessTripPlace: businessTripDetail.business_trip_place,
        businessTripPurpose: businessTripDetail.business_trip_purpose,
        description: businessTripDetail.description,
        startDate: businessTripDetail.start_date,
        endDate: businessTripDetail.end_date,
        updated_at: businessTripDetail.updated_at,
        updated_user: businessTripDetail.updated_user,
        status: businessTripDetail.status,
      });

      const jobTitleList = await GeneralApi.jobTitleList();
      setJobTitleList(jobTitleList);

      setLoading(false);
    }

    fetchData();
  }, []);

  async function updateBusinessTrip(details) {
    var config = {
      method: "put",
      url: AppEnvironment.urlBaseApi + "business-trip/update",
      headers: AppEnvironment.headers,
      data: {
        business_trip_id: businessTripId,
        business_trip_name: details.businessTripName ?? "",
        job_title_id: details.jobTitle ? details.jobTitle.id ?? details.jobTitle.value : "",
        business_trip_place: details.businessTripPlace ?? "",
        business_trip_purpose: details.businessTripPurpose ?? "",
        description: details.description ?? "",
        start_date: details.startDate ?? "",
        end_date: details.endDate ?? "",
      },
    };

    return axios(config)
      .then((response) => response.data)
      .catch(function (error) {
        console.log(error);
      });
  }
  const submitHanble = async (e) => {
    e.preventDefault();
    console.log(details);
    let response = await updateBusinessTrip(details);
    let code = response.code;
    if (code === 0) {
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.business_trip_id) {
        setErrorBusinessTripId(response.messageObject.business_trip_id[0]);
      } else {
        setErrorBusinessTripId("");
      }

      if (response.messageObject.business_trip_name) {
        setErrorBusinessTripName(response.messageObject.business_trip_name[0]);
      } else {
        setErrorBusinessTripName("");
      }

      if (response.messageObject.job_title_id) {
        setErrorJobTitle(response.messageObject.job_title_id[0]);
      } else {
        setErrorJobTitle("");
      }

      if (response.messageObject.business_trip_place) {
        setErrorBusinessTripPlace(response.messageObject.business_trip_place[0]);
      } else {
        setErrorBusinessTripPlace("");
      }

      if (response.messageObject.business_trip_purpose) {
        setErrorBusinessTripPurpose(response.messageObject.business_trip_purpose[0]);
      } else {
        setErrorBusinessTripPurpose("");
      }

      if (response.messageObject.description) {
        setErrorDescription(response.messageObject.description[0]);
      } else {
        setErrorDescription("");
      }

      if (response.messageObject.start_date) {
        setErrorStartDate(response.messageObject.start_date[0]);
      } else {
        setErrorStartDate("");
      }

      if (response.messageObject.end_date) {
        setErrorEndDate(response.messageObject.end_date[0]);
      } else {
        setErrorEndDate("");
      }
    }
  };

  async function updateStatusBusinessTrip(status) {
    var config = {
      method: "put",
      url: AppEnvironment.urlBaseApi + "business-trip/update-status",
      headers: AppEnvironment.headers,
      data: {
        business_trip_id: businessTripId,
        status: status ?? "",
      },
    };

    return axios(config)
      .then((response) => response.data)
      .catch(function (error) {
        console.log(error);
      });
  }
  const updateStatus = async (e, status) => {
    e.preventDefault();
    let response = await updateStatusBusinessTrip(status);
    let code = response.code;
    if (code === 0) {
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.business_trip_id) {
        setErrorBusinessTripId(response.messageObject.business_trip_id[0]);
      } else {
        setErrorBusinessTripId("");
      }
    }
  };

  return loading ? (
    <div className="App">
      <div className="content-wrapper">
        <Loading></Loading>
      </div>
    </div>
  ) : (
    <div className="App">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Chuyến công tác</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <i className="nav-icon fas fa-id-card" /> Chuyến công tác
                  </li>
                  <li className="breadcrumb-item active">
                    <a href="/business-trip" className="text-secondary">
                      Chuyến công tác
                    </a>
                  </li>
                  <li className="breadcrumb-item active">Chi tiết</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Thông tin chuyến công tác</h3>
            </div>
            <form onSubmit={submitHanble}>
              <div className="card-body">
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">Mã chuyến công tác</label>
                  <input type="text" className="form-control" defaultValue={details.businessTripCode} disabled />
                  {errorBusinessTripId !== "" ? <div className="error">{errorBusinessTripId}</div> : ""}
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">
                    Tên chuyến công tác <span className="require-field">*</span>
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Nhập tên chuyến công tác"
                    defaultValue={details.businessTripName}
                    onChange={(e) => setDetails({ ...details, businessTripName: e.target.value })}
                  />
                  {errorBusinessTripName !== "" ? <div className="error">{errorBusinessTripName}</div> : ""}
                </div>
                <div className="row">
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>
                        Chức danh nghề <span className="require-field">*</span>
                      </label>
                      <Select
                        options={jobTitleList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn chức danh nghề --"
                        defaultValue={{ value: details.jobTitle.id, label: details.jobTitle.job_title_name + " - " + details.jobTitle.job_title_code }}
                        onChange={(e) => setDetails({ ...details, jobTitle: e ?? "" })}
                      />
                      {errorJobTitle !== "" ? <div className="error">{errorJobTitle}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-8">
                    <div className="form-group">
                      <label>
                        Địa điểm chuyến công tác <span className="require-field">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nhập địa điểm chuyến công tác"
                        defaultValue={details.businessTripPlace}
                        onChange={(e) => setDetails({ ...details, businessTripPlace: e.target.value })}
                      />
                      {errorBusinessTripPlace !== "" ? <div className="error">{errorBusinessTripPlace}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Mục đích chuyến công tác <span className="require-field">*</span>
                      </label>
                      <textarea
                        className="form-control"
                        rows="3"
                        placeholder="Nhập mục đích chuyến công tác"
                        defaultValue={details.businessTripPurpose}
                        onChange={(e) => setDetails({ ...details, businessTripPurpose: e.target.value })}
                      ></textarea>
                      {errorBusinessTripPurpose !== "" ? <div className="error">{errorBusinessTripPurpose}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Mô tả</label>
                      <textarea
                        className="form-control"
                        rows="3"
                        placeholder="Nhập mô tả"
                        defaultValue={details.description}
                        onChange={(e) => setDetails({ ...details, description: e.target.value })}
                      ></textarea>
                      {errorDescription !== "" ? <div className="error">{errorDescription}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Ngày bắt đầu <span className="require-field">*</span>
                      </label>
                      <input type="date" className="form-control" defaultValue={details.startDate} onChange={(e) => setDetails({ ...details, startDate: e.target.value })} />
                      {errorStartDate !== "" ? <div className="error">{errorStartDate}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Ngày kết thúc <span className="require-field">*</span>
                      </label>
                      <input type="date" className="form-control" defaultValue={details.endDate} onChange={(e) => setDetails({ ...details, endDate: e.target.value })} />
                      {errorEndDate !== "" ? <div className="error">{errorEndDate}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="form-group">
                  <label>Trạng thái</label>
                  <input
                    type="text"
                    className="form-control"
                    defaultValue={
                      details.status === 1
                        ? "Chưa thực hiện"
                        : details.status === 2
                        ? "Đang thực hiện"
                        : details.status === 3
                        ? "Đã hoàn thành"
                        : details.status === 4
                        ? "Đã hủy"
                        : ""
                    }
                    disabled
                  />
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Người sửa gần nhất</label>
                      <input type="text" className="form-control" value={details.updated_user.first_name + " " + details.updated_user.last_name} disabled />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Ngày sửa gần nhất</label>
                      <input type="text" className="form-control" value={helpers.convertDateTimeForApp(details.updated_at)} disabled />
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer text-center">
                {details.status === 1 ? (
                  <>
                    <button type="submit" className="btn btn-primary float-left">
                      <i className="nav-icon fas fa-pen" /> Cập nhật chuyến công tác
                    </button>
                    <button type="button" className="btn btn-success" onClick={(e) => updateStatus(e, 2)}>
                      <i className="nav-icon fas fa-pen" /> Thực hiện chuyến công tác
                    </button>
                    <button type="button" className="btn btn-danger float-right" onClick={(e) => updateStatus(e, 4)}>
                      <i className="nav-icon fas fa-pen" /> Hủy chuyến công tác
                    </button>
                  </>
                ) : (
                  <></>
                )}
                {details.status === 2 ? (
                  <>
                    <button type="button" className="btn btn-success float-left" onClick={(e) => updateStatus(e, 3)}>
                      <i className="nav-icon fas fa-pen" /> Hoàn thành chuyến công tác
                    </button>
                    <button type="button" className="btn btn-danger float-right" onClick={(e) => updateStatus(e, 4)}>
                      <i className="nav-icon fas fa-pen" /> Hủy chuyến công tác
                    </button>
                  </>
                ) : (
                  <></>
                )}
              </div>
            </form>
          </div>
        </section>
        <section className="content">
          <StaffBusinessTripList businessTripId={businessTripId} jobTitleId={details.jobTitle.id} status={details.status} />
        </section>
      </div>
    </div>
  );
};

export default JobTitleDetailPage;
