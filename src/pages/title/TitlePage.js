import React, { useState } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";

import TitleListTable from "../../components/title/TitleListTable";

const business = helpers.getBusiness();
const user = helpers.getUser();

async function createTitle(details) {
  var config = {
    method: "post",
    url: AppEnvironment.urlBaseApi + "title/store",
    headers: AppEnvironment.headers,
    data: {
      business_id: business.id,
      title_name: details.titleName,
      priority: details.priority,
      description: details.description,
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const TitlePage = () => {
  const [details, setDetails] = useState({ titleName: "", description: "" });

  const [errorTitleName, setErrorTitleName] = useState("");
  const [errorDescription, setErrorDescription] = useState("");

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await createTitle(details);
    let code = response.code;
    if (code === 0) {
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.title_name) {
        setErrorTitleName(response.messageObject.title_name[0]);
      } else {
        setErrorTitleName("");
      }

      if (response.messageObject.description) {
        setErrorDescription(response.messageObject.description[0]);
      } else {
        setErrorDescription("");
      }
    }
  };

  return (
    <div className="App">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Chức danh</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <i className="nav-icon fas fa-id-card" /> Chức danh nghề
                  </li>
                  <li className="breadcrumb-item active">Chức danh</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Thêm chức danh</h3>
            </div>
            <form onSubmit={submitHanble}>
              <div className="card-body">
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">
                    Tên chức danh <span className="require-field">*</span>
                  </label>
                  <input type="text" className="form-control" placeholder="Nhập tên chức danh" onChange={(e) => setDetails({ ...details, titleName: e.target.value })} />
                  {errorTitleName !== "" ? <div className="error">{errorTitleName}</div> : ""}
                </div>

                <div class="form-group">
                  <label>Mô tả</label>
                  <textarea class="form-control" rows="3" placeholder="Nhập mô tả" onChange={(e) => setDetails({ ...details, description: e.target.value })}></textarea>
                  {errorDescription !== "" ? <div className="error">{errorDescription}</div> : ""}
                </div>

                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Người tạo</label>
                      <input type="text" className="form-control" placeholder={user.first_name + " " + user.last_name} disabled />
                    </div>
                  </div>
                  <div class="col-sm-1"></div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Ngày tạo</label>
                      <input type="text" className="form-control" placeholder={helpers.getCurrentDay()} disabled />
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
                <button type="submit" className="btn btn-primary">
                  <i className="nav-icon fas fa-plus" /> Tạo chức danh
                </button>
              </div>
            </form>
          </div>
        </section>
        <section className="content">
          <TitleListTable />
        </section>
      </div>
    </div>
  );
};

export default TitlePage;
