import React, { useState } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import { useNavigate } from "react-router-dom";
import helpers from "../../helpers";
import StaffList from "../../components/staff/StaffList";

async function updateTitle(details) {
  var config = {
    method: "put",
    url: AppEnvironment.urlBaseApi + "title/update",
    headers: AppEnvironment.headers,
    data: {
      title_id: details.id,
      title_name: details.titleName,
      description: details.description,
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const TitleDetailPage = () => {
  var title = JSON.parse(localStorage.getItem("title"));
  const business = helpers.getBusiness();
  const businessLocationId = helpers.getBusinessLocationId();

  const [details, setDetails] = useState({
    id: title.id,
    titleCode: title.title_code,
    titleName: title.title_name,
    description: title.description ?? "",
  });
  const [errorTitleName, setErrorTitleName] = useState("");
  const [errorDescription, setErrorDescription] = useState("");

  const navigate = useNavigate();

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await updateTitle(details);
    let code = response.code;
    if (code === 0) {
      title.title_name = details.titleName;
      title.description = details.description;

      localStorage.setItem("title", JSON.stringify(title));
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.title_name) {
        setErrorTitleName(response.messageObject.title_name[0]);
      } else {
        setErrorTitleName("");
      }

      if (response.messageObject.description) {
        setErrorDescription(response.messageObject.description[0]);
      } else {
        setErrorDescription("");
      }
    }
  };

  const deleteHandle = (e, id) => {
    e.preventDefault();
    const config = {
      method: "delete",
      url: AppEnvironment.urlBaseApi + "title/destroy",
      headers: AppEnvironment.headers,
      data: {
        title_id: id,
      },
    };

    axios(config)
      .then((res) => {
        localStorage.removeItem("title");
        navigate("/title");
        window.location.reload();
      })
      .catch((err) => console.log(err));
  };

  const url =
    AppEnvironment.urlBaseApi +
    "staff?status=1&page=1&attribute_name=title&attribute_id=" +
    title.id +
    "&business_id=" +
    business.id +
    "&business_location_id=" +
    businessLocationId;

  return (
    <div className="App">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Chức danh</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <i className="nav-icon fas fa-users" /> Nhân viên
                  </li>
                  <li className="breadcrumb-item active">Chức danh</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Sửa chức danh</h3>
            </div>
            <form onSubmit={submitHanble}>
              <div className="card-body">
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">Mã chức danh</label>
                  <input type="text" className="form-control" defaultValue={details.titleCode} disabled />
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">
                    Tên chức danh <span className="require-field">*</span>
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Nhập tên chức danh"
                    defaultValue={details.titleName}
                    onChange={(e) => setDetails({ ...details, titleName: e.target.value })}
                  />
                  {errorTitleName !== "" ? <div className="error">{errorTitleName}</div> : ""}
                </div>

                <div class="form-group">
                  <label>Mô tả</label>
                  <textarea
                    class="form-control"
                    rows="3"
                    placeholder="Nhập mô tả"
                    defaultValue={details.description}
                    onChange={(e) => setDetails({ ...details, description: e.target.value })}
                  ></textarea>
                  {errorDescription !== "" ? <div className="error">{errorDescription}</div> : ""}
                </div>

                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Người sửa</label>
                      <input type="text" className="form-control" value="Hoàng Thanh Quang" disabled />
                      {/* defaultValue={details.updated_user.first_name + " " + details.created_user.last_name} */}
                    </div>
                  </div>
                  <div class="col-sm-1"></div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Ngày sửa</label>
                      <input type="text" className="form-control" value="08/14/2022" disabled />
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
                <button type="submit" className="btn btn-primary">
                  <i className="nav-icon fas fa-pen" /> Cập nhật chức danh
                </button>
                <button type="button" className="btn btn-danger float-right" onClick={(e) => deleteHandle(e, details.id)}>
                  Xóa chức danh
                </button>
              </div>
            </form>
          </div>
        </section>
        <section className="content">
          <StaffList url={url} />
        </section>
      </div>
    </div>
  );
};

export default TitleDetailPage;
