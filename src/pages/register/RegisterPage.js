import React, { useState } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import { useNavigate } from "react-router-dom";

async function registerUser(details) {
  var config = {
    method: "post",
    url: AppEnvironment.urlBaseApi + "auth/register",
    headers: AppEnvironment.headers,
    data: {
      first_name: details.firstName,
      last_name: details.lastName,
      email: details.email,
      mobile_phone: details.phone,
      password: details.password,
      password_confirm: details.rePassword,
      image: "",
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const RegisterPage = () => {
  const [details, setDetails] = useState({ firstName: "", lastName: "", email: "", phone: "", password: "", rePassword: "" });

  const [errorFirstName, setErrorFirstName] = useState("");
  const [errorLastName, setErrorLastName] = useState("");
  const [errorEmail, setErrorEmail] = useState("");
  const [errorPhone, setErrorPhone] = useState("");
  const [errorPassword, setErrorPassword] = useState("");
  const [errorRePassword, setErrorRePassword] = useState("");

  const navigate = useNavigate();

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await registerUser(details);
    let code = response.code;
    if (code === 0) {
      localStorage.setItem("email", details.email);
      navigate("/register/verify-otp");
    } else if (code === 2) {
      if (response.messageObject.first_name) {
        setErrorFirstName(response.messageObject.first_name[0]);
      } else {
        setErrorFirstName("");
      }

      if (response.messageObject.last_name) {
        setErrorLastName(response.messageObject.last_name[0]);
      } else {
        setErrorLastName("");
      }

      if (response.messageObject.email) {
        setErrorEmail(response.messageObject.email[0]);
      } else {
        setErrorEmail("");
      }

      if (response.messageObject.mobile_phone) {
        setErrorPhone(response.messageObject.mobile_phone[0]);
      } else {
        setErrorPhone("");
      }

      if (response.messageObject.password) {
        setErrorPassword(response.messageObject.password[0]);
      } else {
        setErrorPassword("");
      }

      if (response.messageObject.password_confirm) {
        setErrorRePassword(response.messageObject.password_confirm[0]);
      } else {
        setErrorRePassword("");
      }
    }
  };

  return (
    <div className="register-page">
      <div className="register-box">
        <div className="card card-outline card-primary">
          <div className="card-header text-center">
            <a href="/" className="h1">
              <b>Quản lý nhân sự</b>
            </a>
          </div>
          <div className="card-body">
            <p className="login-box-msg">Đăng ký tài khoản mới!</p>
            <form onSubmit={submitHanble}>
              <div className="input-group mb-1 mt-3">
                <input type="text" className="form-control" placeholder="Họ và tên đệm" onChange={(e) => setDetails({ ...details, firstName: e.target.value })} />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-user" />
                  </div>
                </div>
              </div>
              {errorFirstName !== "" ? <div className="error">{errorFirstName}</div> : ""}

              <div className="input-group mb-1 mt-3">
                <input type="text" className="form-control" placeholder="Tên" onChange={(e) => setDetails({ ...details, lastName: e.target.value })} />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-user" />
                  </div>
                </div>
              </div>
              {errorLastName !== "" ? <div className="error">{errorLastName}</div> : ""}

              <div className="input-group mb-1 mt-3">
                <input type="text" className="form-control" placeholder="Email" onChange={(e) => setDetails({ ...details, email: e.target.value })} />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-envelope" />
                  </div>
                </div>
              </div>
              {errorEmail !== "" ? <div className="error">{errorEmail}</div> : ""}

              <div className="input-group mb-1 mt-3">
                <input type="text" className="form-control" placeholder="Số điện thoại (không bắt buộc)" onChange={(e) => setDetails({ ...details, phone: e.target.value })} />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-envelope" />
                  </div>
                </div>
              </div>
              {errorPhone !== "" ? <div className="error">{errorPhone}</div> : ""}

              <div className="input-group mb-1 mt-3">
                <input type="password" className="form-control" placeholder="Mật khẩu" onChange={(e) => setDetails({ ...details, password: e.target.value })} />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock" />
                  </div>
                </div>
              </div>
              {errorPassword !== "" ? <div className="error">{errorPassword}</div> : ""}

              <div className="input-group mb-1 mt-3">
                <input type="password" className="form-control" placeholder="Xác nhận mật khẩu" onChange={(e) => setDetails({ ...details, rePassword: e.target.value })} />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock" />
                  </div>
                </div>
              </div>
              {errorRePassword !== "" ? <div className="error">{errorRePassword}</div> : ""}

              <div className="row mt-4">
                <div className="col-8">
                  <div className="icheck-primary">
                    <a href="/" className="text-center mt-2">
                      Tôi đã có tài khoản
                    </a>
                  </div>
                </div>
                <div className="col-4">
                  <button type="submit" className="btn btn-primary btn-block">
                    Đăng ký
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
export default RegisterPage;
