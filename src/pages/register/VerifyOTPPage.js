import React, { useState } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import { useNavigate } from "react-router-dom";

async function verifiOTP(details) {
  var config = {
    method: "put",
    url: AppEnvironment.urlBaseApi + "auth/register/verify-email",
    headers: AppEnvironment.headers,
    data: {
      email: details.email,
      otp_code: details.otp,
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

async function resendOTP(details) {
  var config = {
    method: "put",
    url: AppEnvironment.urlBaseApi + "auth/register/resend-otp",
    headers: AppEnvironment.headers,
    data: {
      email: details.email,
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const VerifiOTPPage = () => {
  const [details, setDetails] = useState({ email: localStorage.getItem("email"), otp: "" });
  const [errorOTP, setErrorOTP] = useState("");
  const navigate = useNavigate();

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await verifiOTP(details);
    let code = response.code;
    if (code === 0) {
      navigate("/register-success/verify-otp");
    } else if (code === 2) {
      if (response.messageObject.otp_code) {
        setErrorOTP(response.messageObject.otp_code[0]);
      } else {
        setErrorOTP("");
      }
    }
  };

  const resendOtp = async (e) => {
    e.preventDefault();
    let response = await resendOTP(details);
    console.log(response);
  };

  return (
    <div className="login-page">
      <div className="login-box">
        <div className="card card-outline card-primary">
          <div className="card-header text-center">
            <a href="/" className="h1">
              <b>Quản lý nhân sự</b>
            </a>
          </div>
          <div className="card-body">
            <p className="login-box-msg">
              <b>{localStorage.getItem("email")}</b>
            </p>
            <form onSubmit={submitHanble}>
              <div className="input-group mb-1">
                <input
                  type="text"
                  className="form-control center"
                  placeholder="Mã OTP nhận từ email"
                  style={{ textAlign: "center" }}
                  onChange={(e) => setDetails({ ...details, otp: e.target.value })}
                />
              </div>
              {errorOTP !== "" ? (
                <div className="error" style={{ textAlign: "center" }}>
                  {errorOTP}
                </div>
              ) : (
                ""
              )}

              <div className="row mt-3">
                <div className="col-12">
                  <button type="submit" className="btn btn-primary btn-block">
                    Xác nhận OTP
                  </button>
                </div>
              </div>
            </form>
            <p className="mt-3 mb-1">
              <a href="/">Đăng nhập</a>
              <a href="" className="float-right" onClick={resendOtp}>
                Gửi lại mã OTP
              </a>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
export default VerifiOTPPage;
