import React from "react";
import { useNavigate } from "react-router-dom";

const SuccessVerifyPage = () => {
  const navigate = useNavigate();

  const backLogin = async (e) => {
    e.preventDefault();
    localStorage.removeItem("email");
    navigate("/");
  };

  return (
    <div className="login-page">
      <div className="login-box">
        <div className="card card-outline card-primary">
          <div className="card-header text-center">
            <a href="/" className="h1" onClick={backLogin}>
              <b>Quản lý nhân sự</b>
            </a>
          </div>
          <div className="card-body">
            <p className="login-box-msg">
              <b>{localStorage.getItem("email")}</b>
            </p>
            <p className="login-box-msg">
              <b>Đăng ký thành công</b>
            </p>
            <p className="mt-3 login-box-msg">
              <a href="/" onClick={backLogin}>
                Đăng nhập để sử dụng
              </a>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
export default SuccessVerifyPage;
