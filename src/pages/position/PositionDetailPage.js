import React, { useState } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import { useNavigate } from "react-router-dom";
import helpers from "../../helpers";

import StaffList from "../../components/staff/StaffList";

async function updatePosition(details) {
  var config = {
    method: "put",
    url: AppEnvironment.urlBaseApi + "position/update",
    headers: AppEnvironment.headers,
    data: {
      position_id: details.id,
      position_name: details.positionName,
      daily_wage: details.dailyWage,
      priority: details.priority,
      description: details.description,
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const PositionDetailPage = () => {
  var position = JSON.parse(localStorage.getItem("position"));
  const business = helpers.getBusiness();
  const businessLocationId = helpers.getBusinessLocationId();

  const [details, setDetails] = useState({
    id: position.id,
    positionCode: position.position_code,
    positionName: position.position_name,
    dailyWage: position.daily_wage,
    priority: position.priority ?? "",
    description: position.description ?? "",
  });
  const [errorPositionName, setErrorPositionName] = useState("");
  const [errorDailyWage, setErrorDailyWage] = useState("");
  const [errorPriority, setErrorPriority] = useState("");
  const [errorDescription, setErrorDescription] = useState("");

  const navigate = useNavigate();

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await updatePosition(details);
    let code = response.code;
    if (code === 0) {
      position.position_name = details.positionName;
      position.daily_wage = details.dailyWage;
      position.priority = details.priority;
      position.description = details.description;

      localStorage.setItem("position", JSON.stringify(position));
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.position_name) {
        setErrorPositionName(response.messageObject.position_name[0]);
      } else {
        setErrorPositionName("");
      }

      if (response.messageObject.daily_wage) {
        setErrorDailyWage(response.messageObject.daily_wage[0]);
      } else {
        setErrorDailyWage("");
      }

      if (response.messageObject.priority) {
        setErrorPriority(response.messageObject.priority[0]);
      } else {
        setErrorPriority("");
      }

      if (response.messageObject.description) {
        setErrorDescription(response.messageObject.description[0]);
      } else {
        setErrorDescription("");
      }
    }
  };

  const deleteHandle = (e, id) => {
    e.preventDefault();
    const config = {
      method: "delete",
      url: AppEnvironment.urlBaseApi + "position/destroy",
      headers: AppEnvironment.headers,
      data: {
        position_id: id,
      },
    };

    axios(config)
      .then((res) => {
        localStorage.removeItem("position");
        navigate("/position");
        window.location.reload();
      })
      .catch((err) => console.log(err));
  };

  const url =
    AppEnvironment.urlBaseApi +
    "staff?status=1&page=1&attribute_name=position&attribute_id=" +
    position.id +
    "&business_id=" +
    business.id +
    "&business_location_id=" +
    businessLocationId;

  return (
    <div className="App">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Chức vụ</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <i className="nav-icon fas fa-users" /> Nhân viên
                  </li>
                  <li className="breadcrumb-item active">Chức vụ</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Sửa chức vụ</h3>
            </div>
            <form onSubmit={submitHanble}>
              <div className="card-body">
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">Mã chức vụ</label>
                  <input type="text" className="form-control" defaultValue={details.positionCode} disabled />
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">
                    Tên chức vụ <span className="require-field">*</span>
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Nhập tên chức vụ"
                    defaultValue={details.positionName}
                    onChange={(e) => setDetails({ ...details, positionName: e.target.value })}
                  />
                  {errorPositionName !== "" ? <div className="error">{errorPositionName}</div> : ""}
                </div>

                <div class="row">
                  <div class="col-sm-4">
                    <div className="form-group">
                      <label htmlFor="exampleInputEmail1">
                        Lương ngày <span className="require-field">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nhập lương ngày"
                        defaultValue={details.dailyWage}
                        onChange={(e) => setDetails({ ...details, dailyWage: e.target.value })}
                      />
                      {errorDailyWage !== "" ? <div className="error">{errorDailyWage}</div> : ""}
                    </div>
                  </div>
                  <div class="col-sm-1"></div>
                  <div class="col-sm-4">
                    <div className="form-group">
                      <label htmlFor="exampleInputEmail1">
                        Độ ưu tiên (1-10) <span className="require-field">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nhập độ ưu tiên"
                        defaultValue={details.priority}
                        onChange={(e) => setDetails({ ...details, priority: e.target.value })}
                      />
                      {errorPriority !== "" ? <div className="error">{errorPriority}</div> : ""}
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label>Mô tả</label>
                  <textarea
                    class="form-control"
                    rows="3"
                    placeholder="Nhập mô tả"
                    defaultValue={details.description}
                    onChange={(e) => setDetails({ ...details, description: e.target.value })}
                  ></textarea>
                  {errorDescription !== "" ? <div className="error">{errorDescription}</div> : ""}
                </div>

                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Người sửa</label>
                      <input type="text" className="form-control" value="Hoàng Thanh Quang" disabled />
                      {/* defaultValue={details.updated_user.first_name + " " + details.created_user.last_name} */}
                    </div>
                  </div>
                  <div class="col-sm-1"></div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Ngày sửa</label>
                      <input type="text" className="form-control" value="08/14/2022" disabled />
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
                <button type="submit" className="btn btn-primary">
                  <i className="nav-icon fas fa-pen" /> Cập nhật chức vụ
                </button>
                <button type="button" className="btn btn-danger float-right" onClick={(e) => deleteHandle(e, details.id)}>
                  Xóa chức vụ
                </button>
              </div>
            </form>
          </div>
        </section>
        <section className="content">
          <StaffList url={url} />
        </section>
      </div>
    </div>
  );
};

export default PositionDetailPage;
