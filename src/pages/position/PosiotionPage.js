import React, { useState } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";

import PositionListTable from "../../components/position/PositionListTable";

const business = helpers.getBusiness();
const user = helpers.getUser();

async function createPosition(details) {
  var config = {
    method: "post",
    url: AppEnvironment.urlBaseApi + "position/store",
    headers: AppEnvironment.headers,
    data: {
      business_id: business.id,
      position_name: details.positionName,
      daily_wage: details.dailyWage,
      priority: details.priority,
      description: details.description,
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const PositionPage = () => {
  const [details, setDetails] = useState({ positionName: "", dailyWage: "", priority: "", description: "" });

  const [errorPositionName, setErrorPositionName] = useState("");
  const [errorDailyWage, setErrorDailyWage] = useState("");
  const [errorPriority, setErrorPriority] = useState("");
  const [errorDescription, setErrorDescription] = useState("");

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await createPosition(details);
    let code = response.code;
    if (code === 0) {
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.position_name) {
        setErrorPositionName(response.messageObject.position_name[0]);
      } else {
        setErrorPositionName("");
      }

      if (response.messageObject.daily_wage) {
        setErrorDailyWage(response.messageObject.daily_wage[0]);
      } else {
        setErrorDailyWage("");
      }

      if (response.messageObject.priority) {
        setErrorPriority(response.messageObject.priority[0]);
      } else {
        setErrorPriority("");
      }

      if (response.messageObject.description) {
        setErrorDescription(response.messageObject.description[0]);
      } else {
        setErrorDescription("");
      }
    }
  };

  return (
    <div className="App">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Chức vụ</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <i className="nav-icon fas fa-users" /> Nhân viên
                  </li>
                  <li className="breadcrumb-item active">Chức vụ</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Thêm chức vụ</h3>
            </div>
            <form onSubmit={submitHanble}>
              <div className="card-body">
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">Tên chức vụ <span className="require-field">*</span></label>
                  <input type="text" className="form-control" placeholder="Nhập tên phòng ban" onChange={(e) => setDetails({ ...details, positionName: e.target.value })} />
                  {errorPositionName !== "" ? <div className="error">{errorPositionName}</div> : ""}
                </div>

                <div class="row">
                  <div class="col-sm-4">
                    <div className="form-group">
                      <label htmlFor="exampleInputEmail1">Lương ngày <span className="require-field">*</span></label>
                      <input type="text" className="form-control" placeholder="Nhập lương ngày" onChange={(e) => setDetails({ ...details, dailyWage: e.target.value })} />
                      {errorDailyWage !== "" ? <div className="error">{errorDailyWage}</div> : ""}
                    </div>
                  </div>
                  <div class="col-sm-1"></div>
                  <div class="col-sm-4">
                    <div className="form-group">
                      <label htmlFor="exampleInputEmail1">Độ ưu tiên (1-10) <span className="require-field">*</span></label>
                      <input type="text" className="form-control" placeholder="Nhập độ ưu tiên" onChange={(e) => setDetails({ ...details, priority: e.target.value })} />
                      {errorPriority !== "" ? <div className="error">{errorPriority}</div> : ""}
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label>Mô tả</label>
                  <textarea class="form-control" rows="3" placeholder="Nhập mô tả" onChange={(e) => setDetails({ ...details, description: e.target.value })}></textarea>
                  {errorDescription !== "" ? <div className="error">{errorDescription}</div> : ""}
                </div>

                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Người tạo</label>
                      <input type="text" className="form-control" placeholder={user.first_name + " " + user.last_name} disabled />
                    </div>
                  </div>
                  <div class="col-sm-1"></div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Ngày tạo</label>
                      <input type="text" className="form-control" placeholder={helpers.getCurrentDay()} disabled />
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
                <button type="submit" className="btn btn-primary">
                  <i className="nav-icon fas fa-plus" /> Tạo chức vụ
                </button>
              </div>
            </form>
          </div>
        </section>
        <section className="content">
          <PositionListTable />
        </section>
      </div>
    </div>
  );
};

export default PositionPage;
