import React, { useState } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import { useNavigate } from "react-router-dom";
import helpers from "../../helpers";

import StaffList from "../../components/staff/StaffList";

async function updateStaffType(details) {
  var config = {
    method: "put",
    url: AppEnvironment.urlBaseApi + "staff-type/update",
    headers: AppEnvironment.headers,
    data: {
      staff_type_id: details.id,
      staff_type_name: details.staffTypeName,
      daily_wage: details.dailyWage,
      priority: details.priority,
      description: details.description,
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const StaffTypeDetailPage = () => {
  var staffType = JSON.parse(localStorage.getItem("staffType"));
  const business = helpers.getBusiness();
  const businessLocationId = helpers.getBusinessLocationId();
  
  const [details, setDetails] = useState({
    id: staffType.id,
    staffTypeCode: staffType.staff_type_code,
    staffTypeName: staffType.staff_type_name,
    dailyWage: staffType.daily_wage,
    priority: staffType.priority ?? "",
    description: staffType.description ?? "",
  });
  const [errorStaffTypeName, setErrorStaffTypeName] = useState("");
  const [errorDescription, setErrorDescription] = useState("");

  const navigate = useNavigate();

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await updateStaffType(details);
    let code = response.code;
    if (code === 0) {
      staffType.staff_type_name = details.staffTypeName;
      staffType.daily_wage = details.dailyWage;
      staffType.priority = details.priority;
      staffType.description = details.description;

      localStorage.setItem("staffType", JSON.stringify(staffType));
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.staff_type_name) {
        setErrorStaffTypeName(response.messageObject.staff_type_name[0]);
      } else {
        setErrorStaffTypeName("");
      }

      if (response.messageObject.description) {
        setErrorDescription(response.messageObject.description[0]);
      } else {
        setErrorDescription("");
      }
    }
  };

  const deleteHandle = (e, id) => {
    e.preventDefault();
    const config = {
      method: "delete",
      url: AppEnvironment.urlBaseApi + "staff-type/destroy",
      headers: AppEnvironment.headers,
      data: {
        staff_type_id: id,
      },
    };

    axios(config)
      .then((res) => {
        localStorage.removeItem("staffType");
        navigate("/staff-type");
        window.location.reload();
      })
      .catch((err) => console.log(err));
  };

  const url =
    AppEnvironment.urlBaseApi +
    "staff?status=1&page=1&attribute_name=staff-type&attribute_id=" +
    staffType.id +
    "&business_id=" +
    business.id +
    "&business_location_id=" +
    businessLocationId;

  return (
    <div className="App">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Loại nhân viên</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <i className="nav-icon fas fa-users" /> Nhân viên
                  </li>
                  <li className="breadcrumb-item active">Loại nhân viên</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Sửa loại nhân viên</h3>
            </div>
            <form onSubmit={submitHanble}>
              <div className="card-body">
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">Mã loại nhân viên</label>
                  <input type="text" className="form-control" defaultValue={details.staffTypeCode} disabled />
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">
                    Tên loại nhân viên <span className="require-field">*</span>
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Nhập tên loại nhân viên"
                    defaultValue={details.staffTypeName}
                    onChange={(e) => setDetails({ ...details, staffTypeName: e.target.value })}
                  />
                  {errorStaffTypeName !== "" ? <div className="error">{errorStaffTypeName}</div> : ""}
                </div>

                <div class="form-group">
                  <label>Mô tả</label>
                  <textarea
                    class="form-control"
                    rows="3"
                    placeholder="Nhập mô tả"
                    defaultValue={details.description}
                    onChange={(e) => setDetails({ ...details, description: e.target.value })}
                  ></textarea>
                  {errorDescription !== "" ? <div className="error">{errorDescription}</div> : ""}
                </div>

                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Người sửa</label>
                      <input type="text" className="form-control" value="Hoàng Thanh Quang" disabled />
                      {/* defaultValue={details.updated_user.first_name + " " + details.created_user.last_name} */}
                    </div>
                  </div>
                  <div class="col-sm-1"></div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Ngày sửa</label>
                      <input type="text" className="form-control" value="08/14/2022" disabled />
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
                <button type="submit" className="btn btn-primary">
                  <i className="nav-icon fas fa-pen" /> Cập nhật loại nhân viên
                </button>
                <button type="button" className="btn btn-danger float-right" onClick={(e) => deleteHandle(e, details.id)}>
                  Xóa loại nhân viên
                </button>
              </div>
            </form>
          </div>
        </section>
        <section className="content">
          <StaffList url={url} />
        </section>
      </div>
    </div>
  );
};

export default StaffTypeDetailPage;
