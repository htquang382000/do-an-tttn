import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import { useNavigate } from "react-router-dom";
import helpers from "../../helpers";
import Loading from "../../components/Loading";
import StaffList from "../../components/staff/StaffList";

async function updateCertification(details) {
  var config = {
    method: "put",
    url: AppEnvironment.urlBaseApi + "certification/update",
    headers: AppEnvironment.headers,
    data: {
      certification_id: details.id,
      certification_name: details.certificationName,
      description: details.description,
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const CertificationDetailPage = () => {
  var certification = JSON.parse(localStorage.getItem("certification"));
  const business = helpers.getBusiness();
  const businessLocationId = helpers.getBusinessLocationId();

  const [details, setDetails] = useState({
    id: certification.id,
    certificationCode: certification.certification_code,
    certificationName: certification.certification_name,
    description: certification.description ?? "",
  });
  const [errorCertificationName, setErrorCertificationName] = useState("");
  const [errorDescription, setErrorDescription] = useState("");

  const navigate = useNavigate();

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await updateCertification(details);
    let code = response.code;
    if (code === 0) {
      certification.certification_name = details.certificationName;
      certification.description = details.description;

      localStorage.setItem("certification", JSON.stringify(certification));
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.certification_name) {
        setErrorCertificationName(response.messageObject.certification_name[0]);
      } else {
        setErrorCertificationName("");
      }

      if (response.messageObject.description) {
        setErrorDescription(response.messageObject.description[0]);
      } else {
        setErrorDescription("");
      }
    }
  };

  const deleteHandle = (e, id) => {
    e.preventDefault();
    const config = {
      method: "delete",
      url: AppEnvironment.urlBaseApi + "certification/destroy",
      headers: AppEnvironment.headers,
      data: {
        certification_id: id,
      },
    };

    axios(config)
      .then((res) => {
        localStorage.removeItem("certification");
        navigate("/certification");
        window.location.reload();
      })
      .catch((err) => console.log(err));
  };

  const url =
    AppEnvironment.urlBaseApi +
    "staff?status=1&page=1&attribute_name=certification&attribute_id=" +
    certification.id +
    "&business_id=" +
    business.id +
    "&business_location_id=" +
    businessLocationId;

  return (
    <div className="App">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Bằng cấp</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <i className="nav-icon fas fa-users" /> Nhân viên
                  </li>
                  <li className="breadcrumb-item active">Bằng cấp</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Sửa bằng cấp</h3>
            </div>
            <form onSubmit={submitHanble}>
              <div className="card-body">
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">Mã bằng cấp:</label>
                  <input type="text" className="form-control" defaultValue={details.certificationCode} disabled />
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">Tên bằng cấp:</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Nhập tên bằng cấp"
                    defaultValue={details.certificationName}
                    onChange={(e) => setDetails({ ...details, certificationName: e.target.value })}
                  />
                  {errorCertificationName !== "" ? <div className="error">{errorCertificationName}</div> : ""}
                </div>

                <div class="form-group">
                  <label>Mô tả (không bắt buộc)</label>
                  <textarea
                    class="form-control"
                    rows="3"
                    placeholder="Nhập mô tả"
                    defaultValue={details.description}
                    onChange={(e) => setDetails({ ...details, description: e.target.value })}
                  ></textarea>
                  {errorDescription !== "" ? <div className="error">{errorDescription}</div> : ""}
                </div>

                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Người sửa</label>
                      <input type="text" className="form-control" value="Hoàng Thanh Quang" disabled />
                      {/* defaultValue={details.updated_user.first_name + " " + details.created_user.last_name} */}
                    </div>
                  </div>
                  <div class="col-sm-1"></div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Ngày sửa</label>
                      <input type="text" className="form-control" value="08/14/2022" disabled />
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
                <button type="submit" className="btn btn-primary">
                  <i className="nav-icon fas fa-pen" /> Cập nhật trình độ
                </button>
                <button type="button" className="btn btn-danger float-right" onClick={(e) => deleteHandle(e, details.id)}>
                  Xóa trình độ
                </button>
              </div>
            </form>
          </div>
        </section>
        <section className="content">
          <StaffList url={url} />
        </section>
      </div>
    </div>
  );
};

export default CertificationDetailPage;
