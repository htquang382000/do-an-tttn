import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
import Select from "react-select";
import UserListTable from "../../components/user/UserListTable";
import Loading from "../../components/Loading";

const LevelPage = () => {
  const business = helpers.getBusiness();
  const user = helpers.getUser();

  const [loading, setLoading] = useState(true);
  const [details, setDetails] = useState({ firstName: "", lastName: "", email: "", mobilePhone: "", businessLocation: "", status: 1, image: "" });

  const [businessLocationList, setBusinessLocationList] = useState([]);

  const [errorFirstName, setErrorFirstName] = useState("");
  const [errorLastName, setErrorLastName] = useState("");
  const [errorEmail, setErrorEmail] = useState("");
  const [errorMobilePhone, setErrorMobilePhone] = useState("");
  const [errorBusinessLocation, setErrorBusinessLocation] = useState("");
  const [errorStatus, setErrorStatus] = useState("");
  const [errorImage, setErrorImage] = useState("");

  async function getBusinessLocationList() {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "business-location?business_id=" + business.id,
      headers: AppEnvironment.headers,
    };

    return axios(config)
      .then((res) => res.data.data)
      .catch((err) => console.log(err));
  }

  useEffect(() => {
    async function fetchData() {
      const data = await getBusinessLocationList();
      const businessLocationList = data.map((data) => ({
        value: data.id,
        label: data.business_location_name + " (" + data.address + ", " + data.ward.name + ", " + data.district.name + ", " + data.city.name + ")",
      }));

      setBusinessLocationList(businessLocationList);
      setLoading(false);
    }

    fetchData();
  }, []);

  async function createUser(details) {
    var config = {
      method: "post",
      url: AppEnvironment.urlBaseApi + "user/store",
      headers: AppEnvironment.headers,
      data: {
        business_id: business.id,
        first_name: details.firstName,
        last_name: details.lastName,
        email: details.email,
        mobile_phone: details.mobilePhone,
        business_location_id: details.businessLocationId,
        status: details.status,
        image: details.image,
      },
    };

    return axios(config)
      .then((response) => response.data)
      .catch(function (error) {
        console.log(error);
      });
  }

  const submitHanble = async (e) => {
    e.preventDefault();
    console.log(details);
    let response = await createUser(details);
    console.log(response);
    let code = response.code;
    if (code === 0) {
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.first_name) {
        setErrorFirstName(response.messageObject.first_name[0]);
      } else {
        setErrorFirstName("");
      }

      if (response.messageObject.last_name) {
        setErrorLastName(response.messageObject.last_name[0]);
      } else {
        setErrorLastName("");
      }

      if (response.messageObject.email) {
        setErrorEmail(response.messageObject.email[0]);
      } else {
        setErrorEmail("");
      }

      if (response.messageObject.mobile_phone) {
        setErrorMobilePhone(response.messageObject.mobile_phone[0]);
      } else {
        setErrorMobilePhone("");
      }

      if (response.messageObject.business_location_id) {
        setErrorBusinessLocation(response.messageObject.business_location_id[0]);
      } else {
        setErrorBusinessLocation("");
      }

      if (response.messageObject.status) {
        setErrorStatus(response.messageObject.status[0]);
      } else {
        setErrorStatus("");
      }

      if (response.messageObject.image) {
        setErrorImage(response.messageObject.image[0]);
      } else {
        setErrorImage("");
      }
    }
  };

  return loading ? (
    <div className="App">
      <div className="content-wrapper">
        <Loading></Loading>
      </div>
    </div>
  ) : (
    <div className="App">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Tài khoản</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <i className="nav-icon fas fa-house-user" /> Tổng quan
                  </li>
                  <li className="breadcrumb-item active">Danh sách tài khoản</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Thêm tài khoản</h3>
            </div>
            <form onSubmit={submitHanble}>
              <div className="card-body">
                <div className="row">
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Ảnh</label>
                      <input type="file" className="custom-file-input" accept=".jpg,.png" onChange={(e) => setDetails({ ...details, image: e.target.files[0] })} />
                      <label className="custom-file-label custom-add-file">Tìm kiếm</label>
                      {errorImage !== "" ? <div className="error">{errorImage}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label htmlFor="exampleInputEmail1">
                        Họ và tên đệm: <span className="require-field">*</span>
                      </label>
                      <input type="text" className="form-control" placeholder="Nhập họ và tên đệm" onChange={(e) => setDetails({ ...details, firstName: e.target.value })} />
                      {errorFirstName !== "" ? <div className="error">{errorFirstName}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label htmlFor="exampleInputEmail1">
                        Tên: <span className="require-field">*</span>
                      </label>
                      <input type="text" className="form-control" placeholder="Nhập tên" onChange={(e) => setDetails({ ...details, lastName: e.target.value })} />
                      {errorLastName !== "" ? <div className="error">{errorLastName}</div> : ""}
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label htmlFor="exampleInputEmail1">
                        Email: <span className="require-field">*</span>
                      </label>
                      <input type="text" className="form-control" placeholder="Nhập email đăng nhập" onChange={(e) => setDetails({ ...details, email: e.target.value })} />
                      {errorEmail !== "" ? <div className="error">{errorEmail}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label htmlFor="exampleInputEmail1">Số điện thoại:</label>
                      <input type="text" className="form-control" placeholder="Nhập số điện thoại" onChange={(e) => setDetails({ ...details, mobilePhone: e.target.value })} />
                      {errorMobilePhone !== "" ? <div className="error">{errorMobilePhone}</div> : ""}
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label htmlFor="exampleInputEmail1">
                        Chi nhánh doanh nghiệp: <span className="require-field">*</span>
                      </label>
                      <Select
                        isMulti
                        options={businessLocationList}
                        isClearable={true}
                        isSearchable={true}
                        placeholder="-- Chọn chi nhánh --"
                        onChange={(e) => setDetails({ ...details, businessLocationId: Array.isArray(e) ? e.map((x) => x.value) : [] })}
                      />
                      {errorBusinessLocation !== "" ? <div className="error">{errorBusinessLocation}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label htmlFor="exampleInputEmail1">
                        Trạng thái: <span className="require-field">*</span>
                      </label>
                      <div className="form-group">
                        <div className="form-check mt-2">
                          <input className="form-check-input mr-4" type="radio" name="status" checked onChange={(e) => setDetails({ ...details, status: 1 })} />
                          <label className="form-check-label mr-4">Đang hoạt động</label>
                          <input className="form-check-input ml-4" type="radio" name="status" onChange={(e) => setDetails({ ...details, status: 2 })} />
                          <label className="form-check-label ml-5">Vô hiệu hóa</label>
                          {errorStatus !== "" ? <div className="error">{errorStatus}</div> : ""}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Người tạo</label>
                      <input type="text" className="form-control" placeholder={user.first_name + " " + user.last_name} disabled />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Ngày tạo</label>
                      <input type="text" className="form-control" placeholder={helpers.getCurrentDay()} disabled />
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
                <button type="submit" className="btn btn-primary">
                  <i className="nav-icon fas fa-plus" /> Tạo tài khoản
                </button>
              </div>
            </form>
          </div>
        </section>
        <section className="content">
          <UserListTable />
        </section>
      </div>
    </div>
  );
};

export default LevelPage;
