import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
import Select from "react-select";
import Loading from "../../components/Loading";

import ProfileDashboard from "../../components/profile/ProfileDashboard";

const UserDetailPage = () => {
  const userId = localStorage.getItem("userId");
  const [loading, setLoading] = useState(true);

  const [details, setDetails] = useState({ firstName: "", lastName: "", email: "", mobilePhone: "", businessLocationId: "", status: "", image: "" });
  const [businessLocationList, setBusinessLocationList] = useState([]);
  const [user, setUser] = useState({});
  const [errorImage, setErrorImage] = useState("");
  const [errorFirstName, setErrorFirstName] = useState("");
  const [errorLastName, setErrorLastName] = useState("");
  const [errorPhone, setErrorPhone] = useState("");

  async function getBusinessLocationList() {
    const business = helpers.getBusiness();
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "business-location?business_id=" + business.id,
      headers: AppEnvironment.headers,
    };

    return axios(config)
      .then((res) => res.data.data)
      .catch((err) => console.log(err));
  }

  useEffect(() => {
    async function fetchData() {
      const data = await getBusinessLocationList();
      const businessLocationList = data.map((data) => ({
        value: data.id,
        label: data.business_location_name + " (" + data.address + ", " + data.ward.name + ", " + data.district.name + ", " + data.city.name + ")",
      }));
      setBusinessLocationList(businessLocationList);
    }

    fetchData();
  }, []);

  async function updateUser(details) {
    var config = {
      method: "put",
      url: AppEnvironment.urlBaseApi + "user/update",
      headers: AppEnvironment.headers,
      data: {
        user_id: userId,
        image: details.image,
        first_name: details.firstName,
        last_name: details.lastName,
        mobile_phone: details.mobilePhone,
        status: details.status,
        business_location_id: details.businessLocationId,
      },
    };

    return axios(config)
      .then((response) => response.data)
      .catch(function (error) {
        console.log(error);
      });
  }

  useEffect(() => {
    var config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "user/detail?user_id=" + userId,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        const userData = res.data.data;
        setUser(userData);
        setDetails({
          ...details,
          image: userData.image,
          firstName: userData.first_name,
          lastName: userData.last_name,
          mobilePhone: userData.mobile_phone,
          businessLocationId:
            userData.user_business_location && Array.isArray(userData.user_business_location)
              ? userData.user_business_location.map((userBusinessLocation) => userBusinessLocation.business_location_id)
              : [],
          status: userData.status,
        });
        setLoading(false);
      })
      .catch((err) => console.log(err));
  }, []);

  const submitHanble = async (e) => {
    e.preventDefault();
    console.log(details);
    let response = await updateUser(details);
    console.log(response);
    let code = response.code;
    if (code === 0) {
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.image) {
        setErrorImage(response.messageObject.image[0]);
      } else {
        setErrorImage("");
      }

      if (response.messageObject.first_name) {
        setErrorFirstName(response.messageObject.first_name[0]);
      } else {
        setErrorFirstName("");
      }

      if (response.messageObject.last_name) {
        setErrorLastName(response.messageObject.last_name[0]);
      } else {
        setErrorLastName("");
      }

      if (response.messageObject.mobile_phone) {
        setErrorPhone(response.messageObject.mobile_phone[0]);
      } else {
        setErrorPhone("");
      }
    }
  };

  return loading ? (
    <div className="App">
      <div className="content-wrapper">
        <Loading></Loading>
      </div>
    </div>
  ) : (
    <div className="App">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Chi tiết tài khoản</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <i className="nav-icon fas fa-house-user" /> Tổng quan
                  </li>
                  <li className="breadcrumb-item active">
                    <a href="/user" className="text-secondary">
                      Danh sách tải khoản
                    </a>
                  </li>
                  <li className="breadcrumb-item active">Chi tiết</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-3">
                <ProfileDashboard user={user}></ProfileDashboard>
              </div>
              <div className="col-md-9">
                <div className="card card-primary">
                  <div className="card-header">
                    <h3 className="card-title">Thay đổi thông tin</h3>
                  </div>
                  <form onSubmit={submitHanble}>
                    <div className="card-body">
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Chọn ảnh</label>
                            <input type="file" className="custom-file-input" onChange={(e) => setDetails({ ...details, image: e.target.value ?? "" })} />
                            <label className="custom-file-label custom-add-file">Tìm kiếm</label>
                            {errorImage !== "" ? <div className="error">{errorImage}</div> : ""}
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <img
                          className="profile-user-img img-fluid img-circle"
                          src={user.image ?? "https://png.pngtree.com/element_our/20200610/ourlarge/pngtree-default-avatar-image_2237213.jpg"}
                          alt="User profile picture"
                        />
                      </div>

                      <div className="row">
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label htmlFor="exampleInputEmail1">
                              Họ và tên đệm <span className="require-field">*</span>
                            </label>
                            <input
                              type="text"
                              className="form-control"
                              placeholder="Nhập họ và tên đệm"
                              defaultValue={user.first_name}
                              onChange={(e) => setDetails({ ...details, firstName: e.target.value })}
                            />
                            {errorFirstName !== "" ? <div className="error">{errorFirstName}</div> : ""}
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label htmlFor="exampleInputEmail1">
                              Tên <span className="require-field">*</span>
                            </label>
                            <input
                              type="text"
                              className="form-control"
                              placeholder="Nhập tên"
                              defaultValue={user.last_name}
                              onChange={(e) => setDetails({ ...details, lastName: e.target.value })}
                            />
                            {errorLastName !== "" ? <div className="error">{errorLastName}</div> : ""}
                          </div>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label>Email</label>
                            <input type="text" className="form-control" defaultValue={user.email} disabled />
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label>Số điện thoại</label>
                            <input
                              type="text"
                              className="form-control"
                              placeholder="Nhập số điện thoại"
                              defaultValue={user.mobile_phone}
                              onChange={(e) => setDetails({ ...details, mobilePhone: e.target.value })}
                            />
                            {errorPhone !== "" ? <div className="error">{errorPhone}</div> : ""}
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label htmlFor="exampleInputEmail1">
                              Chi nhánh doanh nghiệp <span className="require-field">*</span>
                            </label>
                            <Select
                              isMulti
                              options={businessLocationList}
                              isClearable={true}
                              isSearchable={true}
                              defaultValue={
                                user.user_business_location && Array.isArray(user.user_business_location)
                                  ? user.user_business_location.map((userBusinessLocation) => ({
                                      value: userBusinessLocation.business_location_id,
                                      label:
                                        userBusinessLocation.business_location.business_location_name +
                                        " (" +
                                        userBusinessLocation.business_location.address +
                                        ", " +
                                        userBusinessLocation.business_location.ward.name +
                                        ", " +
                                        userBusinessLocation.business_location.district.name +
                                        ", " +
                                        userBusinessLocation.business_location.city.name +
                                        ")",
                                    }))
                                  : []
                              }
                              placeholder="-- Chọn chi nhánh --"
                              onChange={(e) => setDetails({ ...details, businessLocationId: Array.isArray(e) ? e.map((x) => x.value) : [] })}
                            />
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label htmlFor="exampleInputEmail1">
                              Trạng thái <span className="require-field">*</span>
                            </label>
                            <div className="form-group">
                              <div className="form-check mt-2">
                                <input
                                  className="form-check-input mr-4"
                                  type="radio"
                                  name="status"
                                  checked={details.status === 1 ? true : false}
                                  onChange={(e) => setDetails({ ...details, status: 1 })}
                                />
                                <label className="form-check-label mr-4">Đang hoạt động</label>
                                <input
                                  className="form-check-input ml-4"
                                  type="radio"
                                  name="status"
                                  checked={details.status === 2 ? true : false}
                                  onChange={(e) => setDetails({ ...details, status: 2 })}
                                />
                                <label className="form-check-label ml-5">Vô hiệu hóa</label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="card-footer">
                      <button type="submit" className="btn btn-primary">
                        <i className="nav-icon fas fa-pen" /> Cập nhật
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};

export default UserDetailPage;
