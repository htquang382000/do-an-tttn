import React, { useState, useEffect } from "react";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
import StaffList from "../../components/comment/StaffList";

const CommentPage = () => {
  const business = helpers.getBusiness();
  const businessLocationId = helpers.getBusinessLocationId();
  const url = AppEnvironment.urlBaseApi + "staff?status=1&page=1&business_id=" + business.id + "&business_location_id=" + businessLocationId;

  return (
    <div className="App">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Danh sách nhân viên</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <i className="nav-icon fas fa-house-user" /> Đánh giá viên viên
                  </li>
                  <li className="breadcrumb-item active">Danh sách nhân viên</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <StaffList url={url} />
        </section>
      </div>
    </div>
  );
};

export default CommentPage;
