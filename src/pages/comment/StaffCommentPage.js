import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
import Loading from "../../components/Loading";

const StaffCommentPage = () => {
  const staff = JSON.parse(localStorage.getItem("staff"));

  const [loading, setLoading] = useState(true);
  const [details, setDetails] = useState({ commentId: "", content: "", type: "" });
  const [commentStaffList, setcommentStaffList] = useState([]);

  const [errorContent, setErrorContent] = useState("");
  const [errorType, setErrorType] = useState("");

  useEffect(() => {
    const business = helpers.getBusiness();
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "comment?staff_id=" + staff.id + "&business_id=" + business.id,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setLoading(false);
        setcommentStaffList(res.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const viewDetail = (e, comment) => {
    setDetails({ ...details, commentId: comment.id, content: comment.content, type: comment.type });
    console.log(details);
    e.preventDefault();
  };

  async function updateComment(details) {
    var config = {
      method: "put",
      url: AppEnvironment.urlBaseApi + "comment/update",
      headers: AppEnvironment.headers,
      data: {
        comment_id: details.commentId,
        content: details.content ?? "",
        type: details.type ?? "",
      },
    };

    return axios(config)
      .then((response) => response.data)
      .catch(function (error) {
        console.log(error);
      });
  }

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await updateComment(details);
    let code = response.code;
    if (code === 0) {
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.content) {
        setErrorContent(response.messageObject.content[0]);
      } else {
        setErrorContent("");
      }

      if (response.messageObject.type) {
        setErrorType(response.messageObject.type[0]);
      } else {
        setErrorType("");
      }
    }
  };

  return loading ? (
    <div className="App">
      <div className="content-wrapper">
        <Loading></Loading>
      </div>
    </div>
  ) : (
    <div className="App">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Danh sách đánh giá</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <i className="nav-icon fas fa-users" /> Nhân viên
                  </li>
                  <li className="breadcrumb-item active">
                    <a href="/comment" className="text-secondary">
                      Đánh giá nhân viên
                    </a>
                  </li>
                  <li className="breadcrumb-item active">Danh sách đánh giá</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="card">
            <div className="card-header">
              <h3 className="card-title">
                <h5>
                  {staff.full_name} - {staff.staff_code}
                </h5>
              </h3>
            </div>
            <div className="card-body">
              {commentStaffList && commentStaffList.length > 0 ? (
                <table id="" className="table table-bordered table-hover dataTable dtr-inline the-table">
                  <thead>
                    <tr>
                      <th style={{ width: "30px" }}>STT</th>
                      <th style={{ width: "250px" }}>Nội dung</th>
                      <th style={{ width: "60px" }}>Loại</th>
                      <th style={{ width: "100px" }}>Người tạo</th>
                      <th style={{ width: "50px" }}>Ngày tạo</th>
                      <th style={{ width: "100px" }}>Người sửa</th>
                      <th style={{ width: "50px" }}>Ngày sửa</th>
                    </tr>
                  </thead>
                  <tbody>
                    {commentStaffList.map((comment, index) => (
                      <tr onClick={(e) => viewDetail(e, comment)} data-toggle="modal" data-target="#updateComment">
                        <td className="text-center">{index + 1}</td>
                        <td>{comment.content}</td>
                        <td>
                          {comment.type ? (
                            comment.type === 1 ? (
                              <span className="badge bg-success" style={{ width: "60px" }}>
                                Tốt
                              </span>
                            ) : (
                              <span className="badge bg-danger" style={{ width: "60px" }}>
                                Xấu
                              </span>
                            )
                          ) : (
                            <span className="badge bg-blue" style={{ width: "60px" }}>
                              Không
                            </span>
                          )}
                        </td>
                        <td>{comment.created_user.first_name + " " + comment.created_user.last_name}</td>
                        <td>{helpers.convertDateTimeForApp(comment.created_at)}</td>
                        <td>{comment.updated_user.first_name + " " + comment.updated_user.last_name}</td>
                        <td>{helpers.convertDateTimeForApp(comment.updated_at)}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              ) : (
                <div className="text-center">Không có đánh giá nào</div>
              )}
            </div>
            {commentStaffList && commentStaffList.length > 0 ? (
              <div className="card-footer">
                <nav aria-label="Contacts Page Navigation">
                  <ul className="pagination justify-content-center m-0">
                    <li className="page-item active">
                      <a className="page-link" href="#">
                        1
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            ) : (
              <></>
            )}
          </div>
          <div className="modal fade" id="updateComment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLongTitle">
                    Thông tin đánh giá nhân viên
                  </h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form onSubmit={submitHanble}>
                  <div className="modal-body">
                    <div className="form-group">
                      <label>Mã nhân viên</label>
                      <input type="text" className="form-control" defaultValue={staff.staff_code} disabled />
                    </div>
                    <div className="form-group">
                      <label>Họ tên nhân viên</label>
                      <input type="text" className="form-control" defaultValue={staff.full_name} disabled />
                    </div>
                    <div className="form-group">
                      <label htmlFor="exampleInputEmail1">
                        Nội dung đánh giá <span className="require-field">*</span>
                      </label>
                      <textarea
                        className="form-control"
                        rows="4"
                        placeholder="Nhập nội dung đánh giá"
                        defaultValue={details.content}
                        onChange={(e) => setDetails({ ...details, content: e.target.value })}
                      ></textarea>
                      {errorContent !== "" ? <div className="error">{errorContent}</div> : ""}
                    </div>
                    <div className="form-group">
                      <label>
                        Loại đánh giá <span className="require-field">*</span>
                      </label>
                      <div className="form-check mt-2">
                        <input
                          className="form-check-input mr-4"
                          type="radio"
                          name="type"
                          checked={details.type === null ? true : false}
                          onChange={(e) => setDetails({ ...details, type: "" })}
                        />
                        <label className="form-check-label mr-4">Không</label>
                        <input
                          className="form-check-input ml-4"
                          type="radio"
                          name="type"
                          checked={details.type === 1 ? true : false}
                          onChange={(e) => setDetails({ ...details, type: 1 })}
                        />
                        <label className="form-check-label ml-5">Đánh giá tốt</label>
                        <input
                          className="form-check-input ml-4"
                          type="radio"
                          name="type"
                          checked={details.type === 2 ? true : false}
                          onChange={(e) => setDetails({ ...details, type: 2 })}
                        />
                        <label className="form-check-label ml-5">Đánh giá xấu</label>
                        {errorType !== "" ? <div className="error">{errorType}</div> : ""}
                      </div>
                    </div>
                  </div>

                  <div className="modal-footer">
                    <button type="submit" className="btn btn-primary">
                      Cập nhật
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};

export default StaffCommentPage;
