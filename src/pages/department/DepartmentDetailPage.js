import React, { useState } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import { useNavigate } from "react-router-dom";
import helpers from "../../helpers";

import StaffList from "../../components/staff/StaffList";

async function updateDepartment(details) {
  var config = {
    method: "put",
    url: AppEnvironment.urlBaseApi + "department/update",
    headers: AppEnvironment.headers,
    data: {
      department_id: details.id,
      department_name: details.departmentName,
      description: details.description,
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const DepartmentDetailPage = () => {
  const business = helpers.getBusiness();
  const businessLocationId = helpers.getBusinessLocationId();
  let department = JSON.parse(localStorage.getItem("department"));

  const [details, setDetails] = useState({
    id: department.id,
    departmentCode: department.department_code,
    departmentName: department.department_name,
    description: department.description ?? "",
  });

  const [errorDepartmentName, setErrorDepartmentName] = useState("");
  const [errorDescription, setErrorDescription] = useState("");
  const navigate = useNavigate();

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await updateDepartment(details);
    let code = response.code;
    if (code === 0) {
      department.department_name = details.departmentName;
      department.description = details.description;

      localStorage.setItem("department", JSON.stringify(department));
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.department_name) {
        setErrorDepartmentName(response.messageObject.department_name[0]);
      } else {
        setErrorDepartmentName("");
      }

      if (response.messageObject.description) {
        setErrorDescription(response.messageObject.description[0]);
      } else {
        setErrorDescription("");
      }
    }
  };

  const deleteHandle = (e, id) => {
    e.preventDefault();
    const config = {
      method: "delete",
      url: AppEnvironment.urlBaseApi + "department/destroy",
      headers: AppEnvironment.headers,
      data: {
        department_id: id,
      },
    };

    axios(config)
      .then((res) => {
        localStorage.removeItem("department");
        navigate("/department");
        window.location.reload();
      })
      .catch((err) => console.log(err));
  };

  const url =
    AppEnvironment.urlBaseApi +
    "staff?status=1&page=1&attribute_name=department&attribute_id=" +
    department.id +
    "&business_id=" +
    business.id +
    "&business_location_id=" +
    businessLocationId;

  return (
    <div className="App">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Phòng ban</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <i className="nav-icon fas fa-users" /> Nhân viên
                  </li>
                  <li className="breadcrumb-item active">Phòng ban</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Sửa phòng ban</h3>
            </div>
            <form onSubmit={submitHanble}>
              <div className="card-body">
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">Mã phòng ban</label>
                  <input type="text" className="form-control" defaultValue={details.departmentCode} disabled />
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">
                    Tên phòng ban <span className="require-field">*</span>
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Nhập tên phòng ban"
                    defaultValue={details.departmentName}
                    onChange={(e) => setDetails({ ...details, departmentName: e.target.value })}
                  />
                  {errorDepartmentName !== "" ? <div className="error">{errorDepartmentName}</div> : ""}
                </div>

                <div className="form-group">
                  <label>Mô tả</label>
                  <textarea
                    className="form-control"
                    rows="3"
                    placeholder="Nhập mô tả"
                    defaultValue={details.description}
                    onChange={(e) => setDetails({ ...details, description: e.target.value })}
                  ></textarea>
                  {errorDescription !== "" ? <div className="error">{errorDescription}</div> : ""}
                </div>

                <div className="row">
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Người sửa</label>
                      <input type="text" className="form-control" value="Hoàng Thanh Quang" disabled />
                    </div>
                  </div>
                  <div className="col-sm-1"></div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label>Ngày sửa</label>
                      <input type="text" className="form-control" value="08/14/2022" disabled />
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
                <button type="submit" className="btn btn-primary">
                  Cập nhật phòng ban
                </button>
                <button type="button" className="btn btn-danger float-right" onClick={(e) => deleteHandle(e, details.id)}>
                  Xóa phòng ban
                </button>
              </div>
            </form>
          </div>
        </section>
        <section className="content">
          <StaffList url={url} />
        </section>
      </div>
    </div>
  );
};

export default DepartmentDetailPage;
