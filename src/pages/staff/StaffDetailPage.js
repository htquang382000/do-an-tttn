import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
import Select from "react-select";
import Loading from "../../components/Loading";
import GeneralApi from "../../api/general/GeneralApi";

const business = helpers.getBusiness();
const staffId = localStorage.getItem("staffId");

async function updateStaff(details) {
  var config = {
    method: "put",
    url: AppEnvironment.urlBaseApi + "staff/update",
    headers: AppEnvironment.headers,
    data: {
      business_id: business.id,
      staff_id: staffId,
      full_name: details.fullName ?? "",
      nick_name: details.nickName ?? "",
      mobile_phone: details.mobilePhone ?? "",
      email: details.email ?? "",
      marital_status: details.maritalStatus ?? "",
      nid: details.nid ?? "",
      date_of_nid: details.dateOfNid ?? "",
      place_of_nid: details.placeOfNidObj ? details.placeOfNidObj.id ?? details.placeOfNidObj.value : "",
      national_id: details.national ? details.national.id ?? details.national.value : "",
      religion_id: details.religion ? details.religion.id ?? details.religion.value : "",
      ethnic_id: details.ethnic ? details.ethnic.id ?? details.ethnic.value : "",
      staff_type_id: details.staffType ? details.staffType.id ?? details.staffType.value : "",
      certification_id: details.certification ? details.certification.id ?? details.certification.value : "",
      status: details.status ?? "",
      image3x4: details.image3x4 ?? "",
      gender: details.gender ?? "",
      day_of_birth: details.dayOfBirth ?? "",
      address: details.address ?? "",
      city_id: details.city ? details.city.id ?? details.city.value : "",
      district_id: details.district ? details.district.id ?? details.district.value : "",
      ward_id: details.ward ? details.ward.id ?? details.ward.value : "",
      department_id: details.department ? details.department.id ?? details.department.value : "",
      position_id: details.position ? details.position.id ?? details.position.value : "",
      level_id: details.level ? details.level.id ?? details.level.value : "",
      title_id: details.title ? details.title.id ?? details.title.value : "",
      technique_id: Array.isArray(details.technique) ? details.technique.map((technique) => technique.id ?? technique.value) : [],
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const StaffDetailPage = () => {
  const [loading, setLoading] = useState(true);
  const [details, setDetails] = useState({
    fullName: "",
    nickName: "",
    mobilePhone: "",
    email: "",
    maritalStatus: "",
    nid: "",
    dateOfNid: "",
    placeOfNidObj: "",
    national: "",
    religion: "",
    ethnic: "",
    certification: "",
    staffType: "",
    status: "",
    image3x4: "",
    gender: "",
    dayOfBirth: "",
    address: "",
    city: "",
    district: "",
    ward: "",
    department: "",
    position: "",
    level: "",
    title: "",
    technique: "",
  });

  const [cityList, setCityList] = useState([]);
  const [districtList, setDistrictList] = useState([]);
  const [wardList, setWardList] = useState([]);
  const [nationList, setNationList] = useState([]);
  const [religionList, setReligionList] = useState([]);
  const [ethnicList, setEthnicList] = useState([]);
  const [departmentList, setDepartmentList] = useState([]);
  const [positionList, setPositionList] = useState([]);
  const [levelList, setLevelList] = useState([]);
  const [certifitcationList, setCertifitcationList] = useState([]);
  const [staffTypeList, setStaffTypeList] = useState([]);
  const [titleList, setTitleList] = useState([]);
  const [techniqueList, setTechniqueList] = useState([]);

  const [errorFullName, setErrorFullName] = useState("");
  const [errorNickName, setErrorNickName] = useState("");
  const [errorMobilePhone, setErrorMobilePhone] = useState("");
  const [errorEmail, setErrorEmail] = useState("");
  const [errorMaritalStatus, setErrorMaritalStatus] = useState("");
  const [errorNid, setErrorNid] = useState("");
  const [errorDateOfNid, setErrorDateOfNid] = useState("");
  const [errorPlaceOfNid, setErrorPlaceOfNid] = useState("");
  const [errorNationalId, setErrorNationalId] = useState("");
  const [errorReligionId, setErrorReligionId] = useState("");
  const [errorEthnicId, setErrorEthnicId] = useState("");
  const [errorStaffTypeId, setErrorStaffTypeId] = useState("");
  const [errorCertificationId, setErrorCertificationId] = useState("");
  const [errorStatus, setErrorStatus] = useState("");
  const [errorImage3x4, setErrorImage3x4] = useState("");
  const [errorGender, setErrorGender] = useState("");
  const [errorDayOfBirth, setErrorDayOfBirth] = useState("");
  const [errorAddress, setErrorAddress] = useState("");
  const [errorCityId, setErrorCityId] = useState("");
  const [errorDistrictId, setErrorDistrictId] = useState("");
  const [errorWardId, setErrorWardId] = useState("");
  const [errorDepartmentId, setErrorDepartmentId] = useState("");
  const [errorPositionId, setErrorPositionId] = useState("");
  const [errorLevelId, setErrorLevelId] = useState("");
  const [errorTitleId, setErrorTitleId] = useState("");
  const [errorTechniqueId, setErrorTechniqueId] = useState("");

  async function getStaffDetail(staffId) {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "staff/detail?staff_id=" + staffId,
      headers: AppEnvironment.headers,
    };

    return axios(config)
      .then((res) => res.data.data)
      .catch((err) => console.log(err));
  }

  useEffect(() => {
    async function fetchData() {
      const staffDetail = await getStaffDetail(staffId);
      const techniqueStaffList = staffDetail.staff_technique.map((technique) => ({ id: technique.technique.id, technique_name: technique.technique.technique_name }));

      setDetails({
        staffCode: staffDetail.staff_code,
        fullName: staffDetail.full_name,
        nickName: staffDetail.nick_name,
        mobilePhone: staffDetail.mobile_phone,
        email: staffDetail.email,
        maritalStatus: staffDetail.marital_status,
        nid: staffDetail.nid,
        dateOfNid: staffDetail.date_of_nid,
        placeOfNidObj: staffDetail.place_of_nid_obj,
        national: staffDetail.national,
        religion: staffDetail.religion,
        ethnic: staffDetail.ethnic,
        certification: staffDetail.certification,
        staffType: staffDetail.staff_type,
        status: staffDetail.status,
        image3x4: staffDetail.image3x4,
        gender: staffDetail.gender,
        dayOfBirth: staffDetail.day_of_birth,
        address: staffDetail.address,
        city: staffDetail.city,
        district: staffDetail.district,
        ward: staffDetail.ward,
        department: staffDetail.department,
        position: staffDetail.position,
        level: staffDetail.level,
        title: staffDetail.title,
        technique: techniqueStaffList,
        updated_at: staffDetail.updated_at,
        updated_user: staffDetail.updated_user,
      });

      const cityList = await GeneralApi.cityList();
      setCityList(cityList);

      if (staffDetail.city && staffDetail.district) {
        const districtList = await GeneralApi.districtList(staffDetail.city.id);
        setDistrictList(districtList);

        const wardList = await GeneralApi.wardList(staffDetail.district.id);
        setWardList(wardList);
      }

      const nationList = await GeneralApi.nationList();
      setNationList(nationList);

      const religionList = await GeneralApi.religionList();
      setReligionList(religionList);

      const ethnicList = await GeneralApi.ethnicList();
      setEthnicList(ethnicList);

      const staffTypeList = await GeneralApi.staffTypeList();
      setStaffTypeList(staffTypeList);

      const departmentList = await GeneralApi.departmentList();
      setDepartmentList(departmentList);

      const positionList = await GeneralApi.positionList();
      setPositionList(positionList);

      const levelList = await GeneralApi.levelList();
      setLevelList(levelList);

      const titleList = await GeneralApi.titleList();
      setTitleList(titleList);

      const techniqueList = await GeneralApi.techniqueList();
      setTechniqueList(techniqueList);

      const certificationList = await GeneralApi.certificationList();
      setCertifitcationList(certificationList);

      setLoading(false);
    }

    fetchData();
  }, []);

  const loadDistrict = async (e) => {
    setDetails({ ...details, city: e ?? "" });
    setDetails({ ...details, district: "" });
    setDetails({ ...details, ward: "" });

    const districtList = await GeneralApi.districtList(e.value);
    setDistrictList(districtList);
  };

  const loadWard = async (e) => {
    setDetails({ ...details, district: e ?? "" });
    setDetails({ ...details, ward: "" });

    const wardList = await GeneralApi.wardList(e.value);
    setWardList(wardList);
  };

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await updateStaff(details);
    let code = response.code;
    if (code === 0) {
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.full_name) {
        setErrorFullName(response.messageObject.full_name[0]);
      } else {
        setErrorFullName("");
      }

      if (response.messageObject.nick_name) {
        setErrorNickName(response.messageObject.nick_name[0]);
      } else {
        setErrorNickName("");
      }

      if (response.messageObject.mobile_phone) {
        setErrorMobilePhone(response.messageObject.mobile_phone[0]);
      } else {
        setErrorMobilePhone("");
      }

      if (response.messageObject.email) {
        setErrorEmail(response.messageObject.email[0]);
      } else {
        setErrorEmail("");
      }

      if (response.messageObject.marital_status) {
        setErrorMaritalStatus(response.messageObject.marital_status[0]);
      } else {
        setErrorMaritalStatus("");
      }

      if (response.messageObject.nid) {
        setErrorNid(response.messageObject.nid[0]);
      } else {
        setErrorNid("");
      }

      if (response.messageObject.date_of_nid) {
        setErrorDateOfNid(response.messageObject.date_of_nid[0]);
      } else {
        setErrorDateOfNid("");
      }

      if (response.messageObject.place_of_nid) {
        setErrorPlaceOfNid(response.messageObject.place_of_nid[0]);
      } else {
        setErrorPlaceOfNid("");
      }

      if (response.messageObject.national_id) {
        setErrorNationalId(response.messageObject.national_id[0]);
      } else {
        setErrorNationalId("");
      }

      if (response.messageObject.religion_id) {
        setErrorReligionId(response.messageObject.religion_id[0]);
      } else {
        setErrorReligionId("");
      }

      if (response.messageObject.ethnic_id) {
        setErrorEthnicId(response.messageObject.ethnic_id[0]);
      } else {
        setErrorEthnicId("");
      }

      if (response.messageObject.staff_type_id) {
        setErrorStaffTypeId(response.messageObject.staff_type_id[0]);
      } else {
        setErrorStaffTypeId("");
      }

      if (response.messageObject.certification_id) {
        setErrorCertificationId(response.messageObject.certification_id[0]);
      } else {
        setErrorCertificationId("");
      }

      if (response.messageObject.status) {
        setErrorStatus(response.messageObject.status[0]);
      } else {
        setErrorStatus("");
      }

      if (response.messageObject.image3x4) {
        setErrorImage3x4(response.messageObject.image3x4[0]);
      } else {
        setErrorImage3x4("");
      }

      if (response.messageObject.gender) {
        setErrorGender(response.messageObject.gender[0]);
      } else {
        setErrorGender("");
      }

      if (response.messageObject.day_of_birth) {
        setErrorDayOfBirth(response.messageObject.day_of_birth[0]);
      } else {
        setErrorDayOfBirth("");
      }

      if (response.messageObject.address) {
        setErrorAddress(response.messageObject.address[0]);
      } else {
        setErrorAddress("");
      }

      if (response.messageObject.city_id) {
        setErrorCityId(response.messageObject.city_id[0]);
      } else {
        setErrorCityId("");
      }

      if (response.messageObject.district_id) {
        setErrorDistrictId(response.messageObject.district_id[0]);
      } else {
        setErrorDistrictId("");
      }

      if (response.messageObject.ward_id) {
        setErrorWardId(response.messageObject.ward_id[0]);
      } else {
        setErrorWardId("");
      }

      if (response.messageObject.department_id) {
        setErrorDepartmentId(response.messageObject.department_id[0]);
      } else {
        setErrorDepartmentId("");
      }

      if (response.messageObject.position_id) {
        setErrorPositionId(response.messageObject.position_id[0]);
      } else {
        setErrorPositionId("");
      }

      if (response.messageObject.level_id) {
        setErrorLevelId(response.messageObject.level_id[0]);
      } else {
        setErrorLevelId("");
      }

      if (response.messageObject.title_id) {
        setErrorTitleId(response.messageObject.title_id[0]);
      } else {
        setErrorTitleId("");
      }

      if (response.messageObject.technique_id) {
        setErrorTechniqueId(response.messageObject.technique_id[0]);
      } else {
        setErrorTechniqueId("");
      }
    }
  };

  const status = [
    { value: 1, label: "Đang làm việc" },
    { value: 2, label: "Đã nghỉ việc" },
  ];

  return loading ? (
    <div className="App">
      <div className="content-wrapper">
        <Loading></Loading>
      </div>
    </div>
  ) : (
    <div className="App">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Chi tiết nhân viên</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <i className="nav-icon fas fa-house-user" /> Tổng quan
                  </li>
                  <li className="breadcrumb-item active">
                    <a href="/staff" className="text-secondary">
                      Danh sách nhân viên
                    </a>
                  </li>
                  <li className="breadcrumb-item active">Chi tiết nhân viên</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Thông tin nhân viên</h3>
            </div>
            <form onSubmit={submitHanble}>
              <div className="card-body">
                <div className="form-group">
                  <label>Mã nhân viên</label>
                  <input type="text" className="form-control" defaultValue={details.staffCode} disabled />
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Họ và tên <span className="require-field">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nhập họ và tên nhân viên"
                        defaultValue={details.fullName}
                        onChange={(e) => setDetails({ ...details, fullName: e.target.value })}
                      />
                      {errorFullName !== "" ? <div className="error">{errorFullName}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Ảnh 3x4</label>
                      <input type="file" className="custom-file-input" onChange={(e) => setDetails({ ...details, image3x4: e.target.value })} />
                      <label className="custom-file-label custom-add-file">Tìm kiếm</label>
                      {errorImage3x4 !== "" ? <div className="error">{errorImage3x4}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Biệt danh</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nhập biệt danh nhân viên"
                        defaultValue={details.nickName ?? ""}
                        onChange={(e) => setDetails({ ...details, nickName: e.target.value })}
                      />
                      {errorNickName !== "" ? <div className="error">{errorNickName}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Giới tính <span className="require-field">*</span>
                      </label>
                      <div className="form-group">
                        <div className="form-check mt-2">
                          <input
                            className="form-check-input mr-4"
                            type="radio"
                            name="gender"
                            checked={details.gender === 1 ? true : false}
                            onChange={(e) => setDetails({ ...details, gender: 1 })}
                          />
                          <label className="form-check-label mr-4">Nam</label>
                          <input
                            className="form-check-input ml-4"
                            type="radio"
                            name="gender"
                            checked={details.gender === 2 ? true : false}
                            onChange={(e) => setDetails({ ...details, gender: 2 })}
                          />
                          <label className="form-check-label ml-5">Nữ</label>
                          {errorGender !== "" ? <div className="error">{errorGender}</div> : ""}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Số điện thoại <span className="require-field">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nhập số điện thoại"
                        defaultValue={details.mobilePhone}
                        onChange={(e) => setDetails({ ...details, mobilePhone: e.target.value })}
                      />
                      {errorMobilePhone !== "" ? <div className="error">{errorMobilePhone}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Email <span className="require-field">*</span>
                      </label>
                      <input type="text" className="form-control" placeholder="Nhập email" defaultValue={details.email} onChange={(e) => setDetails({ ...details, email: e.target.value })} />
                      {errorEmail !== "" ? <div className="error">{errorEmail}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Trình trạng hôn nhân <span className="require-field">*</span>
                      </label>
                      <div className="form-check mt-2">
                        <input
                          className="form-check-input mr-4"
                          type="radio"
                          name="marital"
                          checked={details.maritalStatus === 1 ? true : false}
                          onChange={(e) => setDetails({ ...details, maritalStatus: 1 })}
                        />
                        <label className="form-check-label mr-4">Độc thân</label>
                        <input
                          className="form-check-input ml-4"
                          type="radio"
                          name="marital"
                          checked={details.maritalStatus === 2 ? true : false}
                          onChange={(e) => setDetails({ ...details, maritalStatus: 2 })}
                        />
                        <label className="form-check-label ml-5">Đã kết hôn</label>
                        {errorMaritalStatus !== "" ? <div className="error">{errorMaritalStatus}</div> : ""}
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Ngày sinh <span className="require-field">*</span>
                      </label>
                      <input type="date" className="form-control" max="2006-12-31" defaultValue={details.dayOfBirth} onChange={(e) => setDetails({ ...details, dayOfBirth: e.target.value })} />
                      {errorDayOfBirth !== "" ? <div className="error">{errorDayOfBirth}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Số CMND/CCCD <span className="require-field">*</span>
                      </label>
                      <input type="text" className="form-control" placeholder="Nhập số CMND/CCCD" defaultValue={details.nid} onChange={(e) => setDetails({ ...details, nid: e.target.value })} />
                      {errorNid !== "" ? <div className="error">{errorNid}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Nơi ở</label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nhập số nhà, tên đường"
                        defaultValue={details.address ?? ""}
                        onChange={(e) => setDetails({ ...details, address: e.target.value })}
                      />
                      {errorAddress !== "" ? <div className="error">{errorAddress}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-3">
                    <div className="form-group">
                      <label>
                        Ngày cấp <span className="require-field">*</span>
                      </label>
                      <input type="date" className="form-control" defaultValue={details.dateOfNid} onChange={(e) => setDetails({ ...details, dateOfNid: e.target.value })} />
                      {errorDateOfNid !== "" ? <div className="error">{errorDateOfNid}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-3">
                    <div className="form-group">
                      <label>
                        Nơi cấp <span className="require-field">*</span>
                      </label>
                      <Select
                        options={cityList}
                        isSearchable={true}
                        placeholder="-- Nơi cấp --"
                        defaultValue={{ value: details.placeOfNidObj.id, label: details.placeOfNidObj.name }}
                        onChange={(e) => setDetails({ ...details, placeOfNidObj: e ?? "" })}
                      />
                      {errorPlaceOfNid !== "" ? <div className="error">{errorPlaceOfNid}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-2 mt-2">
                    <div className="form-group">
                      <label></label>
                      <Select
                        options={cityList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn tỉnh/thành phố --"
                        defaultValue={details.city ? { value: details.city.id, label: details.city.name } : ""}
                        onChange={loadDistrict}
                      />
                      {errorCityId !== "" ? <div className="error">{errorCityId}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-2 mt-2">
                    <div className="form-group">
                      <label></label>
                      <Select
                        options={districtList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn quận/huyện --"
                        defaultValue={details.district ? { value: details.district.id, label: details.district.name } : ""}
                        onChange={loadWard}
                      />
                      {errorDistrictId !== "" ? <div className="error">{errorDistrictId}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-2 mt-2">
                    <div className="form-group">
                      <label></label>
                      <Select
                        options={wardList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn phường/xã --"
                        defaultValue={details.ward ? { value: details.ward.id, label: details.ward.name } : ""}
                        onChange={(e) => setDetails({ ...details, ward: e ?? "" })}
                      />
                      {errorWardId !== "" ? <div className="error">{errorWardId}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Quốc tịch <span className="require-field">*</span>
                      </label>
                      <Select
                        options={nationList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn quốc tịch --"
                        defaultValue={details.national ? { value: details.national.id, label: details.national.name } : ""}
                        onChange={(e) => setDetails({ ...details, national: e ?? "" })}
                      />
                      {errorNationalId !== "" ? <div className="error">{errorNationalId}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Phòng ban <span className="require-field">*</span>
                      </label>
                      <Select
                        options={departmentList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn phòng ban --"
                        defaultValue={{ value: details.department.id, label: details.department.department_name }}
                        onChange={(e) => setDetails({ ...details, department: e ?? "" })}
                      />
                      {errorDepartmentId !== "" ? <div className="error">{errorDepartmentId}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Tôn giáo</label>
                      <Select
                        options={religionList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn tôn giáo --"
                        defaultValue={details.religion ? { value: details.religion.id, label: details.religion.name } : ""}
                        onChange={(e) => setDetails({ ...details, religion: e ?? "" })}
                      />
                      {errorReligionId !== "" ? <div className="error">{errorReligionId}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Chức vụ <span className="require-field">*</span>
                      </label>
                      <Select
                        options={positionList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn chức vụ --"
                        defaultValue={{ value: details.position.id, label: details.position.position_name }}
                        onChange={(e) => setDetails({ ...details, position: e ?? "" })}
                      />
                      {errorPositionId !== "" ? <div className="error">{errorPositionId}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Dân tộc</label>
                      <Select
                        options={ethnicList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn dân tộc --"
                        defaultValue={details.ethnic ? { value: details.ethnic.id, label: details.ethnic.name } : ""}
                        onChange={(e) => setDetails({ ...details, ethnic: e ?? "" })}
                      />
                      {errorEthnicId !== "" ? <div className="error">{errorEthnicId}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Trình độ <span className="require-field">*</span>
                      </label>
                      <Select
                        options={levelList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn trình độ --"
                        defaultValue={{ value: details.level.id, label: details.level.level_name }}
                        onChange={(e) => setDetails({ ...details, level: e ?? "" })}
                      />
                      {errorLevelId !== "" ? <div className="error">{errorLevelId}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Loại nhân viên <span className="require-field">*</span>
                      </label>
                      <Select
                        options={staffTypeList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn loại nhân viên --"
                        defaultValue={{ value: details.staffType.id, label: details.staffType.staff_type_name }}
                        onChange={(e) => setDetails({ ...details, staffType: e ?? "" })}
                      />
                      {errorStaffTypeId !== "" ? <div className="error">{errorStaffTypeId}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Chức danh <span className="require-field">*</span>
                      </label>
                      <Select
                        options={titleList}
                        isSearchable={true}
                        placeholder="-- Chọn chức danh --"
                        defaultValue={{ value: details.title.id, label: details.title.title_name }}
                        onChange={(e) => setDetails({ ...details, title: e ?? "" })}
                      />
                      {errorTitleId !== "" ? <div className="error">{errorTitleId}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Bằng cấp</label>
                      <Select
                        options={certifitcationList}
                        isClearable={true}
                        isSearchable={true}
                        placeholder="-- Chọn bằng cấp --"
                        defaultValue={details.certification ? { value: details.certification.id, label: details.certification.certification_name } : ""}
                        onChange={(e) => setDetails({ ...details, certification: e ?? "" })}
                      />
                      {errorCertificationId !== "" ? <div className="error">{errorCertificationId}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Chuyên môn <span className="require-field">*</span>
                      </label>
                      <Select
                        isMulti
                        options={techniqueList}
                        isClearable={true}
                        isSearchable={true}
                        placeholder="-- Chọn chuyên môn --"
                        defaultValue={details.technique && Array.isArray(details.technique) ? details.technique.map((technique) => ({ value: technique.id, label: technique.technique_name })) : []}
                        onChange={(e) => setDetails({ ...details, technique: Array.isArray(e) ? e.map((technique) => technique) : [] })}
                      />
                      {errorTechniqueId !== "" ? <div className="error">{errorTechniqueId}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Trạng thái <span className="require-field">*</span>
                      </label>
                      <Select
                        options={status}
                        placeholder="-- Chọn trạng thái --"
                        defaultValue={details.status === 1 ? { value: 1, label: "Đang làm việc" } : { value: details.certification.id, label: "Đã nghỉ việc" }}
                        onChange={(e) => setDetails({ ...details, status: e.value })}
                      />
                      {errorStatus !== "" ? <div className="error">{errorStatus}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Người sửa gần nhất</label>
                      <input type="text" className="form-control" value={details.updated_user.first_name + " " + details.updated_user.last_name} disabled />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Ngày sửa gần nhất</label>
                      <input type="text" className="form-control" value={helpers.convertDateTimeForApp(details.updated_at)} disabled />
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
                <button type="submit" className="btn btn-primary">
                  <i className="nav-icon fas fa-plus" /> Cập nhật nhân viên
                </button>
              </div>
            </form>
          </div>
        </section>
      </div>
    </div>
  );
};

export default StaffDetailPage;
