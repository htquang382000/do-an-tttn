import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
import Select from "react-select";
import Loading from "../../components/Loading";
import GeneralApi from "../../api/general/GeneralApi";
import UploadImage from "../../api/upload-image/UploadImage";

const business = helpers.getBusiness();
const user = helpers.getUser();
const businessLocationId = helpers.getBusinessLocationId();

async function createStaff(details) {
  var config = {
    method: "post",
    url: AppEnvironment.urlBaseApi + "staff/store",
    headers: AppEnvironment.headers,
    data: {
      business_id: business.id,
      business_location_id: businessLocationId,
      full_name: details.fullName,
      nick_name: details.nickName,
      mobile_phone: details.mobilePhone,
      email: details.email,
      marital_status: details.maritalStatus,
      nid: details.nid,
      date_of_nid: details.dateOfNid,
      place_of_nid: details.placeOfNid,
      national_id: details.nationalId,
      religion_id: details.religionId,
      ethnic_id: details.ethnicId,
      staff_type_id: details.certificationId,
      certification_id: details.staffTypeId,
      status: details.status,
      image3x4: details.image3x4,
      gender: details.gender,
      day_of_birth: details.dayOfBirth,
      address: details.address,
      city_id: details.cityId,
      district_id: details.districtId,
      ward_id: details.wardId,
      department_id: details.departmentId,
      position_id: details.positionId,
      level_id: details.levelId,
      title_id: details.titleId,
      technique_id: details.techniqueId,
    },
  };

  return axios(config)
    .then((response) => response.data)
    .catch(function (error) {
      console.log(error);
    });
}

const LevelPage = () => {
  const [loading, setLoading] = useState(true);
  const [details, setDetails] = useState({
    fullName: "",
    nickName: "",
    mobilePhone: "",
    email: "",
    maritalStatus: 1,
    nid: "",
    dateOfNid: "",
    placeOfNid: "",
    nationalId: "",
    religionId: "",
    ethnicId: "",
    certificationId: "",
    staffTypeId: "",
    status: "",
    image3x4: "",
    gender: 1,
    dayOfBirth: "",
    address: "",
    cityId: "",
    districtId: "",
    wardId: "",
    departmentId: "",
    positionId: "",
    levelId: "",
    titleId: "",
    techniqueId: [],
  });

  const [cityList, setCityList] = useState([]);
  const [districtList, setDistrictList] = useState([]);
  const [wardList, setWardList] = useState([]);
  const [nationList, setNationList] = useState([]);
  const [religionList, setReligionList] = useState([]);
  const [ethnicList, setEthnicList] = useState([]);
  const [departmentList, setDepartmentList] = useState([]);
  const [positionList, setPositionList] = useState([]);
  const [levelList, setLevelList] = useState([]);
  const [certifitcationList, setCertifitcationList] = useState([]);
  const [staffTypeList, setStaffTypeList] = useState([]);
  const [titleList, setTitleList] = useState([]);
  const [techniqueList, setTechniqueList] = useState([]);

  const [errorFullName, setErrorFullName] = useState("");
  const [errorNickName, setErrorNickName] = useState("");
  const [errorMobilePhone, setErrorMobilePhone] = useState("");
  const [errorEmail, setErrorEmail] = useState("");
  const [errorMaritalStatus, setErrorMaritalStatus] = useState("");
  const [errorNid, setErrorNid] = useState("");
  const [errorDateOfNid, setErrorDateOfNid] = useState("");
  const [errorPlaceOfNid, setErrorPlaceOfNid] = useState("");
  const [errorNationalId, setErrorNationalId] = useState("");
  const [errorReligionId, setErrorReligionId] = useState("");
  const [errorEthnicId, setErrorEthnicId] = useState("");
  const [errorStaffTypeId, setErrorStaffTypeId] = useState("");
  const [errorCertificationId, setErrorCertificationId] = useState("");
  const [errorStatus, setErrorStatus] = useState("");
  const [errorImage3x4, setErrorImage3x4] = useState("");
  const [errorGender, setErrorGender] = useState("");
  const [errorDayOfBirth, setErrorDayOfBirth] = useState("");
  const [errorAddress, setErrorAddress] = useState("");
  const [errorCityId, setErrorCityId] = useState("");
  const [errorDistrictId, setErrorDistrictId] = useState("");
  const [errorWardId, setErrorWardId] = useState("");
  const [errorDepartmentId, setErrorDepartmentId] = useState("");
  const [errorPositionId, setErrorPositionId] = useState("");
  const [errorLevelId, setErrorLevelId] = useState("");
  const [errorTitleId, setErrorTitleId] = useState("");
  const [errorTechniqueId, setErrorTechniqueId] = useState("");

  useEffect(() => {
    async function fetchData() {
      const cityList = await GeneralApi.cityList();
      setCityList(cityList);

      const nationList = await GeneralApi.nationList();
      setNationList(nationList);

      const religionList = await GeneralApi.religionList();
      setReligionList(religionList);

      const ethnicList = await GeneralApi.ethnicList();
      setEthnicList(ethnicList);

      const staffTypeList = await GeneralApi.staffTypeList();
      setStaffTypeList(staffTypeList);

      const departmentList = await GeneralApi.departmentList();
      setDepartmentList(departmentList);

      const positionList = await GeneralApi.positionList();
      setPositionList(positionList);

      const levelList = await GeneralApi.levelList();
      setLevelList(levelList);

      const titleList = await GeneralApi.titleList();
      setTitleList(titleList);

      const techniqueList = await GeneralApi.techniqueList();
      setTechniqueList(techniqueList);

      const certificationList = await GeneralApi.certificationList();
      setCertifitcationList(certificationList);

      setLoading(false);
    }

    fetchData();
  }, []);

  const loadDistrict = async (e) => {
    setDetails({ ...details, cityId: e ? e.value : "" });
    setDetails({ ...details, districtId: "" });
    setDetails({ ...details, wardId: "" });

    const districtList = await GeneralApi.districtList(e.value);
    setDistrictList(districtList);
  };

  const loadWard = async (e) => {
    setDetails({ ...details, districtId: e ? e.value : "" });
    setDetails({ ...details, wardId: "" });

    const wardList = await GeneralApi.wardList(e.value);
    setWardList(wardList);
  };

  const submitHanble = async (e) => {
    e.preventDefault();

    // console.log(details.image3x4);

    // const formData = new FormData();
    // formData.append("image", details.image3x4, details.image3x4.name);
    // console.log(formData);

    // let responseImage = await UploadImage(formData);
    // console.log(responseImage);

    let response = await createStaff(details);
    let code = response.code;
    if (code === 0) {
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.full_name) {
        setErrorFullName(response.messageObject.full_name[0]);
      } else {
        setErrorFullName("");
      }

      if (response.messageObject.nick_name) {
        setErrorNickName(response.messageObject.nick_name[0]);
      } else {
        setErrorNickName("");
      }

      if (response.messageObject.mobile_phone) {
        setErrorMobilePhone(response.messageObject.mobile_phone[0]);
      } else {
        setErrorMobilePhone("");
      }

      if (response.messageObject.email) {
        setErrorEmail(response.messageObject.email[0]);
      } else {
        setErrorEmail("");
      }

      if (response.messageObject.marital_status) {
        setErrorMaritalStatus(response.messageObject.marital_status[0]);
      } else {
        setErrorMaritalStatus("");
      }

      if (response.messageObject.nid) {
        setErrorNid(response.messageObject.nid[0]);
      } else {
        setErrorNid("");
      }

      if (response.messageObject.date_of_nid) {
        setErrorDateOfNid(response.messageObject.date_of_nid[0]);
      } else {
        setErrorDateOfNid("");
      }

      if (response.messageObject.place_of_nid) {
        setErrorPlaceOfNid(response.messageObject.place_of_nid[0]);
      } else {
        setErrorPlaceOfNid("");
      }

      if (response.messageObject.national_id) {
        setErrorNationalId(response.messageObject.national_id[0]);
      } else {
        setErrorNationalId("");
      }

      if (response.messageObject.religion_id) {
        setErrorReligionId(response.messageObject.religion_id[0]);
      } else {
        setErrorReligionId("");
      }

      if (response.messageObject.ethnic_id) {
        setErrorEthnicId(response.messageObject.ethnic_id[0]);
      } else {
        setErrorEthnicId("");
      }

      if (response.messageObject.staff_type_id) {
        setErrorStaffTypeId(response.messageObject.staff_type_id[0]);
      } else {
        setErrorStaffTypeId("");
      }

      if (response.messageObject.certification_id) {
        setErrorCertificationId(response.messageObject.certification_id[0]);
      } else {
        setErrorCertificationId("");
      }

      if (response.messageObject.status) {
        setErrorStatus(response.messageObject.status[0]);
      } else {
        setErrorStatus("");
      }

      if (response.messageObject.image3x4) {
        setErrorImage3x4(response.messageObject.image3x4[0]);
      } else {
        setErrorImage3x4("");
      }

      if (response.messageObject.gender) {
        setErrorGender(response.messageObject.gender[0]);
      } else {
        setErrorGender("");
      }

      if (response.messageObject.day_of_birth) {
        setErrorDayOfBirth(response.messageObject.day_of_birth[0]);
      } else {
        setErrorDayOfBirth("");
      }

      if (response.messageObject.address) {
        setErrorAddress(response.messageObject.address[0]);
      } else {
        setErrorAddress("");
      }

      if (response.messageObject.city_id) {
        setErrorCityId(response.messageObject.city_id[0]);
      } else {
        setErrorCityId("");
      }

      if (response.messageObject.district_id) {
        setErrorDistrictId(response.messageObject.district_id[0]);
      } else {
        setErrorDistrictId("");
      }

      if (response.messageObject.ward_id) {
        setErrorWardId(response.messageObject.ward_id[0]);
      } else {
        setErrorWardId("");
      }

      if (response.messageObject.department_id) {
        setErrorDepartmentId(response.messageObject.department_id[0]);
      } else {
        setErrorDepartmentId("");
      }

      if (response.messageObject.position_id) {
        setErrorPositionId(response.messageObject.position_id[0]);
      } else {
        setErrorPositionId("");
      }

      if (response.messageObject.level_id) {
        setErrorLevelId(response.messageObject.level_id[0]);
      } else {
        setErrorLevelId("");
      }

      if (response.messageObject.title_id) {
        setErrorTitleId(response.messageObject.title_id[0]);
      } else {
        setErrorTitleId("");
      }

      if (response.messageObject.technique_id) {
        setErrorTechniqueId(response.messageObject.technique_id[0]);
      } else {
        setErrorTechniqueId("");
      }
    }
  };

  const status = [
    { value: 1, label: "Đang làm việc" },
    { value: 2, label: "Đã nghỉ việc" },
  ];

  return loading ? (
    <div className="App">
      <div className="content-wrapper">
        <Loading></Loading>
      </div>
    </div>
  ) : (
    <div className="App">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Thêm nhân viên mới</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <i className="nav-icon fas fa-users" /> Nhân viên
                  </li>
                  <li className="breadcrumb-item active">Thêm nhân viên mới</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Thông tin nhân viên</h3>
            </div>
            <form onSubmit={submitHanble}>
              <div className="card-body">
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Họ và tên <span className="require-field">*</span>
                      </label>
                      <input type="text" className="form-control" placeholder="Nhập họ và tên nhân viên" onChange={(e) => setDetails({ ...details, fullName: e.target.value })} />
                      {errorFullName !== "" ? <div className="error">{errorFullName}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Ảnh 3x4</label>
                      <input type="file" className="custom-file-input" accept=".jpg,.png" onChange={(e) => setDetails({ ...details, image3x4: e.target.files[0] })} />
                      <label className="custom-file-label custom-add-file">Tìm kiếm</label>
                      {errorImage3x4 !== "" ? <div className="error">{errorImage3x4}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Biệt danh</label>
                      <input type="text" className="form-control" placeholder="Nhập biệt danh nhân viên" onChange={(e) => setDetails({ ...details, nickName: e.target.value })} />
                      {errorNickName !== "" ? <div className="error">{errorNickName}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Giới tính <span className="require-field">*</span>
                      </label>
                      <div className="form-group">
                        <div className="form-check mt-2">
                          <input className="form-check-input mr-4" type="radio" name="gender" checked onChange={(e) => setDetails({ ...details, priority: 1 })} />
                          <label className="form-check-label mr-4">Nam</label>
                          <input className="form-check-input ml-4" type="radio" name="gender" onChange={(e) => setDetails({ ...details, gender: 2 })} />
                          <label className="form-check-label ml-5">Nữ</label>
                          {errorGender !== "" ? <div className="error">{errorGender}</div> : ""}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Số điện thoại <span className="require-field">*</span>
                      </label>
                      <input type="text" className="form-control" placeholder="Nhập số điện thoại" onChange={(e) => setDetails({ ...details, mobilePhone: e.target.value })} />
                      {errorMobilePhone !== "" ? <div className="error">{errorMobilePhone}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Email <span className="require-field">*</span>
                      </label>
                      <input type="text" className="form-control" placeholder="Nhập email" onChange={(e) => setDetails({ ...details, email: e.target.value })} />
                      {errorEmail !== "" ? <div className="error">{errorEmail}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Trình trạng hôn nhân <span className="require-field">*</span>
                      </label>
                      <div className="form-check mt-2">
                        <input className="form-check-input mr-4" type="radio" name="marital" checked onChange={(e) => setDetails({ ...details, maritalStatus: 1 })} />
                        <label className="form-check-label mr-4">Độc thân</label>
                        <input className="form-check-input ml-4" type="radio" name="marital" onChange={(e) => setDetails({ ...details, maritalStatus: 2 })} />
                        <label className="form-check-label ml-5">Đã kết hôn</label>
                        {errorMaritalStatus !== "" ? <div className="error">{errorMaritalStatus}</div> : ""}
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Ngày sinh <span className="require-field">*</span>
                      </label>
                      <input type="date" className="form-control" max="2006-12-31" onChange={(e) => setDetails({ ...details, dayOfBirth: e.target.value })} />
                      {errorDayOfBirth !== "" ? <div className="error">{errorDayOfBirth}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Số CMND/CCCD <span className="require-field">*</span>
                      </label>
                      <input type="text" className="form-control" placeholder="Nhập số CMND/CCCD" onChange={(e) => setDetails({ ...details, nid: e.target.value })} />
                      {errorNid !== "" ? <div className="error">{errorNid}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Nơi ở</label>
                      <input type="text" className="form-control" placeholder="Nhập số nhà, tên đường" onChange={(e) => setDetails({ ...details, address: e.target.value })} />
                      {errorAddress !== "" ? <div className="error">{errorAddress}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-3">
                    <div className="form-group">
                      <label>
                        Ngày cấp <span className="require-field">*</span>
                      </label>
                      <input type="date" className="form-control" onChange={(e) => setDetails({ ...details, dateOfNid: e.target.value })} />
                      {errorDateOfNid !== "" ? <div className="error">{errorDateOfNid}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-3">
                    <div className="form-group">
                      <label>
                        Nơi cấp <span className="require-field">*</span>
                      </label>
                      <Select
                        options={cityList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Nơi cấp --"
                        onChange={(e) => setDetails({ ...details, placeOfNid: e ? e.value : "" })}
                      />
                      {errorPlaceOfNid !== "" ? <div className="error">{errorPlaceOfNid}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-2 mt-2">
                    <div className="form-group">
                      <label></label>
                      <Select options={cityList} isSearchable={true} isClearable={true} placeholder="-- Chọn tỉnh/thành phố --" onChange={loadDistrict} />
                      {errorCityId !== "" ? <div className="error">{errorCityId}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-2 mt-2">
                    <div className="form-group">
                      <label></label>
                      <Select options={districtList} isSearchable={true} isClearable={true} placeholder="-- Chọn quận/huyện --" onChange={loadWard} />
                      {errorDistrictId !== "" ? <div className="error">{errorDistrictId}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-2 mt-2">
                    <div className="form-group">
                      <label></label>
                      <Select
                        options={wardList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn phường/xã --"
                        onChange={(e) => setDetails({ ...details, wardId: e ? e.value : "" })}
                      />
                      {errorWardId !== "" ? <div className="error">{errorWardId}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Quốc tịch <span className="require-field">*</span>
                      </label>
                      <Select
                        options={nationList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn quốc tịch --"
                        onChange={(e) => setDetails({ ...details, nationalId: e ? e.value : "" })}
                      />
                      {errorNationalId !== "" ? <div className="error">{errorNationalId}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Phòng ban <span className="require-field">*</span>
                      </label>
                      <Select
                        options={departmentList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn phòng ban --"
                        onChange={(e) => setDetails({ ...details, departmentId: e ? e.value : "" })}
                      />
                      {errorDepartmentId !== "" ? <div className="error">{errorDepartmentId}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Tôn giáo</label>
                      <Select
                        options={religionList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn tôn giáo --"
                        onChange={(e) => setDetails({ ...details, religionId: e ? e.value : "" })}
                      />
                      {errorReligionId !== "" ? <div className="error">{errorReligionId}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Chức vụ <span className="require-field">*</span>
                      </label>
                      <Select
                        options={positionList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn chức vụ --"
                        onChange={(e) => setDetails({ ...details, positionId: e ? e.value : "" })}
                      />
                      {errorPositionId !== "" ? <div className="error">{errorPositionId}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Dân tộc</label>
                      <Select
                        options={ethnicList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn dân tộc --"
                        onChange={(e) => setDetails({ ...details, ethnicId: e ? e.value : "" })}
                      />
                      {errorEthnicId !== "" ? <div className="error">{errorEthnicId}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Trình độ <span className="require-field">*</span>
                      </label>
                      <Select
                        options={levelList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn trình độ --"
                        onChange={(e) => setDetails({ ...details, levelId: e ? e.value : "" })}
                      />
                      {errorLevelId !== "" ? <div className="error">{errorLevelId}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Loại nhân viên <span className="require-field">*</span>
                      </label>
                      <Select
                        options={staffTypeList}
                        isSearchable={true}
                        placeholder="-- Chọn loại nhân viên --"
                        onChange={(e) => setDetails({ ...details, staffTypeId: e ? e.value : "" })}
                      />
                      {errorStaffTypeId !== "" ? <div className="error">{errorStaffTypeId}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Chức danh <span className="require-field">*</span>
                      </label>
                      <Select
                        options={titleList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn chức danh --"
                        onChange={(e) => setDetails({ ...details, titleId: e ? e.value : "" })}
                      />
                      {errorTitleId !== "" ? <div className="error">{errorTitleId}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Bằng cấp</label>
                      <Select
                        options={certifitcationList}
                        isSearchable={true}
                        isClearable={true}
                        placeholder="-- Chọn bằng cấp --"
                        onChange={(e) => setDetails({ ...details, certificationId: e ? e.value : "" })}
                      />
                      {errorCertificationId !== "" ? <div className="error">{errorCertificationId}</div> : ""}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Chuyên môn <span className="require-field">*</span>
                      </label>
                      <Select
                        isMulti
                        options={techniqueList}
                        isClearable={true}
                        isSearchable={true}
                        placeholder="-- Chọn chuyên môn --"
                        onChange={(e) => setDetails({ ...details, techniqueId: Array.isArray(e) ? e.map((x) => x.value) : [] })}
                      />
                      {errorTechniqueId !== "" ? <div className="error">{errorTechniqueId}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>
                        Trạng thái <span className="require-field">*</span>
                      </label>
                      <Select options={status} placeholder="-- Chọn trạng thái --" onChange={(e) => setDetails({ ...details, status: e ? e.value : "" })} />
                      {errorStatus !== "" ? <div className="error">{errorStatus}</div> : ""}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Người tạo</label>
                      <input type="text" className="form-control" value={user.first_name + " " + user.last_name} disabled />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="form-group">
                      <label>Ngày tạo</label>
                      <input type="text" className="form-control" value={helpers.getCurrentDay()} disabled />
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-footer">
                <button type="submit" className="btn btn-primary">
                  <i className="nav-icon fas fa-plus" /> Tạo nhân viên
                </button>
              </div>
            </form>
          </div>
        </section>
      </div>
    </div>
  );
};

export default LevelPage;
