import React, { useState } from "react";
import axios from "axios";

const Form = () => {
  const [selectedFile, setSelectedFile] = useState(null);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const formData = new FormData();
    formData.append("selectedFile", selectedFile);
    try {
      const response = await axios({
        method: "post",
        url: "https://freeimage.host/api/1/upload?key=xczxc",
        data: formData,
        headers: {
          "Accept-Language": "vi",
          "X-Authorization": "c27bf764096fe14c662232bbd9fbb837",
        },
      });
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };

  const handleFileSelect = (event) => {
    setSelectedFile(event.target.files[0]);
  };

  return (
    <div className="App">
      <div className="content-wrapper">
        <form onSubmit={handleSubmit}>
          <input type="file" onChange={handleFileSelect} />
          <input type="submit"></input>
        </form>
      </div>
    </div>
  );
};

export default Form;
