import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
import { useNavigate } from "react-router-dom";
import Loading from "../Loading";

const JobTitleListTable = () => {
  const [loading, setLoading] = useState(true);
  const [jobTitleList, setJobTitleList] = useState([]);
  const navigate = useNavigate();
  const business = helpers.getBusiness();

  useEffect(() => {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "job-title?page=1&business_id=" + business.id,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setLoading(false);
        setJobTitleList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const searchKeyword = (e) => {
    e.preventDefault();
    let keyword = e.target.value;

    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "job-title?page=1&business_id=" + business.id + "&keyword=" + keyword,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setJobTitleList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  };

  const viewDetail = (e, jobTitleId) => {
    e.preventDefault();
    localStorage.setItem("jobTitleId", JSON.stringify(jobTitleId));
    navigate("/job-title/detail");
    window.location.reload();
  };

  return loading ? (
    <div className="card text-center">
      <Loading></Loading>
    </div>
  ) : (
    <div className="card">
      <div className="card-header">
        <h3 className="card-title line-height-search">Danh sách chức danh nghề</h3>
        
        <div className="form-inline float-right">
          <div className="input-group" data-widget="sidebar-search">
            <input className="form-control form-control-sidebar" type="search" onChange={searchKeyword} placeholder="Tìm kiếm" aria-label="Search" />
            <div className="input-group-append">
              <button className="btn btn-sidebar">
                <i className="fas fa-search fa-fw" />
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="card-body">
        {jobTitleList && jobTitleList.length > 0 ? (
          <table id="" className="table table-bordered table-hover dataTable dtr-inline">
            <thead>
              <tr>
                <th className="stt">STT</th>
                <th style={{ width: "140px" }}>Mã chức danh nghề</th>
                <th style={{ width: "250px" }}>Tên chức danh nghề</th>
                <th className="staff-count">Số NV</th>
                <th className="create-user">Người tạo</th>
                <th className="create-date">Ngày tạo</th>
                <th className="update-user">Người sửa</th>
                <th className="update-date">Ngày sửa</th>
              </tr>
            </thead>
            <tbody>
              {jobTitleList.map((jobTitle, index) => (
                <tr onClick={(e) => viewDetail(e, jobTitle.id)}>
                  <td className="text-center">{index + 1}</td>
                  <td>{jobTitle.job_title_code}</td>
                  <td>{jobTitle.job_title_name}</td>
                  <td className="text-center">{jobTitle.staff_job_title_count}</td>
                  <td>{jobTitle.created_user.first_name + " " + jobTitle.created_user.last_name}</td>
                  <td>{helpers.convertDateTimeForApp(jobTitle.created_at)}</td>
                  <td>{jobTitle.updated_user.first_name + " " + jobTitle.updated_user.last_name}</td>
                  <td>{helpers.convertDateTimeForApp(jobTitle.updated_at)}</td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <>
            <div className="text-center mb-3">Không có chức danh nghề nghiệp nào</div>
          </>
        )}
      </div>
      {jobTitleList && jobTitleList.length > 0 ? (
        <div className="card-footer">
          <nav aria-label="Contacts Page Navigation">
            <ul className="pagination justify-content-center m-0">
              <li className="page-item active">
                <a className="page-link">1</a>
              </li>
            </ul>
          </nav>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};

export default JobTitleListTable;
