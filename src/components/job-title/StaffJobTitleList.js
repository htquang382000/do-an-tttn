import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import helpers from "../../helpers";
import AppEnvironment from "../../AppEnviroment";
import axios from "axios";
import Loading from "../../components/Loading";
import StaffListTableModal from "./StaffListTableModal";

const StaffJobTitleListTable = (data) => {
  const businessLocationId = helpers.getBusinessLocationId();
  const jobTitleId = data.jobTitleId;
  const navigate = useNavigate();

  const [loading, setLoading] = useState(true);
  const [staffList, setStaffList] = useState([]);

  useEffect(() => {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "job-title/get-staff-list?page=1&business_location_id=" + businessLocationId + "&job_title_id=" + jobTitleId,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setLoading(false);
        setStaffList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const searchKeyword = (e) => {
    e.preventDefault();
    let keyword = e.target.value;
    console.log(keyword);
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "job-title/get-staff-list?page=1&business_location_id=" + businessLocationId + "&job_title_id=" + jobTitleId + "&keyword=" + keyword,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        console.log(res.data.data.data);
        setStaffList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  };

  const viewDetail = (e, staffId) => {
    e.preventDefault();
    localStorage.setItem("staffId", staffId);
    navigate("/staff/detail");
    window.location.reload();
  };

  const deleteStaff = (e, staffId) => {
    console.log(staffId);
    e.preventDefault();
    const config = {
      method: "delete",
      url: AppEnvironment.urlBaseApi + "job-title/delete-staff",
      headers: AppEnvironment.headers,
      data: {
        job_title_id: jobTitleId,
        staff_id: staffId,
      },
    };

    axios(config)
      .then((res) => {
        console.log(res);
        window.location.reload();
      })
      .catch((err) => console.log(err));
  };

  return loading ? (
    <div>
      <div className="card card-solid">
        <Loading></Loading>
      </div>
    </div>
  ) : (
    <div>
      <div className="card card-solid">
        <div className="card-header">
          <h3 className="card-title" style={{ lineHeight: "38px" }}>
            Danh sách nhân viên của chức danh nghề ({staffList.length})
          </h3>
          <StaffListTableModal jobTitleId={jobTitleId}></StaffListTableModal>
        </div>
        <div className="form-inline float-right mt-3 ml-3">
          <div className="input-group" data-widget="sidebar-search">
            <input className="form-control form-control-sidebar" type="search" placeholder="Tìm kiếm" aria-label="Search" onChange={searchKeyword} />
            <div className="input-group-append">
              <button className="btn btn-sidebar">
                <i className="fas fa-search fa-fw" />
              </button>
            </div>
          </div>
        </div>
        <div className="card-body pb-0">
          {staffList && staffList.length > 0 ? (
            <div className="row">
              {staffList.map((staff) => (
                <div className="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column">
                  <div className="card bg-light d-flex flex-fill">
                    <div className="card-header text-muted border-bottom-0"></div>
                    <div className="card-body pt-0">
                      <div className="row">
                        <div className="col-7">
                          <h2 className="lead">
                            <b>{staff.staff.full_name}</b>
                          </h2>
                          <p className="text-muted text-sm">
                            <b>Mã nhân viên: </b>
                            {staff.staff.staff_code}
                          </p>
                          <ul className="ml-4 mb-0 fa-ul text-muted">
                            <li className="small">
                              <span className="fa-li">
                                <i className="fas fa-lg fa-genderless" />
                              </span>
                              Giới tính: {staff.staff.gender === 1 ? "Nam" : "Nữ"}
                            </li>
                            <li className="small mt-1">
                              <span className="fa-li">
                                <i className="fas fa-lg fa-birthday-cake" />
                              </span>
                              Năm sinh: {staff.staff.day_of_birth}
                            </li>
                            <li className="small mt-1">
                              <span className="fa-li">
                                <i className="fas fa-lg fa-user" />
                              </span>
                              CMND/CCCD: {staff.staff.nid}
                            </li>
                            <li className="small mt-1">
                              <span className="fa-li">
                                <i className="fas fa-lg fa-mail-bulk" />
                              </span>
                              Email: {staff.staff.email}
                            </li>
                            <li className="small mt-1">
                              <span className="fa-li">
                                <i className="fas fa-lg fa-phone" />
                              </span>
                              Số điện thoại: {staff.staff.mobile_phone}
                            </li>
                          </ul>
                        </div>
                        <div className="col-5 text-center">
                          <img
                            src={staff.staff.image3x4 ?? "https://png.pngtree.com/element_our/20200610/ourlarge/pngtree-default-avatar-image_2237213.jpg"}
                            alt="user-avatar"
                            className="img-circle img-fluid"
                          />
                        </div>
                      </div>
                    </div>
                    <div className="card-footer">
                      <div className="text-right">
                        <a className="btn btn-sm bg-danger float-left" onClick={(e) => deleteStaff(e, staff.staff.id)}>
                          <i className="fas fa-trash" /> Xóa khỏi danh sách
                        </a>
                        <a className="btn btn-sm bg-teal" onClick={(e) => viewDetail(e, staff.staff.id)}>
                          <i className="fas fa-user" /> Xem chi tiết
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          ) : (
            <div className="text-center mb-3">Không có nhân viên nào</div>
          )}
        </div>
        {staffList && staffList.length > 0 ? (
          <div className="card-footer">
            <nav aria-label="Contacts Page Navigation">
              <ul className="pagination justify-content-center m-0">
                <li className="page-item active">
                  <a className="page-link">1</a>
                </li>
              </ul>
            </nav>
          </div>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
};

export default StaffJobTitleListTable;
