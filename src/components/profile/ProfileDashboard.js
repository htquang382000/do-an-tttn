import React from "react";
import helpers from "../../helpers";

const ProfileDashboard = (user) => {
  user = user.user;
  return (
    <div className="card card-primary card-outline">
      <div className="card-body box-profile">
        <div className="text-center">
          <img
            className="profile-user-img img-fluid img-circle"
            src={user.image ?? "https://png.pngtree.com/element_our/20200610/ourlarge/pngtree-default-avatar-image_2237213.jpg"}
            alt="User profile picture"
          />
        </div>
        <h3 className="profile-username text-center">{user.first_name + " " + user.last_name}</h3>
        <p className="text-muted text-center">
          <b>Email: </b> {user.email}
        </p>
        <ul className="list-group list-group-unbordered mb-3">
          <li className="list-group-item">
            <b>Ngày tạo: </b> <a className="float-right">{helpers.convertDateTimeForApp(user.created_at)}</a>
          </li>
          <li className="list-group-item">
            <b>Ngày sửa gần nhất: </b> <a className="float-right">{helpers.convertDateTimeForApp(user.updated_at)}</a>
          </li>
          <li className="list-group-item">
            <b>Lần đăng nhập gần nhất: </b> <a className="float-right">{helpers.convertDateTimeForApp(user.last_login)}</a>
          </li>
          <li className="list-group-item">
            <b>Loại tài khoản: </b> <a className="float-right">{user.user_type === 1 ? "Quản trị viên" : "Nhân viên nhân sự"}</a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default ProfileDashboard;
