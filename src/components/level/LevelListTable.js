import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
import { useNavigate } from "react-router-dom";

import Loading from "../Loading";

const LevelListTable = () => {
  const [loading, setLoading] = useState(true);
  const [levelList, setlevelList] = useState([]);
  const navigate = useNavigate();
  const business = helpers.getBusiness();

  useEffect(() => {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "level?page=1&business_id=" + business.id,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setLoading(false);
        setlevelList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const searchKeyword = (e) => {
    e.preventDefault();
    let keyword = e.target.value;

    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "level?page=1&business_id=" + business.id + "&keyword=" + keyword,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setlevelList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  };

  const viewDetail = (e, level) => {
    e.preventDefault();
    localStorage.setItem("level", JSON.stringify(level));
    navigate("/level/detail");
    window.location.reload();
  };

  return loading ? (
    <div className="card text-center">
      <Loading></Loading>
    </div>
  ) : (
    <div className="card">
      <div className="card-header">
        <h3 className="card-title line-height-search">Danh sách trình độ</h3>
        <div className="form-inline float-right">
          <div className="input-group" data-widget="sidebar-search">
            <input className="form-control form-control-sidebar" type="search" placeholder="Tìm kiếm" aria-label="Search" onChange={searchKeyword} />
            <div className="input-group-append">
              <button className="btn btn-sidebar">
                <i className="fas fa-search fa-fw" />
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="card-body">
        {levelList && levelList.length > 0 ? (
          <table id="" className="table table-bordered table-hover dataTable dtr-inline">
            <thead>
              <tr>
                <th className="stt">STT</th>
                <th className="code">Mã trình độ</th>
                <th style={{ width: "250px" }}>Tên trình độ</th>
                <th className="priority">Độ ưu tiên</th>
                <th className="staff-count">Số NV</th>
                <th className="create-user">Người tạo</th>
                <th className="create-date">Ngày tạo</th>
                <th className="update-user">Người sửa</th>
                <th className="update-date">Ngày sửa</th>
              </tr>
            </thead>
            <tbody>
              {levelList.map((level, index) => (
                <tr onClick={(e) => viewDetail(e, level)}>
                  <td className="text-center">{index + 1}</td>
                  <td>{level.level_code}</td>
                  <td>{level.level_name}</td>
                  <td className="text-center">{level.priority}</td>
                  <td className="text-center">{level.staff_count}</td>
                  <td>{level.created_user.first_name + " " + level.created_user.last_name}</td>
                  <td>{helpers.convertDateTimeForApp(level.created_at)}</td>
                  <td>{level.updated_user.first_name + " " + level.updated_user.last_name}</td>
                  <td>{helpers.convertDateTimeForApp(level.updated_at)}</td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <>
            <div className="text-center mb-3">Không có trình độ học vấn nào</div>
          </>
        )}
      </div>
      {levelList && levelList.length > 0 ? (
        <div className="card-footer">
          <nav aria-label="Contacts Page Navigation">
            <ul className="pagination justify-content-center m-0">
              <li className="page-item active">
                <a className="page-link">1</a>
              </li>
            </ul>
          </nav>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};

export default LevelListTable;
