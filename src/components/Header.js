import React from "react";
import { useNavigate } from "react-router-dom";
const Header = () => {
  const navigate = useNavigate();
  const logout = (e) => {
    localStorage.clear();
    navigate("/");
    window.location.reload();
  };

  const selectLocation = (e) => {
    localStorage.removeItem("businessLocationId");
    navigate("/business-location");
    window.location.reload();
  };

  return (
    <div className="header">
      <nav className="navbar navbar-expand navbar-white navbar-light">
        <ul className="navbar-nav ml-auto">
          <li className="nav-item dropdown">
            <a className="nav-link" data-toggle="dropdown" href="#">
              <i className="fas fa-cog" />
            </a>
            <div className="dropdown-menu dropdown-menu-right">
              <a href="/profile" className="dropdown-item">
                <i className="fas fa-user mr-2" /> Thông tin cá nhân
              </a>
              <div className="dropdown-divider" />
              <a href="/profile/change-password" className="dropdown-item">
                <i className="fas fa-lock mr-2" /> Đổi mật khẩu
              </a>
              <div className="dropdown-divider" />
              <button type="submit" className="dropdown-item" onClick={(e) => selectLocation()}>
                <i className="fas fa-building mr-2" /> Đổi chi nhánh
              </button>
              <div className="dropdown-divider" />
              <button type="submit" className="dropdown-item" onClick={(e) => logout()}>
                <i className="fas fa-sign-out-alt mr-2" /> Đăng xuất
              </button>
            </div>
          </li>
          <li className="nav-item">
            <a className="nav-link" data-widget="fullscreen" role="button">
              <i className="fas fa-expand-arrows-alt" />
            </a>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Header;
