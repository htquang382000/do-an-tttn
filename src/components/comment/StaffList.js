import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import AppEnvironment from "../../AppEnviroment";
import axios from "axios";
import Loading from "../../components/Loading";
import helpers from "../../helpers";

const StaffListTable = (data) => {
  const business = helpers.getBusiness();
  const url = data.url;
  const navigate = useNavigate();

  const [details, setDetails] = useState({ staffId: "", content: "", type: "" });
  const [staffList, setStaffList] = useState([]);
  const [loading, setLoading] = useState(true);

  const [errorContent, setErrorContent] = useState("");
  const [errorType, setErrorType] = useState("");

  useEffect(() => {
    const config = {
      method: "get",
      url: url,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setLoading(false);
        setStaffList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const viewDetail = (e, staff) => {
    e.preventDefault();
    localStorage.setItem("staff", JSON.stringify(staff));
    navigate("/staff/comment");
    window.location.reload();
  };

  async function createComment(details) {
    var config = {
      method: "post",
      url: AppEnvironment.urlBaseApi + "comment/store",
      headers: AppEnvironment.headers,
      data: {
        business_id: business.id,
        staff_id: details.staffId ?? "",
        content: details.content ?? "",
        type: details.type ?? "",
      },
    };

    return axios(config)
      .then((response) => response.data)
      .catch(function (error) {
        console.log(error);
      });
  }

  const searchKeyword = (e) => {
    e.preventDefault();
    let keyword = e.target.value;

    const config = {
      method: "get",
      url: url + "&keyword=" + keyword,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setStaffList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  };

  const submitHanble = async (e) => {
    e.preventDefault();
    let response = await createComment(details);
    let code = response.code;
    if (code === 0) {
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.content) {
        setErrorContent(response.messageObject.content[0]);
      } else {
        setErrorContent("");
      }

      if (response.messageObject.type) {
        setErrorType(response.messageObject.type[0]);
      } else {
        setErrorType("");
      }
    }
  };

  return loading ? (
    <Loading></Loading>
  ) : (
    <div>
      <div className="card card-solid">
        <div className="card-header">
          <h3 className="card-title line-height-search">Danh sách nhân viên ({staffList.length})</h3>
          <div className="form-inline float-right">
            <div className="input-group" data-widget="sidebar-search">
              <input className="form-control form-control-sidebar" type="search" onChange={searchKeyword} placeholder="Tìm kiếm" aria-label="Search" />
              <div className="input-group-append">
                <button className="btn btn-sidebar">
                  <i className="fas fa-search fa-fw" />
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="card-body pb-0">
          {staffList && staffList.length > 0 ? (
            <div className="row">
              {staffList.map((staff) => (
                <div className="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column">
                  <div className="card bg-light d-flex flex-fill">
                    <div className="card-header text-muted border-bottom-0"></div>
                    <div className="card-body pt-0">
                      <div className="row">
                        <div className="col-7">
                          <h2 className="lead">
                            <b>{staff.full_name}</b>
                          </h2>
                          <p className="text-muted text-sm">
                            <b>Mã nhân viên: </b>
                            {staff.staff_code}
                          </p>
                          <ul className="ml-4 mb-0 fa-ul text-muted">
                            <li className="small">
                              <span className="fa-li">
                                <i className="fas fa-lg fa-genderless" />
                              </span>
                              Giới tính: {staff.gender === 1 ? "Nam" : "Nữ"}
                            </li>
                            <li className="small mt-1">
                              <span className="fa-li">
                                <i className="fas fa-lg fa-birthday-cake" />
                              </span>
                              Năm sinh: {staff.day_of_birth}
                            </li>
                            <li className="small mt-1">
                              <span className="fa-li">
                                <i className="fas fa-lg fa-user" />
                              </span>
                              CMND/CCCD: {staff.nid}
                            </li>
                            <li className="small mt-1">
                              <span className="fa-li">
                                <i className="fas fa-lg fa-mail-bulk" />
                              </span>
                              Email: {staff.email}
                            </li>
                            <li className="small mt-1">
                              <span className="fa-li">
                                <i className="fas fa-lg fa-phone" />
                              </span>
                              Số điện thoại: {staff.mobile_phone}
                            </li>
                          </ul>
                        </div>
                        <div className="col-5 text-center">
                          <img
                            src={staff.image3x4 ?? "https://png.pngtree.com/element_our/20200610/ourlarge/pngtree-default-avatar-image_2237213.jpg"}
                            alt="user-avatar"
                            className="img-circle img-fluid"
                          />
                        </div>
                      </div>
                    </div>
                    <div className="card-footer">
                      <div className="text-right">
                        <a className="btn btn-sm bg-blue float-left" onClick={(e) => viewDetail(e, staff)}>
                          <i className="fas fa-comments" /> Xem đánh giá
                        </a>
                        <a
                          className="btn btn-sm bg-teal "
                          data-toggle="modal"
                          data-target="#createComment"
                          onClick={(e) => setDetails({ ...details, staffId: staff.id, staffCode: staff.staff_code, fullName: staff.full_name })}
                        >
                          <i className="fas fa-comments" /> Viết đánh giá
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          ) : (
            <div className="text-center mb-3">Không có nhân viên nào</div>
          )}
          <div className="modal fade" id="createComment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLongTitle">
                    Thông tin đánh giá nhân viên
                  </h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form onSubmit={submitHanble}>
                  <div className="modal-body">
                    <div className="form-group">
                      <label>Mã nhân viên</label>
                      <input type="text" className="form-control" defaultValue={details.staffCode} disabled />
                    </div>
                    <div className="form-group">
                      <label>Họ tên nhân viên</label>
                      <input type="text" className="form-control" defaultValue={details.fullName} disabled />
                    </div>
                    <div className="form-group">
                      <label htmlFor="exampleInputEmail1">
                        Nội dung đánh giá <span className="require-field">*</span>
                      </label>
                      <textarea
                        className="form-control"
                        rows="4"
                        placeholder="Nhập nội dung đánh giá"
                        onChange={(e) => setDetails({ ...details, content: e.target.value })}
                      ></textarea>
                      {errorContent !== "" ? <div className="error">{errorContent}</div> : ""}
                    </div>
                    <div className="form-group">
                      <label>
                        Loại đánh giá <span className="require-field">*</span>
                      </label>
                      <div className="form-check mt-2">
                        <input className="form-check-input mr-4" type="radio" name="type" checked onChange={(e) => setDetails({ ...details, type: "" })} />
                        <label className="form-check-label mr-4">Không</label>
                        <input className="form-check-input ml-4" type="radio" name="type" onChange={(e) => setDetails({ ...details, type: 1 })} />
                        <label className="form-check-label ml-5">Đánh giá tốt</label>
                        <input className="form-check-input ml-4" type="radio" name="type" onChange={(e) => setDetails({ ...details, type: 2 })} />
                        <label className="form-check-label ml-5">Đánh giá xấu</label>
                        {errorType !== "" ? <div className="error">{errorType}</div> : ""}
                      </div>
                    </div>
                  </div>

                  <div className="modal-footer">
                    <button type="submit" className="btn btn-primary">
                      Thêm
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        {staffList && staffList.length > 0 ? (
          <div className="card-footer">
            <nav aria-label="Contacts Page Navigation">
              <ul className="pagination justify-content-center m-0">
                <li className="page-item active">
                  <a className="page-link" href="#">
                    1
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
};

export default StaffListTable;
