import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
import { useNavigate } from "react-router-dom";

import Loading from "../Loading";

const PositionListTable = () => {
  const [loading, setLoading] = useState(true);
  const [positionList, setPostionList] = useState([]);
  const navigate = useNavigate();
  const business = helpers.getBusiness();

  useEffect(() => {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "position?page=1&business_id=" + business.id,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setLoading(false);
        setPostionList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const viewDetail = (e, position) => {
    e.preventDefault();
    localStorage.setItem("position", JSON.stringify(position));
    navigate("/position/detail");
    window.location.reload();
  };

  const searchKeyword = (e) => {
    e.preventDefault();
    let keyword = e.target.value;

    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "position?page=1&business_id=" + business.id + "&keyword=" + keyword,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setPostionList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  };

  return loading ? (
    <div className="card text-center">
      <Loading></Loading>
    </div>
  ) : (
    <div className="card">
      <div className="card-header">
        <h3 className="card-title line-height-search">Danh sách chức vụ</h3>
        <div className="form-inline float-right">
          <div className="input-group" data-widget="sidebar-search">
            <input className="form-control form-control-sidebar" type="search" placeholder="Tìm kiếm" aria-label="Search" onChange={searchKeyword} />
            <div className="input-group-append">
              <button className="btn btn-sidebar">
                <i className="fas fa-search fa-fw" />
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="card-body">
        {positionList && positionList.length > 0 ? (
          <table id="" className="table table-bordered table-hover dataTable dtr-inline">
            <thead>
              <tr>
                <th className="stt">STT</th>
                <th className="code">Mã chức vụ</th>
                <th style={{ width: "250px" }}>Tên chức vụ</th>
                <th className="daily-wage">Lương ngày</th>
                <th className="priority">Độ ưu tiên</th>
                <th className="staff-count">Số NV</th>
                <th className="create-user">Người tạo</th>
                <th className="create-date">Ngày tạo</th>
                <th className="update-user">Người sửa</th>
                <th className="update-date">Ngày sửa</th>
              </tr>
            </thead>
            <tbody>
              {positionList.map((position, index) => (
                <tr onClick={(e) => viewDetail(e, position)}>
                  <td className="text-center">{index + 1}</td>
                  <td>{position.position_code}</td>
                  <td>{position.position_name}</td>
                  <td>{helpers.convertCurrency(position.daily_wage)}</td>
                  <td className="text-center">{position.priority}</td>
                  <td className="text-center">{position.staff_count}</td>
                  <td>{position.created_user.first_name + " " + position.created_user.last_name}</td>
                  <td>{helpers.convertDateTimeForApp(position.created_at)}</td>
                  <td>{position.updated_user.first_name + " " + position.updated_user.last_name}</td>
                  <td>{helpers.convertDateTimeForApp(position.updated_at)}</td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <>
            <div className="text-center mb-3">Không có chức vụ nào</div>
          </>
        )}
      </div>
      {positionList && positionList.length > 0 ? (
        <div className="card-footer">
          <nav aria-label="Contacts Page Navigation">
            <ul className="pagination justify-content-center m-0">
              <li className="page-item active">
                <a className="page-link">1</a>
              </li>
            </ul>
          </nav>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};

export default PositionListTable;
