import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
import { useNavigate } from "react-router-dom";

import Loading from "../../components/Loading";

const DepartmentListTable = () => {
  const business = helpers.getBusiness();
  const [loading, setLoading] = useState(true);
  const [departmentList, setDepartmentList] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "department?page=1&business_id=" + business.id,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setLoading(false);
        setDepartmentList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const viewDetail = (e, department) => {
    e.preventDefault();
    localStorage.setItem("department", JSON.stringify(department));
    navigate("/department/detail");
    window.location.reload();
  };

  const searchKeyword = (e) => {
    e.preventDefault();
    let keyword = e.target.value;

    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "department?page=1&business_id=" + business.id + "&keyword=" + keyword,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setDepartmentList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  };

  return loading ? (
    <div className="card text-center">
      <Loading></Loading>
    </div>
  ) : (
    <div className="card">
      <div className="card-header">
        <h3 className="card-title line-height-search">Danh sách phòng ban</h3>
        <div className="form-inline float-right">
          <div className="input-group" data-widget="sidebar-search">
            <input className="form-control form-control-sidebar" type="search" placeholder="Tìm kiếm" aria-label="Search" onChange={searchKeyword} />
            <div className="input-group-append">
              <button className="btn btn-sidebar">
                <i className="fas fa-search fa-fw" />
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="card-body">
        {departmentList && departmentList.length > 0 ? (
          <table id="" className="table table-bordered table-hover dataTable dtr-inline">
            <thead>
              <tr>
                <th className="stt">STT</th>
                <th className="code">Mã phòng ban</th>
                <th className="name">Tên phòng ban</th>
                <th className="staff-count">Số NV</th>
                <th className="create-user">Người tạo</th>
                <th className="create-date">Ngày tạo</th>
                <th className="update-user">Người sửa</th>
                <th className="update-date">Ngày sửa</th>
              </tr>
            </thead>
            <tbody>
              {departmentList.map((department, index) => (
                <tr onClick={(e) => viewDetail(e, department)}>
                  <td className="text-center">{index + 1}</td>
                  <td>{department.department_code}</td>
                  <td>{department.department_name}</td>
                  <td className="text-center">{department.staff_count}</td>
                  <td>{department.created_user.first_name + " " + department.created_user.last_name}</td>
                  <td>{helpers.convertDateTimeForApp(department.created_at)}</td>
                  <td>{department.updated_user.first_name + " " + department.updated_user.last_name}</td>
                  <td>{helpers.convertDateTimeForApp(department.updated_at)}</td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <>
            <div className="text-center mb-3">Không có bộ phận nào</div>
          </>
        )}
      </div>
      {departmentList && departmentList.length > 0 ? (
        <div className="card-footer">
          <nav aria-label="Contacts Page Navigation">
            <ul className="pagination justify-content-center m-0">
              <li className="page-item active">
                <a className="page-link">1</a>
              </li>
            </ul>
          </nav>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};

export default DepartmentListTable;
