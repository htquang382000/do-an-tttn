import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import { useNavigate } from "react-router-dom";
import GeneralApi from "../../api/general/GeneralApi";
import Select from "react-select";
import helpers from "../../helpers";

import Loading from "../../components/Loading";

const BusinessLocationList = () => {
  const business = helpers.getBusiness();

  const [loading, setLoading] = useState(true);
  const [businessLocationList, setBusinessLocationList] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "auth/business-location",
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setLoading(false);
        setBusinessLocationList(res.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const clickHandle = (e) => {
    e.preventDefault();
    const businessLocationId = e.target.value;
    if (typeof businessLocationId !== "undefined") {
      localStorage.setItem("businessLocationId", businessLocationId);
      navigate("/");
      window.location.reload();
    }
  };

  const [details, setDetails] = useState({ businessLocationName: "", address: "", city: "", district: "", ward: "", isDefault: 0 });

  const [cityList, setCityList] = useState([]);
  const [districtList, setDistrictList] = useState([]);
  const [wardList, setWardList] = useState([]);

  const [errorBusinessLocationName, setErrorBusinessLocationName] = useState("");
  const [errorAdress, setErrorAdress] = useState("");
  const [errorCity, setErrorCity] = useState("");
  const [errorDistrict, setErrorDistrict] = useState("");
  const [errorWard, setErrorWard] = useState("");
  const [errorIsDefault, setErrorIsDefault] = useState("");

  useEffect(() => {
    async function fetchData() {
      const cityList = await GeneralApi.cityList();
      setCityList(cityList);
    }

    fetchData();
  }, []);

  async function updateBusinessLocation(businessLocationId, details) {
    var config = {
      method: "put",
      url: AppEnvironment.urlBaseApi + "business-location/update",
      headers: AppEnvironment.headers,
      data: {
        business_id: business.id,
        business_location_id: businessLocationId,
        business_location_name: details.businessLocationName ?? "",
        address: details.address ?? "",
        city_id: details.city ? details.city.id ?? details.city.value : "",
        district_id: details.district ? details.district.id ?? details.district.value : "",
        ward_id: details.ward ? details.ward.id ?? details.ward.value : "",
        is_default: details.isDefault ?? 0,
      },
    };

    return axios(config)
      .then((response) => response.data)
      .catch(function (error) {
        console.log(error);
      });
  }

  const loadDistrict = async (e) => {
    setDetails({ ...details, city: e ?? "" });
    console.log(details);

    if (e !== null) {
      const districtList = await GeneralApi.districtList(e.value);
      setDistrictList(districtList);
    }
  };

  const loadWard = async (e) => {
    setDetails({ ...details, district: e ?? "" });
    console.log(details);

    if (e !== null) {
      const wardList = await GeneralApi.wardList(e.value);
      setWardList(wardList);
    }
  };

  const setBusinessLocation = (e, businessLocation) => {
    e.preventDefault();
    setDetails({
      ...details,
      businessLocationName: businessLocation.business_location_name,
      address: businessLocation.address,
      city: businessLocation.city,
      district: businessLocation.district,
      ward: businessLocation.ward,
      isDefault: businessLocation.is_default,
    });
  };

  const submitHanble = async (e, businessLocationId) => {
    e.preventDefault();
    console.log(details);
    let response = await updateBusinessLocation(businessLocationId, details);
    console.log(response);
    let code = response.code;
    if (code === 0) {
      window.location.reload();
    } else if (code === 2) {
      if (response.messageObject.business_location_name) {
        setErrorBusinessLocationName(response.messageObject.business_location_name[0]);
      } else {
        setErrorBusinessLocationName("");
      }

      if (response.messageObject.address) {
        setErrorAdress(response.messageObject.address[0]);
      } else {
        setErrorAdress("");
      }

      if (response.messageObject.city_id) {
        setErrorCity(response.messageObject.city_id[0]);
      } else {
        setErrorCity("");
      }

      if (response.messageObject.district_id) {
        setErrorDistrict(response.messageObject.district_id[0]);
      } else {
        setErrorDistrict("");
      }

      if (response.messageObject.ward_id) {
        setErrorWard(response.messageObject.ward_id[0]);
      } else {
        setErrorWard("");
      }

      if (response.messageObject.is_default) {
        setErrorIsDefault(response.messageObject.is_default[0]);
      } else {
        setErrorIsDefault("");
      }
    }
  };

  const deleteHandle = (e, businessLocationId) => {
    e.preventDefault();
    const config = {
      method: "delete",
      url: AppEnvironment.urlBaseApi + "business-location/destroy",
      headers: AppEnvironment.headers,
      data: {
        business_location_id: businessLocationId,
      },
    };

    axios(config)
      .then((res) => {
        window.location.reload();
      })
      .catch((err) => console.log(err));
  };

  return loading ? (
    <Loading></Loading>
  ) : (
    <div>
      {businessLocationList && businessLocationList.length > 0 ? (
        <>
          {businessLocationList.map((businessLocation) => (
            <div key={businessLocation.business_location.id} className="input-group mb-1 mt-3">
              {business.is_finished_setup === 1 ? (
                <button type="submit" className="btn btn-block btn-default btn-lg" value={businessLocation.business_location.id} onClick={(e) => clickHandle(e, "value")}>
                  {businessLocation.business_location.business_location_name} <br />
                  <p className="business-location-address">
                    {businessLocation.business_location.address}, {businessLocation.business_location.ward.name}, {businessLocation.business_location.district.name}, {businessLocation.business_location.city.name}
                    <br />
                    {businessLocation.business_location.is_default === 1 ? <b>(Mặc định)</b> : ""}
                  </p>
                </button>
              ) : (
                <>
                  <button
                    type="submit"
                    className="btn btn-block btn-default btn-lg"
                    data-toggle="modal"
                    data-target={"#updateModal" + businessLocation.business_location.id}
                    onClick={(e) => setBusinessLocation(e, businessLocation)}
                  >
                    {businessLocation.business_location.business_location_name} <br />
                    <p className="business-location-address">
                      {businessLocation.business_location.address}, {businessLocation.business_location.ward.name}, {businessLocation.business_location.district.name}, {businessLocation.business_location.city.name}
                      <br />
                      {businessLocation.business_location.is_default === 1 ? <b>(Mặc định)</b> : ""}
                    </p>
                  </button>
                  <div className="modal fade" id={"updateModal" + businessLocation.business_location.id} tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                      <div className="modal-content">
                        <div className="modal-header">
                          <h5 className="modal-title" id="exampleModalLongTitle">
                            Thông tin chi nhánh doanh nghiệp
                          </h5>
                          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <form onSubmit={(e) => submitHanble(e, businessLocation.business_location.id)}>
                          <div className="modal-body">
                            <div className="form-group">
                              <label htmlFor="exampleInputEmail1">
                                Tên chi nhánh doanh nghiệp <span className="require-field">*</span>
                              </label>
                              <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập tên chi nhánh doanh nghiệp"
                                defaultValue={businessLocation.business_location_name}
                                onChange={(e) => setDetails({ ...details, businessLocationName: e.target.value })}
                              />
                              {errorBusinessLocationName !== "" ? <div className="error">{errorBusinessLocationName}</div> : ""}
                            </div>
                            <div className="form-group">
                              <label htmlFor="exampleInputEmail1">
                                Địa chỉ <span className="require-field">*</span>
                              </label>
                              <input
                                type="text"
                                className="form-control"
                                placeholder="Nhập địa chỉ chi nhánh doanh nghiệp"
                                defaultValue={businessLocation.address}
                                onChange={(e) => setDetails({ ...details, address: e.target.value })}
                              />
                              {errorAdress !== "" ? <div className="error">{errorAdress}</div> : ""}
                            </div>
                            <div className="row">
                              <div className="col-sm-4">
                                <div className="form-group">
                                  <label>
                                    Tỉnh/Thành phố <span className="require-field">*</span>
                                  </label>
                                  <Select
                                    options={cityList}
                                    isSearchable={true}
                                    isClearable={true}
                                    placeholder="-- Chọn tỉnh/thành phố --"
                                    defaultValue={{ value: businessLocation.city.id, label: businessLocation.city.name }}
                                    onChange={loadDistrict}
                                  />
                                  {errorCity !== "" ? <div className="error">{errorCity}</div> : ""}
                                </div>
                              </div>
                              <div className="col-sm-4">
                                <div className="form-group">
                                  <label>
                                    Quận/Huyện <span className="require-field">*</span>
                                  </label>
                                  <Select
                                    options={districtList}
                                    isSearchable={true}
                                    isClearable={true}
                                    placeholder="-- Chọn quận/huyện --"
                                    defaultValue={{ value: businessLocation.district.id, label: businessLocation.district.name }}
                                    onChange={loadWard}
                                  />
                                  {errorDistrict !== "" ? <div className="error">{errorDistrict}</div> : ""}
                                </div>
                              </div>
                              <div className="col-sm-4">
                                <div className="form-group">
                                  <label>
                                    Phường/Xã <span className="require-field">*</span>
                                  </label>
                                  <Select
                                    options={wardList}
                                    isSearchable={true}
                                    isClearable={true}
                                    placeholder="-- Chọn phường/xã --"
                                    defaultValue={{ value: businessLocation.ward.id, label: businessLocation.ward.name }}
                                    onChange={(e) => setDetails({ ...details, ward: e ?? "" })}
                                  />
                                  {errorWard !== "" ? <div className="error">{errorWard}</div> : ""}
                                </div>
                              </div>
                            </div>
                            <div className="form-group">
                              <div className="custom-control custom-switch">
                                <input
                                  type="checkbox"
                                  className="custom-control-input"
                                  id="customSwitch1"
                                  checked={businessLocation.is_default === 1 ? true : false}
                                  onChange={(e) => setDetails({ ...details, isDefault: e.target.checked === true ? 1 : 0 })}
                                />
                                <label className="custom-control-label" htmlFor="customSwitch1">
                                  Chi nhánh doanh nghiệp mặc định
                                </label>
                              </div>
                            </div>
                          </div>

                          <div className="modal-footer" style={{ display: "block" }}>
                            <button type="button" className="btn btn-danger" onClick={(e) => deleteHandle(e, businessLocation.id)}>
                              Xóa
                            </button>
                            <button type="submit" className="btn btn-primary float-right">
                              Cập nhật
                            </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </>
              )}
            </div>
          ))}
        </>
      ) : (
        <div className="text-center mb-3">Không có chi nhánh doanh nghiệp nào</div>
      )}
    </div>
  );
};

export default BusinessLocationList;
