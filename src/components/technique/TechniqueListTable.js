import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
import { useNavigate } from "react-router-dom";

import Loading from "../Loading";

const TechniqueListTable = () => {
  const [loading, setLoading] = useState(true);
  const [techniqueList, setTechniqueList] = useState([]);
  const navigate = useNavigate();
  const business = helpers.getBusiness();

  useEffect(() => {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "technique?page=1&business_id=" + business.id,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setLoading(false);
        setTechniqueList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const searchKeyword = (e) => {
    e.preventDefault();
    let keyword = e.target.value;

    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "technique?page=1&business_id=" + business.id + "&keyword=" + keyword,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setTechniqueList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  };

  const viewDetail = (e, technique) => {
    e.preventDefault();
    localStorage.setItem("technique", JSON.stringify(technique));
    navigate("/technique/detail");
    window.location.reload();
  };

  return loading ? (
    <div className="card text-center">
      <Loading></Loading>
    </div>
  ) : (
    <div className="card">
      <div className="card-header">
        <h3 className="card-title line-height-search">Danh sách chuyên môn</h3>
        <div className="form-inline float-right">
          <div className="input-group" data-widget="sidebar-search">
            <input className="form-control form-control-sidebar" type="search" onChange={searchKeyword} placeholder="Tìm kiếm" aria-label="Search" />
            <div className="input-group-append">
              <button className="btn btn-sidebar">
                <i className="fas fa-search fa-fw" />
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="card-body">
        {techniqueList && techniqueList.length > 0 ? (
          <table id="" className="table table-bordered table-hover dataTable dtr-inline">
            <thead>
              <tr>
                <th className="stt">STT</th>
                <th className="code">Mã chuyên môn</th>
                <th className="name">Tên chuyên môn</th>
                <th className="create-user">Người tạo</th>
                <th className="create-date">Ngày tạo</th>
                <th className="update-user">Người sửa</th>
                <th className="update-date">Ngày sửa</th>
              </tr>
            </thead>
            <tbody>
              {techniqueList.map((technique, index) => (
                <tr onClick={(e) => viewDetail(e, technique)}>
                  <td className="text-center">{index + 1}</td>
                  <td>{technique.technique_code}</td>
                  <td>{technique.technique_name}</td>
                  <td>{technique.created_user.first_name + " " + technique.created_user.last_name}</td>
                  <td>{helpers.convertDateTimeForApp(technique.created_at)}</td>
                  <td>{technique.updated_user.first_name + " " + technique.updated_user.last_name}</td>
                  <td>{helpers.convertDateTimeForApp(technique.updated_at)}</td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <>
            <div className="text-center mb-3">Không có chuyên môn nào</div>
          </>
        )}
      </div>
      {techniqueList && techniqueList.length > 0 ? (
        <div className="card-footer">
          <nav aria-label="Contacts Page Navigation">
            <ul className="pagination justify-content-center m-0">
              <li className="page-item active">
                <a className="page-link">1</a>
              </li>
            </ul>
          </nav>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};

export default TechniqueListTable;
