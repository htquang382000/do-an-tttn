import React, { useState, useEffect } from "react";
import AppEnvironment from "../AppEnviroment";
import axios from "axios";
import Loading from "./Loading";
import helpers from "../helpers";

const Home = () => {
  const business = helpers.getBusiness();
  const businessLocationId = helpers.getBusinessLocationId();

  const [staffCount, setStaffCount] = useState([]);
  const [jobTitleCount, setJobTitleCount] = useState([]);
  const [businessTripCount, setBusinessTripCount] = useState([]);
  const [userCount, setUserCount] = useState([]);
  const [loading, setLoading] = useState(true);

  async function getStaffCount() {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "staff?status=1&page=1&business_id=" + business.id + "&business_location_id=" + businessLocationId,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setStaffCount(res.data.data.total);
      })
      .catch((err) => console.log(err));
  }

  async function getJobTitleCount() {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "job-title?page=1&business_id=" + business.id,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setJobTitleCount(res.data.data.total);
      })
      .catch((err) => console.log(err));
  }

  async function getBusinessTripCount() {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "business-trip?page=1&business_location_id=" + businessLocationId + "&status[]=1&status[]=2&status[]=3&status[]=4",
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setBusinessTripCount(res.data.data.total);
      })
      .catch((err) => console.log(err));
  }

  async function getUserCount() {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "user?page=1",
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setLoading(false);
        setUserCount(res.data.data.total);
      })
      .catch((err) => console.log(err));
  }

  useEffect(() => {
    async function fetchData() {
      const staffCount = await getStaffCount();
      setStaffCount(staffCount);

      const jobTitleCount = await getJobTitleCount();
      setJobTitleCount(jobTitleCount);

      const businessTripCount = await getBusinessTripCount();
      setBusinessTripCount(businessTripCount);

      const userCount = await getUserCount();
      setUserCount(userCount);
    }

    fetchData();
  }, []);

  return loading ? (
    <div className="home">
      <div className="content-wrapper">
        <Loading></Loading>
      </div>
    </div>
  ) : (
    <div className="home">
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0">Thống kê</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">Tổng quan</li>
                  <li className="breadcrumb-item active">Thống kê</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <section className="content">
          <div className="container-fluid">
            <div className="row">
              <div className="col-lg-3 col-6">
                <div className="small-box bg-info">
                  <div className="inner">
                    <h3>{staffCount}</h3>
                    <p>Nhân viên</p>
                  </div>
                  <div className="icon">
                    <i className="ion ion-person" />
                  </div>
                  <a href="/staff" className="small-box-footer">
                    Xem thêm <i className="fas fa-arrow-circle-right" />
                  </a>
                </div>
              </div>
              <div className="col-lg-3 col-6">
                <div className="small-box bg-warning">
                  <div className="inner">
                    <h3>{jobTitleCount}</h3>
                    <p>Chức danh nghề nghiệp</p>
                  </div>
                  <div className="icon">
                    <i className="ion ion-person-stalker" />
                  </div>
                  <a href="/job-title" className="small-box-footer">
                    Xem thêm <i className="fas fa-arrow-circle-right" />
                  </a>
                </div>
              </div>
              <div className="col-lg-3 col-6">
                <div className="small-box bg-success">
                  <div className="inner">
                    <h3>{businessTripCount}</h3>
                    <p>Chuyến công tác</p>
                  </div>
                  <div className="icon">
                    <i className="ion ion-briefcase" />
                  </div>
                  <a href="/business-trip" className="small-box-footer">
                    Xem thêm <i className="fas fa-arrow-circle-right" />
                  </a>
                </div>
              </div>
              <div className="col-lg-3 col-6">
                <div className="small-box bg-danger">
                  <div className="inner">
                    <h3>{userCount}</h3>
                    <p>Tài khoản</p>
                  </div>
                  <div className="icon">
                    <i className="ion ion-locked" />
                  </div>
                  <a href="/user" className="small-box-footer">
                    Xem thêm <i className="fas fa-arrow-circle-right" />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};

export default Home;
