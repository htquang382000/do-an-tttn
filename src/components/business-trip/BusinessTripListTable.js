import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
import { useNavigate } from "react-router-dom";
import Loading from "../Loading";

const BusinessTripListTable = () => {
  const [loading, setLoading] = useState(true);
  const [businessTripList, setBusinessTripList] = useState([]);
  const navigate = useNavigate();
  const businessLocationId = helpers.getBusinessLocationId();

  useEffect(() => {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "business-trip?page=1&business_location_id=" + businessLocationId + "&status[]=1&status[]=2&status[]=3&status[]=4",
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setLoading(false);
        setBusinessTripList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const searchKeyword = (e) => {
    e.preventDefault();
    let keyword = e.target.value;

    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "business-trip?page=1&business_location_id=" + businessLocationId + "&status[]=1&status[]=2&status[]=3&status[]=4" + "&keyword=" + keyword,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setBusinessTripList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  };

  const viewDetail = (e, businessTripId) => {
    e.preventDefault();
    localStorage.setItem("businessTripId", businessTripId);
    navigate("/business-trip/detail");
    window.location.reload();
  };

  return loading ? (
    <div className="card text-center">
      <Loading></Loading>
    </div>
  ) : (
    <div className="card">
      <div className="card-header">
        <h3 className="card-title line-height-search">Danh sách chuyến công tác</h3>
        <div>
          <div className="form-inline float-right">
            <div className="input-group" data-widget="sidebar-search">
              <input className="form-control form-control-sidebar" type="search" placeholder="Tìm kiếm" aria-label="Search" onChange={searchKeyword} />
              <div className="input-group-append">
                <button className="btn btn-sidebar">
                  <i className="fas fa-search fa-fw" />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="card-body">
        {businessTripList && businessTripList.length > 0 ? (
          <table id="" className="table table-bordered table-hover dataTable dtr-inline">
            <thead>
              <tr>
                <th className="stt">STT</th>
                <th style={{ width: "120px" }}>Mã công tác</th>
                <th style={{ width: "250px" }}>Tên chuyến công tác</th>
                <th style={{ width: "250px" }}>Chức danh nghề</th>
                <th className="staff-count">Số NV</th>
                <th style={{ width: "120px" }}>Trạng thái</th>
                <th className="create-user">Người tạo</th>
                <th className="create-date">Ngày tạo</th>
              </tr>
            </thead>
            <tbody>
              {businessTripList.map((businessTrip, index) => (
                <tr onClick={(e) => viewDetail(e, businessTrip.id)}>
                  <td className="text-center">{index + 1}</td>
                  <td>{businessTrip.business_trip_code}</td>
                  <td>{businessTrip.business_trip_name}</td>
                  <td>
                    {businessTrip.job_title.job_title_name} - {businessTrip.job_title.job_title_code}
                  </td>
                  <td className="text-center">{businessTrip.staff_business_trip_count}</td>
                  <td className="text-center">
                    {businessTrip.status === 1 ? (
                      <span className="badge bg-secondary" style={{ width: "120px" }}>
                        Chưa thực hiện
                      </span>
                    ) : businessTrip.status === 2 ? (
                      <span className="badge bg-primary" style={{ width: "120px" }}>
                        Đang thực hiện
                      </span>
                    ) : businessTrip.status === 3 ? (
                      <span className="badge bg-success" style={{ width: "120px" }}>
                        Đã hoàn thành
                      </span>
                    ) : (
                      <span className="badge bg-danger" style={{ width: "120px" }}>
                        Đã hủy
                      </span>
                    )}
                  </td>
                  <td>{businessTrip.created_user.first_name + " " + businessTrip.created_user.last_name}</td>
                  <td>{helpers.convertDateTimeForApp(businessTrip.created_at)}</td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <div className="text-center mb-3">Không có chuyến công tác nào</div>
        )}
      </div>
      {businessTripList && businessTripList.length > 0 ? (
        <div className="card-footer">
          <nav aria-label="Contacts Page Navigation">
            <ul className="pagination justify-content-center m-0">
              <li className="page-item active">
                <a className="page-link" href="#">
                  1
                </a>
              </li>
            </ul>
          </nav>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};

export default BusinessTripListTable;
