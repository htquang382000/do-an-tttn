import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
// import { useNavigate } from "react-router-dom";

import Loading from "../Loading";

const StaffListTableModal = (data) => {
  const [loading, setLoading] = useState(true);
  const [staffJobTitleList, setStaffJobTitleList] = useState([]);
  const [staffBusinessTripList, setStaffBusinessTripList] = useState([]);
  // const navigate = useNavigate();

  const businessTripId = data.businessTripId;
  const jobTitleId = data.jobTitleId;
  const businessLocationId = helpers.getBusinessLocationId();

  useEffect(() => {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "job-title/get-staff-list?business_location_id=" + businessLocationId + "&job_title_id=" + jobTitleId,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setLoading(false);
        setStaffJobTitleList(res.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  useEffect(() => {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "business-trip/get-staff-list?business_trip_id=" + businessTripId,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setLoading(false);
        let array = res.data.data.map((staffBusinessTrip) => staffBusinessTrip.staff_id);
        setStaffBusinessTripList(array);
      })
      .catch((err) => console.log(err));
  }, []);

  const searchKeyword = (e) => {
    e.preventDefault();
    let keyword = e.target.value;
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "job-title/get-staff-list?business_location_id=" + businessLocationId + "&job_title_id=" + jobTitleId + "&keyword=" + keyword,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setStaffJobTitleList(res.data.data);
      })
      .catch((err) => console.log(err));
  };

  const changeStaffBusinessTrip = (e, staffId) => {
    e.preventDefault();
    if (staffBusinessTripList.includes(staffId)) {
      e.currentTarget.className = "btn btn-lg bg-teal";
      e.currentTarget.text = "Thêm";
      staffBusinessTripList.pop(staffId);
    } else {
      e.currentTarget.className = "btn btn-lg bg-danger";
      e.currentTarget.text = "Xóa";
      staffBusinessTripList.push(staffId);
    }
  };

  async function addStaffToBusinessTrip(staffBusinessTripList) {
    var config = {
      method: "post",
      url: AppEnvironment.urlBaseApi + "business-trip/add-staff",
      headers: AppEnvironment.headers,
      data: {
        business_trip_id: businessTripId,
        staff_id: staffBusinessTripList,
      },
    };

    return axios(config)
      .then((response) => response.data)
      .catch(function (error) {
        console.log(error);
      });
  }

  const submitHandle = async () => {
    let response = await addStaffToBusinessTrip(staffBusinessTripList);
    let code = response.code;
    if (code === 0) {
      window.location.reload();
    } else {
    }
  };

  return loading ? (
    <div className="card text-center">
      <Loading></Loading>
    </div>
  ) : (
    <div>
      <button type="button" className="btn bg-primary float-right" data-toggle="modal" data-target="#exampleModalCenter">
        <i className="fas fa-plus" /> Thêm nhân viên
      </button>
      <div className="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered modal-lg" role="document" style={{ maxWidth: "1300px" }}>
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLongTitle">
                <b>Danh sách nhân viên</b> (được lọc bởi các điều kiện của chuyến công tác)
              </h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="form-inline float-right mt-3 ml-3">
              <div className="input-group" data-widget="sidebar-search">
                <input className="form-control form-control-sidebar" type="search" placeholder="Tìm kiếm" aria-label="Search" onChange={searchKeyword} />
                <div className="input-group-append">
                  <button className="btn btn-sidebar">
                    <i className="fas fa-search fa-fw" />
                  </button>
                </div>
              </div>
            </div>
            <div className="modal-body">
              {staffJobTitleList && staffJobTitleList.length > 0 ? (
                <table id="" className="table table-bordered table-hover dataTable dtr-inline">
                  <thead>
                    <tr>
                      <th style={{ width: "10px" }}>STT</th>
                      <th style={{ width: "120px" }}>Mã NV</th>
                      <th style={{ width: "300px" }}>Tên NV</th>
                      <th style={{ width: "20px" }}>Tuổi</th>
                      <th style={{ width: "120px" }}>Giới tính</th>
                      <th style={{ width: "200px" }}>Chức danh</th>
                      <th style={{ width: "200px" }}>Trình độ</th>
                      <th style={{ width: "200px" }}>Bằng cấp</th>
                      <th style={{ width: "200px" }}>Chức vụ</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {staffJobTitleList.map((staff_job_title, index) => (
                      <tr>
                        <td className="text-center">{index + 1}</td>
                        <td>{staff_job_title.staff.staff_code}</td>
                        <td>{staff_job_title.staff.full_name}</td>
                        <td className="text-center">{helpers.calculateAge(staff_job_title.staff.day_of_birth)}</td>
                        <td className="text-center">{staff_job_title.staff.gender === 1 ? "Nam" : "Nữ"}</td>
                        <td>{staff_job_title.staff.title.title_name}</td>
                        <td>{staff_job_title.staff.level.level_name}</td>
                        <td>{staff_job_title.staff.certification.certification_name}</td>
                        <td>{staff_job_title.staff.position.position_name}</td>
                        <td>
                          <a
                            style={{ width: "100px" }}
                            className={staffBusinessTripList.includes(staff_job_title.staff.id) ? "btn btn-lg bg-danger" : "btn btn-lg bg-teal"}
                            onClick={(e) => changeStaffBusinessTrip(e, staff_job_title.staff.id)}
                          >
                            {staffBusinessTripList.includes(staff_job_title.staff.id) ? "Xóa" : "Thêm"}
                          </a>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              ) : (
                <div className="text-center mb-3">Không có nhân viên nào được lọc bởi điều kiện.</div>
              )}
            </div>
            <div className="modal-footer">
              <a className="btn btn-primary" onClick={submitHandle}>
                Hoàn tất
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default StaffListTableModal;
