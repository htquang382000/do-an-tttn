import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
import { useNavigate } from "react-router-dom";

import Loading from "../Loading";

const StaffTypeListTable = () => {
  const [loading, setLoading] = useState(true);
  const [staffTypeList, setStaffTypeList] = useState([]);
  const navigate = useNavigate();
  const business = helpers.getBusiness();

  useEffect(() => {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "staff-type?page=1&business_id=" + business.id,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setLoading(false);
        setStaffTypeList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const searchKeyword = (e) => {
    e.preventDefault();
    let keyword = e.target.value;
    console.log(keyword);

    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "staff-type?page=1&business_id=" + business.id + "&keyword=" + keyword,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setStaffTypeList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  };

  const viewDetail = (e, staffType) => {
    e.preventDefault();
    localStorage.setItem("staffType", JSON.stringify(staffType));
    navigate("/staff-type/detail");
    window.location.reload();
  };

  return loading ? (
    <div className="card text-center">
      <Loading></Loading>
    </div>
  ) : (
    <div className="card">
      <div className="card-header">
        <h3 className="card-title line-height-search">Danh sách loại nhân viên</h3>
        <div className="form-inline float-right">
          <div className="input-group" data-widget="sidebar-search">
            <input className="form-control form-control-sidebar" type="search" onChange={searchKeyword} placeholder="Tìm kiếm" aria-label="Search" />
            <div className="input-group-append">
              <button className="btn btn-sidebar">
                <i className="fas fa-search fa-fw" />
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="card-body">
        {staffTypeList && staffTypeList.length > 0 ? (
          <table id="" className="table table-bordered table-hover dataTable dtr-inline">
            <thead>
              <tr>
                <th className="stt">STT</th>
                <th className="code">Mã loại nhân viên</th>
                <th style={{ width: "250px" }}>Tên loại nhân viên</th>
                <th className="staff-count">Số NV</th>
                <th className="create-user">Người tạo</th>
                <th className="create-date">Ngày tạo</th>
                <th className="update-user">Người sửa</th>
                <th className="update-date">Ngày sửa</th>
              </tr>
            </thead>
            <tbody>
              {staffTypeList.map((staffType, index) => (
                <tr onClick={(e) => viewDetail(e, staffType)}>
                  <td className="text-center">{index + 1}</td>
                  <td>{staffType.staff_type_code}</td>
                  <td>{staffType.staff_type_name}</td>
                  <td className="text-center">{staffType.staff_count}</td>
                  <td>{staffType.created_user.first_name + " " + staffType.created_user.last_name}</td>
                  <td>{helpers.convertDateTimeForApp(staffType.created_at)}</td>
                  <td>{staffType.updated_user.first_name + " " + staffType.updated_user.last_name}</td>
                  <td>{helpers.convertDateTimeForApp(staffType.updated_at)}</td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <>
            <div className="text-center mb-3">Không có loại nhân viên nào</div>
          </>
        )}
      </div>
      <div className="card-footer">
        <nav aria-label="Contacts Page Navigation">
          <ul className="pagination justify-content-center m-0">
            <li className="page-item active">
              <a className="page-link" href="#">
                1
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
};

export default StaffTypeListTable;
