import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
import { useNavigate } from "react-router-dom";

import Loading from "../Loading";

const TitleListTable = () => {
  const [loading, setLoading] = useState(true);
  const [titleList, setTitleList] = useState([]);
  const navigate = useNavigate();
  const business = helpers.getBusiness();

  useEffect(() => {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "title?page=1&business_id=" + business.id,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setLoading(false);
        setTitleList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const searchKeyword = (e) => {
    e.preventDefault();
    let keyword = e.target.value;

    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "title?page=1&business_id=" + business.id + "&keyword=" + keyword,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setTitleList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  };

  const viewDetail = (e, title) => {
    e.preventDefault();
    localStorage.setItem("title", JSON.stringify(title));
    navigate("/title/detail");
    window.location.reload();
  };

  return loading ? (
    <div className="card text-center">
      <Loading></Loading>
    </div>
  ) : (
    <div className="card">
      <div className="card-header">
        <h3 className="card-title line-height-search">Danh sách chức danh</h3>
        <div className="form-inline float-right">
          <div className="input-group" data-widget="sidebar-search">
            <input className="form-control form-control-sidebar" type="search" onChange={searchKeyword} placeholder="Tìm kiếm" aria-label="Search" />
            <div className="input-group-append">
              <button className="btn btn-sidebar">
                <i className="fas fa-search fa-fw" />
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="card-body">
        {titleList && titleList.length > 0 ? (
          <table id="" className="table table-bordered table-hover dataTable dtr-inline">
            <thead>
              <tr>
                <th className="stt">STT</th>
                <th className="code">Mã chức danh</th>
                <th style={{ width: "250px" }}>Tên chức danh</th>
                <th className="staff-count">Số NV</th>
                <th className="create-user">Người tạo</th>
                <th className="create-date">Ngày tạo</th>
                <th className="update-user">Người sửa</th>
                <th className="update-date">Ngày sửa</th>
              </tr>
            </thead>
            <tbody>
              {titleList.map((title, index) => (
                <tr onClick={(e) => viewDetail(e, title)}>
                  <td className="text-center">{index + 1}</td>
                  <td>{title.title_code}</td>
                  <td>{title.title_name}</td>
                  <td className="text-center">{title.staff_count}</td>
                  <td>{title.created_user.first_name + " " + title.created_user.last_name}</td>
                  <td>{helpers.convertDateTimeForApp(title.created_at)}</td>
                  <td>{title.updated_user.first_name + " " + title.updated_user.last_name}</td>
                  <td>{helpers.convertDateTimeForApp(title.updated_at)}</td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <>
            <div className="text-center mb-3">Không có chức danh nào</div>
          </>
        )}
      </div>
      {titleList && titleList.length > 0 ? (
        <div className="card-footer">
          <nav aria-label="Contacts Page Navigation">
            <ul className="pagination justify-content-center m-0">
              <li className="page-item active">
                <a className="page-link">1</a>
              </li>
            </ul>
          </nav>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};

export default TitleListTable;
