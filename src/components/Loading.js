import loading from "../assets/images/loading.gif";
const LoadingIcon = () => (
  <div className="text-center" style={{ backgroundColor: "white"}}>
    <img src={loading} alt="Loading..." style={{ align: "center" }}></img>
  </div>
);

export default LoadingIcon;
