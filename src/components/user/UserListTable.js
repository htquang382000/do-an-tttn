import React, { useState, useEffect } from "react";
import axios from "axios";
import AppEnvironment from "../../AppEnviroment";
import helpers from "../../helpers";
import { useNavigate } from "react-router-dom";

import Loading from "../Loading";

const UserListTable = () => {
  const [loading, setLoading] = useState(true);
  const [userList, setUserList] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "user?page=1",
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setLoading(false);
        setUserList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const searchKeyword = (e) => {
    e.preventDefault();
    let keyword = e.target.value;

    const config = {
      method: "get",
      url: AppEnvironment.urlBaseApi + "user?page=1&keyword=" + keyword,
      headers: AppEnvironment.headers,
    };

    axios(config)
      .then((res) => {
        setUserList(res.data.data.data);
      })
      .catch((err) => console.log(err));
  };

  const viewDetail = (e, userId) => {
    e.preventDefault();
    localStorage.setItem("userId", userId);
    navigate("/user/detail");
    window.location.reload();
  };

  return loading ? (
    <div className="card text-center">
      <Loading></Loading>
    </div>
  ) : (
    <div className="card">
      <div className="card-header">
        <h3 className="card-title line-height-search">Danh sách tài khoản</h3>
        <div className="form-inline float-right">
          <div className="input-group" data-widget="sidebar-search">
            <input className="form-control form-control-sidebar" type="search" onChange={searchKeyword} placeholder="Tìm kiếm" aria-label="Search" />
            <div className="input-group-append">
              <button className="btn btn-sidebar">
                <i className="fas fa-search fa-fw" />
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="card-body">
        {userList && userList.length > 0 ? (
          <table id="" className="table table-bordered table-hover dataTable dtr-inline">
            <thead>
              <tr>
                <th className="stt">STT</th>
                <th style={{ width: "90px" }}>Ảnh</th>
                <th style={{ width: "220px" }}>Họ và tên</th>
                <th style={{ width: "200px" }}>Email</th>
                <th style={{ width: "150px" }}>Số điện thoại</th>
                <th style={{ width: "120px" }}>Trạng thái</th>
              </tr>
            </thead>
            <tbody>
              {userList.map((user, index) => (
                <tr onClick={(e) => viewDetail(e, user.id)}>
                  <td>{index + 1}</td>
                  <td className="text-center">
                    <img
                      className="profile-user-img img-fluid img-circle"
                      src={user.image ?? "https://png.pngtree.com/element_our/20200610/ourlarge/pngtree-default-avatar-image_2237213.jpg"}
                      alt="User profile picture"
                    />
                  </td>
                  <td>
                    {user.first_name} {user.last_name}
                  </td>
                  <td>{user.email}</td>
                  <td>{user.mobile_phone}</td>
                  <td>
                    {user.status === 1 ? (
                      <span className="badge bg-success" style={{ width: "130px" }}>
                        Đang hoạt động
                      </span>
                    ) : (
                      <span className="badge bg-danger" style={{ width: "130px" }}>
                        Vô hiệu hóa
                      </span>
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <>
            <div className="text-center mb-3">Không có tài khoản nào</div>
          </>
        )}
      </div>
      {userList && userList.length > 0 ? (
        <div className="card-footer">
          <nav aria-label="Contacts Page Navigation">
            <ul className="pagination justify-content-center m-0">
              <li className="page-item active">
                <a className="page-link">1</a>
              </li>
            </ul>
          </nav>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};

export default UserListTable;
