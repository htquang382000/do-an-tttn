import React, { useState } from "react";

const SideNav = () => {
  const user = JSON.parse(localStorage.getItem("user"));
  const [details, setDetails] = useState({ value: "" });
  const menuOpen = (e, idName) => {
    // document.getElementById(idName).className += "menu-is-opening menu-open";
  };
  return (
    <div>
      <aside className="main-sidebar sidebar-dark-primary elevation-4">
        <a href="/" className="brand-link text-center">
          <span className="brand-text font-weight-bold">Quản lý nhân sự</span>
        </a>
        <div className="sidebar">
          <div className="user-panel mt-3 pb-3 mb-3 d-flex">
            <div className="image">
              <img
                src={user.image ?? "https://png.pngtree.com/element_our/20200610/ourlarge/pngtree-default-avatar-image_2237213.jpg"}
                className="img-circle elevation-2"
                alt="User Image"
              />
            </div>
            <div className="info">
              <a href="/profile" className="d-block">
                Hoàng Thanh Quang
              </a>
            </div>
          </div>
          <nav className="mt-2">
            <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <li className="nav-item" id="general" onClick={(e) => menuOpen(e, "general")}>
                <a className="nav-link">
                  <i className="nav-icon fas fa-house-user" />
                  <p>
                    Tổng quan
                    <i className="right fas fa-angle-left" />
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <a href="/" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Thống kê</p>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="/staff" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Danh sách nhân viên</p>
                    </a>
                  </li>
                  {user.user_type === 1 ? (
                    <li className="nav-item">
                      <a href="/user" className="nav-link">
                        <i className="far fa-circle nav-icon" />
                        <p>Danh sách tài khoản</p>
                      </a>
                    </li>
                  ) : (
                    <></>
                  )}
                </ul>
              </li>
              <li className="nav-item" id="staff" onClick={(e) => menuOpen(e, "staff")}>
                <a href="#" className="nav-link">
                  <i className="nav-icon fas fa-users" />
                  <p>
                    Nhân viên
                    <i className="right fas fa-angle-left" />
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <a href="/department" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Phòng ban</p>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="/position" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Chức vụ</p>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="/level" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Trình độ</p>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="/technique" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Chuyên môn</p>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="/certification" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Bằng cấp</p>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="/staff-type" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Loại nhân viên</p>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="/staff/create" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Thêm nhân viên mới</p>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="/comment" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Đánh giá nhân viên</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li className="nav-item" id="title" onClick={(e) => menuOpen(e, "title")}>
                <a href="#" className="nav-link">
                  <i className="nav-icon fas fa-id-card" />
                  <p>
                    Chức danh nghề
                    <i className="right fas fa-angle-left" />
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <a href="/title" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Chức danh</p>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="/job-title" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Chức danh nghề</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li className="nav-item" id="trip" onClick={(e) => menuOpen(e, "trip")}>
                <a href="#" className="nav-link">
                  <i className="nav-icon fas fa-suitcase" />
                  <p>
                    Chuyến công tác
                    <i className="right fas fa-angle-left" />
                  </p>
                </a>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <a href="/business-trip" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Chuyến công tác</p>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </nav>
        </div>
      </aside>
    </div>
  );
};

export default SideNav;
