const convertDateTimeForApp = (timestamp) => {
  let date = new Date(timestamp);

  return (
    ("0" + date.getHours()).slice(-2) +
    ":" +
    ("0" + date.getMinutes()).slice(-2) +
    ":" +
    ("0" + date.getSeconds()).slice(-2) +
    " " +
    ("0" + date.getDate()).slice(-2) +
    "/" +
    ("0" + (date.getMonth() + 1)).slice(-2) +
    "/" +
    date.getFullYear()
  );
};

const convertCurrency = (amount) => {
  amount = amount.toLocaleString("vi-VN", { style: "currency", currency: "VND" });
  return amount;
};

const getCurrentDay = () => {
  let today = new Date();
  let date = ("0" + today.getDate()).slice(-2) + "/" + ("0" + (today.getMonth() + 1)).slice(-2) + "/" + today.getFullYear();
  return date;
};

const calculateAge = (dayOfBirth) => {
  var today = new Date();
  var birthDate = new Date(dayOfBirth);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  return age;
};

const getUser = () => {
  let user = JSON.parse(localStorage.getItem("user"));
  return user;
};

const getBusiness = () => {
  let business = JSON.parse(localStorage.getItem("business"));
  return business;
};

const getBusinessLocationId = () => {
  let businessLocationId = localStorage.getItem("businessLocationId");
  return businessLocationId;
};

export default { convertDateTimeForApp, convertCurrency, getCurrentDay, calculateAge, getUser, getBusiness, getBusinessLocationId };
