const detect = () => {
  var user = localStorage.getItem("user") ? JSON.parse(localStorage.getItem("user")) : null;
  if (user == null) {
    var config = {
      // urlBaseApi: "http://localhost:1710/api/",
      urlBaseApi: "http://nhom-16.thegoodguys.tech/api/",
      headers: {
        "Accept-Language": "vi",
        "X-Authorization": "c27bf764096fe14c662232bbd9fbb837",
      },
    };
  } else {
    var config = {
      // urlBaseApi: "http://localhost:1710/api/",
      urlBaseApi: "http://nhom-16.thegoodguys.tech/api/",
      headers: {
        "Accept-Language": "vi",
        "X-Authorization": "c27bf764096fe14c662232bbd9fbb837",
        Authorization: "Bearer " + user.access_token,
      },
    };
  }
  return config;
};

const AppEnvironment = detect();
export default AppEnvironment;
