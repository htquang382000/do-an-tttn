-------- CÀI ĐẶT MÔI TRƯỜNG NODEJS VÀ NPM ----------
- Bước 1: Để download NodeJS, cần truy cập vào địa chỉ sau đây: https://nodejs.org/en/download/
        Chọn phiên bản tương ứng với hệ điều hành đang sử dụng để tải xuống.
        Sau khi tải thành công sẽ có 1 tệp để cài NodeJS.
- Bước 2: Cài đặt NodeJS rất đơn giản, mở tệp cài NodeJS, chấp nhận các tùy chọn mặc định và nhấn "Next ... Next" cho tới bước cuối cùng.
        Chọn "Install" để cài đặt NodeJS. Chọn "Finish" để hoàn tất cài đặt.
        Theo mặc định, phần mềm NPM cũng được cài đặt vào hệ thống. Đây là một phần mềm quản lý các thư viện Javascript.
- Bước 3: Sau khi cài đặt thành công NodeJS. Bây giờ cần kiểm tra lại kết quả cài đặt và cấu hình NodeJS.
        Mở cửa sổ CMD và thực thi các lệnh sau để kiểm tra phiên bản của NodeJS bằng lệnh "node -v", kiểm tra phiên bản NPM bằng lệnh "npm -v".
        Nếu hiển thị các phiên bản hiện tại của NodeJS và NPM thì cài đặt môi trường đã thành công.

----------------- CLONE DỰ ÁN ----------------------
- Bước 1: Tạo một thư mục để chứa dự án
- Bước 2: Mở Terminal thư mục vừa được tạo > Nhập dòng lệnh "git clone https://gitlab.com/htquang382000/do-an-tttn.git" để clone dự án về thư mục
- Bước 3: Sau khi clone dự án thành công, tiếp tục nhập lệnh "cd do-an-tttn" để truy cập vào thư mục chính chứa source code
- Bước 4: Cài đặt môi trường bằng cách nhập lệnh "npm install"
- Bước 5: Sau khi hoàn tất lệnh "npm install", nhập dòng lệnh "npm start" để khởi động chương trình
        Nếu trang web không tự động mở, sử dụng đường dẫn "http://localhost:3000" để vào trang web


--------------- TÀI KHOẢN TRANG WEB -----------------
- Tài khoản đã có sẵn dữ liệu: 
    + Tên đăng nhập: quanght@thegoodguys.tech. Mật khẩu: Aa123456
    + Chi nhánh "Quang chi nhánh 1" đã có sẵn nhân viên.
- Nếu muốn tạo tài khoản và doanh nghiệp cho riêng mình. Vào phần "Đăng ký" để đăng ký tài khoản